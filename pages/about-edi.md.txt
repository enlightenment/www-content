---
~~Title: About Edi~~
---

# Edi (The Enlightenment IDE) #



![Edi logo](/_media/edi-logo.png)

[Download the latest version of Edi](/download)

Edi is a development environment designed for and built using the
*Enlightenment Foundation Libraries (EFL)*. The overall project aim is
to create a new, native development environment for Linux that makes
getting up and running easier than ever before. With so much happening on
Linux, both on desktop and mobile, this will help more developers get
involved in the exciting future of open source development.

## Screenshots ##
![](/_media/aa/shot-2021-12-21_15-06-28.png)
![](/_media/aa/shot-2021-12-21_15-10-18.png)
![](/_media/aa/shot-2021-12-21_15-16-16.png)

## Features ##

Building an IDE is a huge task, so the project has been broken down into
phases. The 'Basic Text Editing' stage is complete. Work on a "Code aware
editor" is progressing well. Once the *Code Aware Editor* is complete,
work on "Basic IDE" will commence. More details are available on the
[project page](https://git.enlightenment.org/enlightenment/edi).

So far the main features include:

* Themes.
* Translucency.
* Multiple Panels.
* Split Editors.
* C, Python, Golang, Rust and other syntax-highlighting.
* Integrated Source Code Management.
* Auto suggestion for C files and headers
* Tabbed browsing of open files.
* Build management and test runner.
* Search & replace in file.
* Displaying file browser for the project (directory).
* No screen waste (toolbar, menu, tab are out of the way of the developer).
* Creation of new projects from name input and skeleton project files.

If you have ideas for features please let us know in the #e chat room on
[IRC](/contact)).
