~~Title: Efl.App: terminate~~

===== Description =====

%%Called before starting the shutdown of the application%%

//Since 1.22//

{{page>:develop:api-include:efl:app:event:terminate:description&nouser&nolink&nodate}}

===== Signature =====

<code>
terminate;
</code>

===== C information =====

<code c>
EFL_APP_EVENT_TERMINATE(void)
</code>

===== C usage =====

<code c>
static void
on_efl_app_event_terminate(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_APP_EVENT_TERMINATE, on_efl_app_event_terminate, d);
}

</code>
