~~Title: Efl.App.efl_version~~
====== Efl.App.efl_version ======

===== Values =====

  * **version** - %%Efl version%%


\\ {{page>:develop:api-include:efl:app:property:efl_version:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property efl_version {
    get {}
    values {
        version: const(Efl.Version);
    }
}
</code>

===== C signature =====

<code c>
const Efl_Version efl_app_efl_version_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:app:property:efl_version|Efl.App.efl_version]]

