~~Title: Efl.Callback_Priority_Default~~

===== Description =====

%%Default priority.%%

//Since 1.22//

{{page>:develop:api-include:efl:callback_priority_default:description&nouser&nolink&nodate}}

===== Signature =====

<code>
const Efl.Callback_Priority_Default: Efl.Callback_Priority = 0;
</code>

===== C signature =====

<code c>
#define EFL_CALLBACK_PRIORITY_DEFAULT 0
</code>
