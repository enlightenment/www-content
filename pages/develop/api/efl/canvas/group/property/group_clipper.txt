~~Title: Efl.Canvas.Group.group_clipper~~
====== Efl.Canvas.Group.group_clipper ======

===== Values =====

  * **clipper** - %%A clipper rectangle.%%


\\ {{page>:develop:api-include:efl:canvas:group:property:group_clipper:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property group_clipper {
    get @protected {}
    values {
        clipper: const(Efl.Canvas.Object);
    }
}
</code>

===== C signature =====

<code c>
const Efl_Canvas_Object *efl_canvas_group_clipper_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:group:property:group_clipper|Efl.Canvas.Group.group_clipper]]

