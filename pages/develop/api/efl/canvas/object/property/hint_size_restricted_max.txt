~~Title: Efl.Canvas.Object.hint_size_restricted_max~~
====== Efl.Canvas.Object.hint_size_restricted_max ======

===== Description =====

%%Internal hints for an object's maximum size.%%

%%This is not a size enforcement in any way, it's just a hint that should be used whenever appropriate.%%

%%Values -1 will be treated as unset hint components, when queried by managers.%%

<note>
%%This property is internal and meant for widget developers to define the absolute maximum size of the object. EFL itself sets this size internally, so any change to it from an application might be ignored. Applications should use %%[[:develop:api:efl:gfx:hint:property:hint_size_max|Efl.Gfx.Hint.hint_size_max]]%% instead.%%
</note>

<note>
%%It is an error for the %%[[:develop:api:efl:gfx:hint:property:hint_size_restricted_max|Efl.Gfx.Hint.hint_size_restricted_max]]%% to be smaller in either axis than %%[[:develop:api:efl:gfx:hint:property:hint_size_restricted_min|Efl.Gfx.Hint.hint_size_restricted_min]]%%. In this scenario, the max size hint will be prioritized over the user min size hint.%%
</note>

//Since 1.22//


{{page>:develop:api-include:efl:canvas:object:property:hint_size_restricted_max:description&nouser&nolink&nodate}}

===== Values =====

  * **sz** - %%Maximum size (hint) in pixels.%%
==== Setter ====

%%This function is protected as it is meant for widgets to indicate their "intrinsic" maximum size.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:hint_size_restricted_max:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:gfx:hint:property:hint_size_restricted_max|Efl.Gfx.Hint.hint_size_restricted_max]] **(get, set)**.//===== Signature =====

<code>
@property hint_size_restricted_max @pure_virtual {
    get {}
    set @protected {}
    values {
        sz: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_gfx_hint_size_restricted_max_get(const Eo *obj);
void efl_gfx_hint_size_restricted_max_set(Eo *obj, Eina_Size2D sz);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:hint:property:hint_size_restricted_max|Efl.Gfx.Hint.hint_size_restricted_max]]
  * [[:develop:api:efl:canvas:object:property:hint_size_restricted_max|Efl.Canvas.Object.hint_size_restricted_max]]

