~~Title: Efl.Canvas.Object.pass_events~~
====== Efl.Canvas.Object.pass_events ======

===== Description =====

%%Whether an Evas object is to pass (ignore) events.%%

%%If %%''pass''%% is %%''true''%%, it will make events on %%''obj''%% to be ignored. They will be triggered on the next lower object (that is not set to pass events), instead (see %%[[:develop:api:efl:gfx:stack:property:below|Efl.Gfx.Stack.below]]%%).%%

%%If %%''pass''%% is %%''false''%% events will be processed on that object as normal.%%

%%See also %%[[:develop:api:efl:canvas:object:property:repeat_events|Efl.Canvas.Object.repeat_events.set]]%%, %%[[:develop:api:efl:canvas:object:property:propagate_events|Efl.Canvas.Object.propagate_events.set]]%%%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:pass_events:description&nouser&nolink&nodate}}

===== Values =====

  * **pass** - %%Whether %%''obj''%% is to pass events (%%''true''%%) or not (%%''false''%%).%%

===== Signature =====

<code>
@property pass_events {
    get {}
    set {}
    values {
        pass: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_canvas_object_pass_events_get(const Eo *obj);
void efl_canvas_object_pass_events_set(Eo *obj, Eina_Bool pass);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:pass_events|Efl.Canvas.Object.pass_events]]

