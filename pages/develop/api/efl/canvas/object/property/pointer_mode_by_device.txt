~~Title: Efl.Canvas.Object.pointer_mode_by_device~~
====== Efl.Canvas.Object.pointer_mode_by_device ======

===== Description =====

%%Low-level pointer behaviour by device. See %%[[:develop:api:efl:canvas:object:property:pointer_mode|Efl.Canvas.Object.pointer_mode.get]]%% and %%[[:develop:api:efl:canvas:object:property:pointer_mode|Efl.Canvas.Object.pointer_mode.set]]%% for more explanation.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:pointer_mode_by_device:description&nouser&nolink&nodate}}

===== Keys =====

  * **dev** - %%The pointer device to set/get the mode. Use %%''null''%% for the default pointer.%%
===== Values =====

  * **pointer_mode** - %%The pointer mode%%

===== Signature =====

<code>
@property pointer_mode_by_device @beta {
    get {}
    set {
        return: bool;
    }
    keys {
        dev: Efl.Input.Device;
    }
    values {
        pointer_mode: Efl.Input.Object_Pointer_Mode;
    }
}
</code>

===== C signature =====

<code c>
Efl_Input_Object_Pointer_Mode efl_canvas_object_pointer_mode_by_device_get(const Eo *obj, Efl_Input_Device *dev);
Eina_Bool efl_canvas_object_pointer_mode_by_device_set(Eo *obj, Efl_Input_Device *dev, Efl_Input_Object_Pointer_Mode pointer_mode);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:pointer_mode_by_device|Efl.Canvas.Object.pointer_mode_by_device]]

