~~Title: Efl.Canvas.Object.render_parent~~
====== Efl.Canvas.Object.render_parent ======

===== Values =====

  * **parent** - %%The parent smart object of %%''obj''%% or %%''null''%%.%%


\\ {{page>:develop:api-include:efl:canvas:object:property:render_parent:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property render_parent {
    get @protected {}
    values {
        parent: Efl.Canvas.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Canvas_Object *efl_canvas_object_render_parent_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:object:property:render_parent|Efl.Canvas.Object.render_parent]]

