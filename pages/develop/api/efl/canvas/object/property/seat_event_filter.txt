~~Title: Efl.Canvas.Object.seat_event_filter~~
====== Efl.Canvas.Object.seat_event_filter ======

===== Description =====

%%Whether input events from a given seat are enabled. If the filter list is empty (no seat is disabled) this object will report mouse, keyboard and focus events from any seat, otherwise those events will only be reported if the event comes from a seat that is not in the list.%%

//Since 1.23//
{{page>:develop:api-include:efl:canvas:object:property:seat_event_filter:description&nouser&nolink&nodate}}

===== Keys =====

  * **seat** - %%The seat to act on.%%
===== Values =====

  * **enable** - %%%%''true''%% to enable events for a seat or %%''false''%% otherwise.%%

//Overridden from [[:develop:api:efl:input:interface:property:seat_event_filter|Efl.Input.Interface.seat_event_filter]] **(get, set)**.//===== Signature =====

<code>
@property seat_event_filter @beta @pure_virtual {
    get {}
    set {}
    keys {
        seat: Efl.Input.Device;
    }
    values {
        enable: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_input_seat_event_filter_get(const Eo *obj, Efl_Input_Device *seat);
void efl_input_seat_event_filter_set(Eo *obj, Efl_Input_Device *seat, Eina_Bool enable);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:interface:property:seat_event_filter|Efl.Input.Interface.seat_event_filter]]
  * [[:develop:api:efl:canvas:object:property:seat_event_filter|Efl.Canvas.Object.seat_event_filter]]

