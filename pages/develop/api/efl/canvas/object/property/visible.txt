~~Title: Efl.Canvas.Object.visible~~
====== Efl.Canvas.Object.visible ======

===== Description =====

%%The visibility of a canvas object.%%

%%All canvas objects will become visible by default just before render. This means that it is not required to call %%[[:develop:api:efl:gfx:entity:property:visible|Efl.Gfx.Entity.visible.set]]%% after creating an object unless you want to create it without showing it. Note that this behavior is new since 1.21, and only applies to canvas objects created with the EO API (i.e. not the legacy C-only API). Other types of Gfx objects may or may not be visible by default.%%

%%Note that many other parameters can prevent a visible object from actually being "visible" on screen. For instance if its color is fully transparent, or its parent is hidden, or it is clipped out, etc...%%

//Since 1.22//


{{page>:develop:api-include:efl:canvas:object:property:visible:description&nouser&nolink&nodate}}

===== Values =====

  * **v** - %%%%''true''%% if to make the object visible, %%''false''%% otherwise%%
==== Getter ====

%%Retrieves whether or not the given canvas object is visible.%%

//Since 1.22//


{{page>:develop:api-include:efl:canvas:object:property:visible:getter_description&nouser&nolink&nodate}}

==== Setter ====

%%Shows or hides this object.%%

//Since 1.22//
{{page>:develop:api-include:efl:canvas:object:property:visible:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:gfx:entity:property:visible|Efl.Gfx.Entity.visible]] **(get, set)**.//===== Signature =====

<code>
@property visible @pure_virtual {
    get {}
    set {}
    values {
        v: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_entity_visible_get(const Eo *obj);
void efl_gfx_entity_visible_set(Eo *obj, Eina_Bool v);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:entity:property:visible|Efl.Gfx.Entity.visible]]
  * [[:develop:api:efl:canvas:vg:node:property:visible|Efl.Canvas.Vg.Node.visible]]
  * [[:develop:api:efl:ui:win:property:visible|Efl.Ui.Win.visible]]
  * [[:develop:api:efl:ui:image:property:visible|Efl.Ui.Image.visible]]
  * [[:develop:api:efl:ui:widget:property:visible|Efl.Ui.Widget.visible]]
  * [[:develop:api:efl:ui:animation_view:property:visible|Efl.Ui.Animation_View.visible]]
  * [[:develop:api:efl:ui:text:property:visible|Efl.Ui.Text.visible]]
  * [[:develop:api:efl:ui:popup:property:visible|Efl.Ui.Popup.visible]]
  * [[:develop:api:efl:ui:navigation_bar_part_back_button:property:visible|Efl.Ui.Navigation_Bar_Part_Back_Button.visible]]
  * [[:develop:api:efl:canvas:object:property:visible|Efl.Canvas.Object.visible]]
  * [[:develop:api:efl:canvas:group:property:visible|Efl.Canvas.Group.visible]]
  * [[:develop:api:efl:canvas:event_grabber:property:visible|Efl.Canvas.Event_Grabber.visible]]
  * [[:develop:api:efl:canvas:layout:property:visible|Efl.Canvas.Layout.visible]]
  * [[:develop:api:efl:ui:pan:property:visible|Efl.Ui.Pan.visible]]

