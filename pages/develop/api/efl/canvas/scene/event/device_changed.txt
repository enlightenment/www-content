~~Title: Efl.Canvas.Scene: device,changed~~

===== Description =====

%%Called when input device changed%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:scene:event:device_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
device,changed @beta: Efl.Input.Device;
</code>

===== C information =====

<code c>
EFL_CANVAS_SCENE_EVENT_DEVICE_CHANGED(Efl_Input_Device *, @beta)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_scene_event_device_changed(void *data, const Efl_Event *event)
{
    Efl_Input_Device *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_SCENE_EVENT_DEVICE_CHANGED, on_efl_canvas_scene_event_device_changed, d);
}

</code>
