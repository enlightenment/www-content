~~Title: Efl.Canvas.Scene: object,focus,out~~

===== Description =====

%%Called when object lost focus%%

//Since 1.22//

{{page>:develop:api-include:efl:canvas:scene:event:object_focus_out:description&nouser&nolink&nodate}}

===== Signature =====

<code>
object,focus,out: Efl.Input.Focus;
</code>

===== C information =====

<code c>
EFL_CANVAS_SCENE_EVENT_OBJECT_FOCUS_OUT(Efl_Input_Focus *)
</code>

===== C usage =====

<code c>
static void
on_efl_canvas_scene_event_object_focus_out(void *data, const Efl_Event *event)
{
    Efl_Input_Focus *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CANVAS_SCENE_EVENT_OBJECT_FOCUS_OUT, on_efl_canvas_scene_event_object_focus_out, d);
}

</code>
