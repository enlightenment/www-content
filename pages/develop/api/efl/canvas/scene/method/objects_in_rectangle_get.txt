~~Title: Efl.Canvas.Scene.objects_in_rectangle_get~~
====== Efl.Canvas.Scene.objects_in_rectangle_get ======

===== Description =====

%%Retrieve a list of objects overlapping a given rectangular region in a canvas.%%

%%This function will traverse all the layers of the given canvas, from top to bottom, querying for objects with areas overlapping with the given rectangular region. The user can exclude from the query objects which are hidden and/or which are set to pass events.%%

<note warning>
%%This function will only evaluate top-level objects; child or "sub" objects will be skipped.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:canvas:scene:method:objects_in_rectangle_get:description&nouser&nolink&nodate}}

===== Signature =====

<code>
objects_in_rectangle_get @pure_virtual {
    params {
        @in rect: Eina.Rect;
        @in include_pass_events_objects: bool;
        @in include_hidden_objects: bool;
    }
    return: iterator<Efl.Gfx.Entity> @no_unused;
}
</code>

===== C signature =====

<code c>
Eina_Iterator *efl_canvas_scene_objects_in_rectangle_get(Eo *obj, Eina_Rect rect, Eina_Bool include_pass_events_objects, Eina_Bool include_hidden_objects);
</code>

===== Parameters =====

  * **rect** //(in)// - %%The rectangular region.%%
  * **include_pass_events_objects** //(in)// - %%Boolean flag to include or not objects which pass events in this calculation.%%
  * **include_hidden_objects** //(in)// - %%Boolean flag to include or not hidden objects in this calculation.%%

===== Implemented by =====

  * [[:develop:api:efl:canvas:scene:method:objects_in_rectangle_get|Efl.Canvas.Scene.objects_in_rectangle_get]]
  * [[:develop:api:efl:ui:win:method:objects_in_rectangle_get|Efl.Ui.Win.objects_in_rectangle_get]]

