~~Title: Efl.Canvas.Scene.seat_default~~
====== Efl.Canvas.Scene.seat_default ======

===== Values =====

  * **seat** - %%The default seat or %%''null''%% if one does not exist.%%
===== Description =====

%%Get the default seat.%%

//Since 1.22//

\\ {{page>:develop:api-include:efl:canvas:scene:property:seat_default:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property seat_default @beta @pure_virtual {
    get {}
    values {
        seat: Efl.Input.Device;
    }
}
</code>

===== C signature =====

<code c>
Efl_Input_Device *efl_canvas_scene_seat_default_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:canvas:scene:property:seat_default|Efl.Canvas.Scene.seat_default]]
  * [[:develop:api:efl:ui:win:property:seat_default|Efl.Ui.Win.seat_default]]

