~~Title: Efl.Composite_Model.child_add~~
====== Efl.Composite_Model.child_add ======

===== Description =====

%%Add a new child.%%

%%Add a new child, possibly dummy, depending on the implementation, of a internal keeping. When the child is effectively added the event %%[[:develop:api:efl:model:event:child,added|Efl.Model.child,added]]%% is then raised and the new child is kept along with other children.%%

//Since 1.23//
{{page>:develop:api-include:efl:composite_model:method:child_add:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:model:method:child_add|Efl.Model.child_add]].//===== Signature =====

<code>
child_add @pure_virtual {
    return: Efl.Object;
}
</code>

===== C signature =====

<code c>
Efl_Object *efl_model_child_add(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:model:method:child_add|Efl.Model.child_add]]
  * [[:develop:api:efl:mono_model_internal_child:method:child_add|Efl.Mono_Model_Internal_Child.child_add]]
  * [[:develop:api:efl:mono_model_internal:method:child_add|Efl.Mono_Model_Internal.child_add]]
  * [[:develop:api:eldbus:model:method:child_add|Eldbus.Model.child_add]]
  * [[:develop:api:efl:composite_model:method:child_add|Efl.Composite_Model.child_add]]
  * [[:develop:api:efl:io:model:method:child_add|Efl.Io.Model.child_add]]
  * [[:develop:api:efl:generic_model:method:child_add|Efl.Generic_Model.child_add]]

