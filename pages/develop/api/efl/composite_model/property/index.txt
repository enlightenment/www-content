~~Title: Efl.Composite_Model.index~~
====== Efl.Composite_Model.index ======

===== Description =====

%%Position of this object in the parent model.%%

%%It can only be set before the object is finalized but after the Model it composes is set (and only if that Model does not provide an index already). It can only be retrieved after the object has been finalized.%%

//Since 1.23//
{{page>:develop:api-include:efl:composite_model:property:index:description&nouser&nolink&nodate}}

===== Values =====

  * **index** - %%Index of the object in the parent model. The index is unique and starts from zero.%%

===== Signature =====

<code>
@property index {
    get {}
    set {}
    values {
        index: uint;
    }
}
</code>

===== C signature =====

<code c>
unsigned int efl_composite_model_index_get(const Eo *obj);
void efl_composite_model_index_set(Eo *obj, unsigned int index);
</code>

===== Implemented by =====

  * [[:develop:api:efl:composite_model:property:index|Efl.Composite_Model.index]]
  * [[:develop:api:efl:filter_model:property:index|Efl.Filter_Model.index]]

