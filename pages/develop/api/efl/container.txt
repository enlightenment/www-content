~~Title: Efl.Container~~
====== Efl.Container (interface) ======

===== Description =====

%%Common interface for objects (containers) that can have multiple contents (sub-objects).%%

%%APIs in this interface deal with containers of multiple sub-objects, not with individual parts.%%

//Since 1.22//

{{page>:develop:api-include:efl:container:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:container:method:content_count|content_count]]**\\
> %%Returns the number of contained sub-objects.%%
<code c>
int efl_content_count(Eo *obj);
</code>
\\
**[[:develop:api:efl:container:method:content_iterate|content_iterate]]**\\
> %%Begin iterating over this object's contents.%%
<code c>
Eina_Iterator *efl_content_iterate(Eo *obj);
</code>
\\

===== Events =====

**[[:develop:api:efl:container:event:content_added|content,added]]**\\
> %%Sent after a new sub-object was added.%%
<code c>
EFL_CONTAINER_EVENT_CONTENT_ADDED(Efl_Gfx_Entity *)
</code>
\\ **[[:develop:api:efl:container:event:content_removed|content,removed]]**\\
> %%Sent after a sub-object was removed, before unref.%%
<code c>
EFL_CONTAINER_EVENT_CONTENT_REMOVED(Efl_Gfx_Entity *)
</code>
\\ 