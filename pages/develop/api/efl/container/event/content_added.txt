~~Title: Efl.Container: content,added~~

===== Description =====

%%Sent after a new sub-object was added.%%

//Since 1.22//

{{page>:develop:api-include:efl:container:event:content_added:description&nouser&nolink&nodate}}

===== Signature =====

<code>
content,added: Efl.Gfx.Entity;
</code>

===== C information =====

<code c>
EFL_CONTAINER_EVENT_CONTENT_ADDED(Efl_Gfx_Entity *)
</code>

===== C usage =====

<code c>
static void
on_efl_container_event_content_added(void *data, const Efl_Event *event)
{
    Efl_Gfx_Entity *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_CONTAINER_EVENT_CONTENT_ADDED, on_efl_container_event_content_added, d);
}

</code>
