~~Title: Efl.Content.content~~
====== Efl.Content.content ======

===== Description =====

%%Sub-object currently set as this object's single content.%%

%%If it is set multiple times, previous sub-objects are removed first. Therefore, if an invalid %%''content''%% is set the object will become empty (it will have no sub-object).%%

//Since 1.22//
{{page>:develop:api-include:efl:content:property:content:description&nouser&nolink&nodate}}

===== Values =====

  * **content** - %%The sub-object.%%

===== Signature =====

<code>
@property content @pure_virtual {
    get {}
    set {
        return: bool;
    }
    values {
        content: Efl.Gfx.Entity;
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_content_get(const Eo *obj);
Eina_Bool efl_content_set(Eo *obj, Efl_Gfx_Entity *content);
</code>

===== Implemented by =====

  * [[:develop:api:efl:content:property:content|Efl.Content.content]]
  * [[:develop:api:efl:ui:check:property:content|Efl.Ui.Check.content]]
  * [[:develop:api:efl:ui:win:property:content|Efl.Ui.Win.content]]
  * [[:develop:api:efl:ui:default_item:property:content|Efl.Ui.Default_Item.content]]
  * [[:develop:api:efl:ui:win_part:property:content|Efl.Ui.Win_Part.content]]
  * [[:develop:api:efl:canvas:layout_part_external:property:content|Efl.Canvas.Layout_Part_External.content]]
  * [[:develop:api:efl:ui:navigation_bar_part_back_button:property:content|Efl.Ui.Navigation_Bar_Part_Back_Button.content]]
  * [[:develop:api:efl:ui:layout_part_legacy:property:content|Efl.Ui.Layout_Part_Legacy.content]]
  * [[:develop:api:elm:dayselector:part:property:content|Elm.Dayselector.Part.content]]
  * [[:develop:api:elm:entry:part:property:content|Elm.Entry.Part.content]]
  * [[:develop:api:elm:naviframe:part:property:content|Elm.Naviframe.Part.content]]
  * [[:develop:api:elm:ctxpopup:part:property:content|Elm.Ctxpopup.Part.content]]
  * [[:develop:api:elm:fileselector:entry:part:property:content|Elm.Fileselector.Entry.Part.content]]
  * [[:develop:api:elm:popup:part:property:content|Elm.Popup.Part.content]]
  * [[:develop:api:elm:hover:part:property:content|Elm.Hover.Part.content]]
  * [[:develop:api:elm:scroller:part:property:content|Elm.Scroller.Part.content]]
  * [[:develop:api:elm:player:part:property:content|Elm.Player.Part.content]]
  * [[:develop:api:elm:notify:part:property:content|Elm.Notify.Part.content]]
  * [[:develop:api:efl:ui:panel:property:content|Efl.Ui.Panel.content]]
  * [[:develop:api:elm:panel:part:property:content|Elm.Panel.Part.content]]
  * [[:develop:api:efl:ui:tab_page:property:content|Efl.Ui.Tab_Page.content]]
  * [[:develop:api:efl:ui:list_placeholder_item:property:content|Efl.Ui.List_Placeholder_Item.content]]
  * [[:develop:api:elm:mapbuf:part:property:content|Elm.Mapbuf.Part.content]]
  * [[:develop:api:efl:ui:scroller:property:content|Efl.Ui.Scroller.content]]
  * [[:develop:api:efl:ui:flip_part:property:content|Efl.Ui.Flip_Part.content]]
  * [[:develop:api:efl:canvas:layout_part_swallow:property:content|Efl.Canvas.Layout_Part_Swallow.content]]
  * [[:develop:api:efl:ui:pan:property:content|Efl.Ui.Pan.content]]
  * [[:develop:api:efl:ui:frame:property:content|Efl.Ui.Frame.content]]
  * [[:develop:api:efl:ui:progressbar:property:content|Efl.Ui.Progressbar.content]]
  * [[:develop:api:efl:ui:popup:property:content|Efl.Ui.Popup.content]]
  * [[:develop:api:efl:ui:navigation_layout:property:content|Efl.Ui.Navigation_Layout.content]]
  * [[:develop:api:elm:flip:part:property:content|Elm.Flip.Part.content]]
  * [[:develop:api:efl:ui:button:property:content|Efl.Ui.Button.content]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:content|Efl.Canvas.Layout_Part_Invalid.content]]
  * [[:develop:api:efl:ui:layout_part_content:property:content|Efl.Ui.Layout_Part_Content.content]]
  * [[:develop:api:efl:ui:progressbar_legacy_part:property:content|Efl.Ui.Progressbar_Legacy_Part.content]]
  * [[:develop:api:efl:ui:check_legacy_part:property:content|Efl.Ui.Check_Legacy_Part.content]]
  * [[:develop:api:efl:ui:button_legacy_part:property:content|Efl.Ui.Button_Legacy_Part.content]]
  * [[:develop:api:efl:ui:navigation_bar_part:property:content|Efl.Ui.Navigation_Bar_Part.content]]
  * [[:develop:api:efl:ui:radio_legacy_part:property:content|Efl.Ui.Radio_Legacy_Part.content]]

