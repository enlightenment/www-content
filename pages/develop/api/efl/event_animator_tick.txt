~~Title: Efl.Event_Animator_Tick~~

===== Description =====

%%EFL event animator tick data structure%%

//Since 1.22//

{{page>:develop:api-include:efl:event_animator_tick:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:event_animator_tick:fields&nouser&nolink&nodate}}

  * **update_area** - %%Area of the canvas that will be pushed to screen.%%

===== Signature =====

<code>
struct Efl.Event_Animator_Tick {
    update_area: Eina.Rect;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Event_Animator_Tick {
    Eina_Rect update_area;
} Efl_Event_Animator_Tick;
</code>
