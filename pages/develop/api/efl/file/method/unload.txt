~~Title: Efl.File.unload~~
====== Efl.File.unload ======

===== Description =====

%%Perform all necessary operations to unload file data from the object.%%

%%In the case where %%[[:develop:api:efl:file:property:mmap|Efl.File.mmap.set]]%% has been externally called on an object, the file handle stored in the object will be preserved.%%

%%Calling %%[[:develop:api:efl:file:method:unload|Efl.File.unload]]%% on an object which is not currently loaded will have no effect.%%

//Since 1.22//
{{page>:develop:api-include:efl:file:method:unload:description&nouser&nolink&nodate}}

===== Signature =====

<code>
unload {}
</code>

===== C signature =====

<code c>
void efl_file_unload(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:method:unload|Efl.File.unload]]
  * [[:develop:api:evas:canvas3d:mesh:method:unload|Evas.Canvas3D.Mesh.unload]]
  * [[:develop:api:efl:ui:win_part:method:unload|Efl.Ui.Win_Part.unload]]
  * [[:develop:api:efl:canvas:video:method:unload|Efl.Canvas.Video.unload]]
  * [[:develop:api:efl:ui:text:method:unload|Efl.Ui.Text.unload]]
  * [[:develop:api:efl:canvas:image:method:unload|Efl.Canvas.Image.unload]]
  * [[:develop:api:efl:canvas:layout:method:unload|Efl.Canvas.Layout.unload]]
  * [[:develop:api:efl:ui:layout:method:unload|Efl.Ui.Layout.unload]]
  * [[:develop:api:efl:ui:image:method:unload|Efl.Ui.Image.unload]]
  * [[:develop:api:efl:ui:image_zoomable:method:unload|Efl.Ui.Image_Zoomable.unload]]
  * [[:develop:api:efl:ui:animation_view:method:unload|Efl.Ui.Animation_View.unload]]
  * [[:develop:api:evas:canvas3d:texture:method:unload|Evas.Canvas3D.Texture.unload]]
  * [[:develop:api:efl:canvas:vg:object:method:unload|Efl.Canvas.Vg.Object.unload]]
  * [[:develop:api:efl:ui:widget_part_bg:method:unload|Efl.Ui.Widget_Part_Bg.unload]]
  * [[:develop:api:efl:ui:video:method:unload|Efl.Ui.Video.unload]]
  * [[:develop:api:efl:ui:popup_part_backwall:method:unload|Efl.Ui.Popup_Part_Backwall.unload]]
  * [[:develop:api:efl:ui:bg:method:unload|Efl.Ui.Bg.unload]]

