~~Title: Efl.File.loaded~~
====== Efl.File.loaded ======

===== Values =====

  * **loaded** - %%%%''true''%% if the object is loaded, %%''false''%% otherwise.%%


\\ {{page>:develop:api-include:efl:file:property:loaded:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property loaded {
    get {}
    values {
        loaded: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_file_loaded_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:property:loaded|Efl.File.loaded]]
  * [[:develop:api:efl:canvas:video:property:loaded|Efl.Canvas.Video.loaded]]

