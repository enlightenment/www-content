~~Title: Efl.Gfx.Center_Fill_Mode~~

===== Description =====

%%How an image's center region (the complement to the border region) should be rendered by EFL%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:center_fill_mode:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:gfx:center_fill_mode:fields&nouser&nolink&nodate}}

  * **none** - %%Image's center region is %%''not''%% to be rendered%%
  * **default** - %%Image's center region is to be %%''blended''%% with objects underneath it, if it has transparency. This is the default behavior for image objects%%
  * **solid** - %%Image's center region is to be made solid, even if it has transparency on it%%

===== Signature =====

<code>
enum Efl.Gfx.Center_Fill_Mode {
    none: 0,
    default: 1,
    solid: 2
}
</code>

===== C signature =====

<code c>
typedef enum {
    EFL_GFX_CENTER_FILL_MODE_NONE = 0,
    EFL_GFX_CENTER_FILL_MODE_DEFAULT = 1,
    EFL_GFX_CENTER_FILL_MODE_SOLID = 2
} Efl_Gfx_Center_Fill_Mode;
</code>
