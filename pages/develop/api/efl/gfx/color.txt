~~Title: Efl.Gfx.Color~~
====== Efl.Gfx.Color (mixin) ======

===== Description =====

%%Efl Gfx Color mixin class%%

//Since 1.22//

{{page>:develop:api-include:efl:gfx:color:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:gfx:color:property:color|color]]** //**(get, set)**//\\
> %%The general/main color of the given Evas object.%%
<code c>
void efl_gfx_color_get(const Eo *obj, int *r, int *g, int *b, int *a);
void efl_gfx_color_set(Eo *obj, int r, int g, int b, int a);
</code>
\\
**[[:develop:api:efl:gfx:color:property:color_code|color_code]]** //**(get, set)**//\\
> %%Hexadecimal color code of given Evas object (#RRGGBBAA).%%
<code c>
const char *efl_gfx_color_code_get(const Eo *obj);
void efl_gfx_color_code_set(Eo *obj, const char *colorcode);
</code>
\\

===== Events =====

