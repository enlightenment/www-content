~~Title: Efl.Gfx.Dash~~

===== Description =====

%%Type describing dash. %%[[:develop:api:efl:gfx:shape:property:stroke_dash|Efl.Gfx.Shape.stroke_dash.set]]%%%%

//Since 1.14//

{{page>:develop:api-include:efl:gfx:dash:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:gfx:dash:fields&nouser&nolink&nodate}}

  * **length** - %%Dash drawing length.%%
  * **gap** - %%Distance between two dashes.%%

===== Signature =====

<code>
struct Efl.Gfx.Dash {
    length: double;
    gap: double;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Gfx_Dash {
    double length;
    double gap;
} Efl_Gfx_Dash;
</code>
