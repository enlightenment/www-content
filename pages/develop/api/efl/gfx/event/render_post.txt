~~Title: Efl.Gfx.Event.Render_Post~~

===== Description =====

%%Data sent along a "render,post" event, after a frame has been rendered.%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:event:render_post:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:gfx:event:render_post:fields&nouser&nolink&nodate}}

  * **updated_area** - %%A list of rectangles that were updated in the canvas.%%

===== Signature =====

<code>
struct Efl.Gfx.Event.Render_Post {
    updated_area: list<Eina.Rect>;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Gfx_Event_Render_Post {
    Eina_List *updated_area;
} Efl_Gfx_Event_Render_Post;
</code>
