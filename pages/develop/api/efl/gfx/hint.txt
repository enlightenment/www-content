~~Title: Efl.Gfx.Hint~~
====== Efl.Gfx.Hint (interface) ======

===== Description =====

%%Efl graphics hint interface%%

//Since 1.22//

{{page>:develop:api-include:efl:gfx:hint:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:gfx:hint:property:hint_align|hint_align]]** //**(get, set)**//\\
> %%Hints for an object's alignment.%%
<code c>
void efl_gfx_hint_align_get(const Eo *obj, double *x, double *y);
void efl_gfx_hint_align_set(Eo *obj, double x, double y);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_aspect|hint_aspect]]** //**(get, set)**//\\
> %%Defines the aspect ratio to respect when scaling this object.%%
<code c>
void efl_gfx_hint_aspect_get(const Eo *obj, Efl_Gfx_Hint_Aspect *mode, Eina_Size2D *sz);
void efl_gfx_hint_aspect_set(Eo *obj, Efl_Gfx_Hint_Aspect mode, Eina_Size2D sz);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_fill|hint_fill]]** //**(get, set)**//\\
> %%Hints for an object's fill property that used to specify "justify" or "fill" by some users. %%[[:develop:api:efl:gfx:hint:property:hint_fill|Efl.Gfx.Hint.hint_fill]]%% specify whether to fill the space inside the boundaries of a container/manager.%%
<code c>
void efl_gfx_hint_fill_get(const Eo *obj, Eina_Bool *x, Eina_Bool *y);
void efl_gfx_hint_fill_set(Eo *obj, Eina_Bool x, Eina_Bool y);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_margin|hint_margin]]** //**(get, set)**//\\
> %%Hints for an object's margin or padding space.%%
<code c>
void efl_gfx_hint_margin_get(const Eo *obj, int *l, int *r, int *t, int *b);
void efl_gfx_hint_margin_set(Eo *obj, int l, int r, int t, int b);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_combined_max|hint_size_combined_max]]** //**(get)**//\\
> 
<code c>
Eina_Size2D efl_gfx_hint_size_combined_max_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_combined_min|hint_size_combined_min]]** //**(get)**//\\
> 
<code c>
Eina_Size2D efl_gfx_hint_size_combined_min_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_max|hint_size_max]]** //**(get, set)**//\\
> %%Hints on the object's maximum size.%%
<code c>
Eina_Size2D efl_gfx_hint_size_max_get(const Eo *obj);
void efl_gfx_hint_size_max_set(Eo *obj, Eina_Size2D sz);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_min|hint_size_min]]** //**(get, set)**//\\
> %%Hints on the object's minimum size.%%
<code c>
Eina_Size2D efl_gfx_hint_size_min_get(const Eo *obj);
void efl_gfx_hint_size_min_set(Eo *obj, Eina_Size2D sz);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_restricted_max|hint_size_restricted_max]]** //**(get, set)**// ''protected set''\\
> %%Internal hints for an object's maximum size.%%
<code c>
Eina_Size2D efl_gfx_hint_size_restricted_max_get(const Eo *obj);
void efl_gfx_hint_size_restricted_max_set(Eo *obj, Eina_Size2D sz);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_size_restricted_min|hint_size_restricted_min]]** //**(get, set)**// ''protected set''\\
> %%Internal hints for an object's minimum size.%%
<code c>
Eina_Size2D efl_gfx_hint_size_restricted_min_get(const Eo *obj);
void efl_gfx_hint_size_restricted_min_set(Eo *obj, Eina_Size2D sz);
</code>
\\
**[[:develop:api:efl:gfx:hint:property:hint_weight|hint_weight]]** //**(get, set)**//\\
> %%Hints for an object's weight.%%
<code c>
void efl_gfx_hint_weight_get(const Eo *obj, double *x, double *y);
void efl_gfx_hint_weight_set(Eo *obj, double x, double y);
</code>
\\

===== Events =====

**[[:develop:api:efl:gfx:hint:event:hints_changed|hints,changed]]**\\
> %%Object hints changed.%%
<code c>
EFL_GFX_ENTITY_EVENT_HINTS_CHANGED(void)
</code>
\\ 