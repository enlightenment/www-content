~~Title: Efl.Gfx.Hint_Align_Top~~

===== Description =====

%%Use with %%[[:develop:api:efl:gfx:hint:property:hint_align|Efl.Gfx.Hint.hint_align]]%%.%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:hint_align_top:description&nouser&nolink&nodate}}

===== Signature =====

<code>
const Efl.Gfx.Hint_Align_Top: double = 0.000000;
</code>

===== C signature =====

<code c>
#define EFL_GFX_HINT_ALIGN_TOP 0.000000
</code>
