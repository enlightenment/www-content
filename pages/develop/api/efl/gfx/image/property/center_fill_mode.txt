~~Title: Efl.Gfx.Image.center_fill_mode~~
====== Efl.Gfx.Image.center_fill_mode ======

===== Description =====

%%Specifies how the center part of the object (not the borders) should be drawn when EFL is rendering it.%%

%%This function sets how the center part of the image object's source image is to be drawn, which must be one of the values in %%[[:develop:api:efl:gfx:center_fill_mode|Efl.Gfx.Center_Fill_Mode]]%%. By center we mean the complementary part of that defined by %%[[:develop:api:efl:gfx:image:property:border_insets|Efl.Gfx.Image.border_insets.set]]%%. This is very useful for making frames and decorations. You would most probably also be using a filled image (as in %%[[:develop:api:efl:gfx:fill:property:fill_auto|Efl.Gfx.Fill.fill_auto]]%%) to use as a frame.%%

//Since 1.23//
{{page>:develop:api-include:efl:gfx:image:property:center_fill_mode:description&nouser&nolink&nodate}}

===== Values =====

  * **fill** - %%Fill mode of the center region. The default behavior is to render and scale the center area, respecting its transparency.%%

===== Signature =====

<code>
@property center_fill_mode @pure_virtual {
    get {}
    set {}
    values {
        fill: Efl.Gfx.Center_Fill_Mode (Efl.Gfx.Center_Fill_Mode.default);
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Center_Fill_Mode efl_gfx_image_center_fill_mode_get(const Eo *obj);
void efl_gfx_image_center_fill_mode_set(Eo *obj, Efl_Gfx_Center_Fill_Mode fill);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:center_fill_mode|Efl.Gfx.Image.center_fill_mode]]
  * [[:develop:api:efl:canvas:image_internal:property:center_fill_mode|Efl.Canvas.Image_Internal.center_fill_mode]]
  * [[:develop:api:efl:ui:image:property:center_fill_mode|Efl.Ui.Image.center_fill_mode]]

