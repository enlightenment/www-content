~~Title: Efl.Gfx.Image.scale_method~~
====== Efl.Gfx.Image.scale_method ======

===== Description =====

%%Determine how the image is scaled at render time.%%

%%This allows more granular controls for how an image object should display its internal buffer. The underlying image data will not be modified.%%

//Since 1.23//
{{page>:develop:api-include:efl:gfx:image:property:scale_method:description&nouser&nolink&nodate}}

===== Values =====

  * **scale_method** - %%Image scale type to use.%%

===== Signature =====

<code>
@property scale_method @pure_virtual {
    get {}
    set {}
    values {
        scale_method: Efl.Gfx.Image_Scale_Method (Efl.Gfx.Image_Scale_Method.none);
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Image_Scale_Method efl_gfx_image_scale_method_get(const Eo *obj);
void efl_gfx_image_scale_method_set(Eo *obj, Efl_Gfx_Image_Scale_Method scale_method);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:scale_method|Efl.Gfx.Image.scale_method]]
  * [[:develop:api:efl:ui:image:property:scale_method|Efl.Ui.Image.scale_method]]

