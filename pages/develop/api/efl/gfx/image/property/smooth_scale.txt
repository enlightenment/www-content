~~Title: Efl.Gfx.Image.smooth_scale~~
====== Efl.Gfx.Image.smooth_scale ======

===== Description =====

%%Whether to use high-quality image scaling algorithm for this image.%%

%%When enabled, a higher quality image scaling algorithm is used when scaling images to sizes other than the source image's original one. This gives better results but is more computationally expensive.%%

//Since 1.23//
{{page>:develop:api-include:efl:gfx:image:property:smooth_scale:description&nouser&nolink&nodate}}

===== Values =====

  * **smooth_scale** - %%Whether to use smooth scale or not.%%

===== Signature =====

<code>
@property smooth_scale @pure_virtual {
    get {}
    set {}
    values {
        smooth_scale: bool (true);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_image_smooth_scale_get(const Eo *obj);
void efl_gfx_image_smooth_scale_set(Eo *obj, Eina_Bool smooth_scale);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:smooth_scale|Efl.Gfx.Image.smooth_scale]]
  * [[:develop:api:efl:canvas:image_internal:property:smooth_scale|Efl.Canvas.Image_Internal.smooth_scale]]
  * [[:develop:api:efl:canvas:video:property:smooth_scale|Efl.Canvas.Video.smooth_scale]]
  * [[:develop:api:efl:ui:image:property:smooth_scale|Efl.Ui.Image.smooth_scale]]

