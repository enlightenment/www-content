~~Title: Efl.Gfx.Image.stretch_region~~
====== Efl.Gfx.Image.stretch_region ======

===== Description =====

%%This property defines the stretchable pixels region of an image.%%

%%When the regions are set by the user, the method will walk the iterators once and then destroy them. When the regions are retrieved by the user, it is his responsibility to destroy the iterators.. It will remember the information for the lifetime of the object. It will ignore all value of %%[[:develop:api:efl:gfx:image:property:border_insets|Efl.Gfx.Image.border_insets]]%%, %%[[:develop:api:efl:gfx:image:property:border_insets_scale|Efl.Gfx.Image.border_insets_scale]]%% and %%[[:develop:api:efl:gfx:image:property:center_fill_mode|Efl.Gfx.Image.center_fill_mode]]%% . To reset the object you can just pass %%''null''%% to both horizontal and vertical at the same time.%%

//Since 1.23//
{{page>:develop:api-include:efl:gfx:image:property:stretch_region:description&nouser&nolink&nodate}}

===== Values =====

  * **horizontal** - %%Representation of areas that are stretchable in the image horizontal space.%%
  * **vertical** - %%Representation of areas that are stretchable in the image vertical space.%%

===== Signature =====

<code>
@property stretch_region @pure_virtual {
    get {}
    set {
        return: Eina.Error;
    }
    values {
        horizontal: iterator<Efl.Gfx.Image_Stretch_Region> (null);
        vertical: iterator<Efl.Gfx.Image_Stretch_Region> (null);
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_image_stretch_region_get(const Eo *obj, Eina_Iterator **horizontal, Eina_Iterator **vertical);
Eina_Error efl_gfx_image_stretch_region_set(Eo *obj, Eina_Iterator *horizontal, Eina_Iterator *vertical);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:stretch_region|Efl.Gfx.Image.stretch_region]]
  * [[:develop:api:efl:canvas:image_internal:property:stretch_region|Efl.Canvas.Image_Internal.stretch_region]]
  * [[:develop:api:efl:ui:image:property:stretch_region|Efl.Ui.Image.stretch_region]]

