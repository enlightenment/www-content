~~Title: Efl.Gfx.Mapping.mapping_smooth~~
====== Efl.Gfx.Mapping.mapping_smooth ======

===== Description =====

%%Smoothing state for map rendering.%%

%%This sets smoothing for map rendering. If the object is a type that has its own smoothing settings, then both the smooth settings for this object and the map must be turned off. By default smooth maps are enabled.%%

//Since 1.22//
{{page>:develop:api-include:efl:gfx:mapping:property:mapping_smooth:description&nouser&nolink&nodate}}

===== Values =====

  * **smooth** - %%%%''true''%% by default.%%

===== Signature =====

<code>
@property mapping_smooth {
    get {}
    set {}
    values {
        smooth: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_mapping_smooth_get(const Eo *obj);
void efl_gfx_mapping_smooth_set(Eo *obj, Eina_Bool smooth);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:mapping:property:mapping_smooth|Efl.Gfx.Mapping.mapping_smooth]]

