~~Title: Efl.Gfx.Stack: stacking,changed~~

===== Description =====

%%Object stacking was changed.%%

//Since 1.22//

{{page>:develop:api-include:efl:gfx:stack:event:stacking_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
stacking,changed;
</code>

===== C information =====

<code c>
EFL_GFX_ENTITY_EVENT_STACKING_CHANGED(void)
</code>

===== C usage =====

<code c>
static void
on_efl_gfx_entity_event_stacking_changed(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_GFX_ENTITY_EVENT_STACKING_CHANGED, on_efl_gfx_entity_event_stacking_changed, d);
}

</code>
