~~Title: Efl.Gfx.Stack.raise_to_top~~
====== Efl.Gfx.Stack.raise_to_top ======

===== Description =====

%%Raise %%''obj''%% to the top of its layer.%%

%%%%''obj''%% will, then, be the highest one in the layer it belongs to. Object on other layers won't get touched.%%

%%See also %%[[:develop:api:efl:gfx:stack:method:stack_above|Efl.Gfx.Stack.stack_above]]%%(), %%[[:develop:api:efl:gfx:stack:method:stack_below|Efl.Gfx.Stack.stack_below]]%%() and %%[[:develop:api:efl:gfx:stack:method:lower_to_bottom|Efl.Gfx.Stack.lower_to_bottom]]%%()%%

//Since 1.22//
{{page>:develop:api-include:efl:gfx:stack:method:raise_to_top:description&nouser&nolink&nodate}}

===== Signature =====

<code>
raise_to_top @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_gfx_stack_raise_to_top(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:stack:method:raise_to_top|Efl.Gfx.Stack.raise_to_top]]
  * [[:develop:api:efl:canvas:vg:node:method:raise_to_top|Efl.Canvas.Vg.Node.raise_to_top]]
  * [[:develop:api:efl:ui:win:method:raise_to_top|Efl.Ui.Win.raise_to_top]]
  * [[:develop:api:efl:canvas:object:method:raise_to_top|Efl.Canvas.Object.raise_to_top]]

