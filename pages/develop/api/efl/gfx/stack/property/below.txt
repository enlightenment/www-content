~~Title: Efl.Gfx.Stack.below~~
====== Efl.Gfx.Stack.below ======



\\ {{page>:develop:api-include:efl:gfx:stack:property:below:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property below @pure_virtual {
    get {
        return: Efl.Gfx.Stack @no_unused;
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Stack *efl_gfx_stack_below_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:stack:property:below|Efl.Gfx.Stack.below]]
  * [[:develop:api:efl:canvas:vg:node:property:below|Efl.Canvas.Vg.Node.below]]
  * [[:develop:api:efl:canvas:object:property:below|Efl.Canvas.Object.below]]

