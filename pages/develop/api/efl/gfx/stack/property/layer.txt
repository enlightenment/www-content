~~Title: Efl.Gfx.Stack.layer~~
====== Efl.Gfx.Stack.layer ======

===== Description =====

%%The layer of its canvas that the given object will be part of.%%

%%If you don't use this property, you'll be dealing with a unique layer of objects (the default one). Additional layers are handy when you don't want a set of objects to interfere with another set with regard to stacking. Two layers are completely disjoint in that matter.%%

%%This is a low-level function, which you'd be using when something should be always on top, for example.%%

<note warning>
%%Don't change the layer of smart objects' children. Smart objects have a layer of their own, which should contain all their child objects.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:gfx:stack:property:layer:description&nouser&nolink&nodate}}

===== Values =====

  * **l** - %%The number of the layer to place the object on. Must be between %%[[:develop:api:efl:gfx:stack_layer_min|Efl.Gfx.Stack_Layer_Min]]%% and %%[[:develop:api:efl:gfx:stack_layer_max|Efl.Gfx.Stack_Layer_Max]]%%.%%

===== Signature =====

<code>
@property layer @pure_virtual {
    get {}
    set {}
    values {
        l: short;
    }
}
</code>

===== C signature =====

<code c>
short efl_gfx_stack_layer_get(const Eo *obj);
void efl_gfx_stack_layer_set(Eo *obj, short l);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:stack:property:layer|Efl.Gfx.Stack.layer]]
  * [[:develop:api:efl:canvas:object:property:layer|Efl.Canvas.Object.layer]]
  * [[:develop:api:efl:canvas:event_grabber:property:layer|Efl.Canvas.Event_Grabber.layer]]

