~~Title: Efl.Gfx.Stack_Layer_Min~~

===== Description =====

%%bottom-most layer number%%

//Since 1.23//

{{page>:develop:api-include:efl:gfx:stack_layer_min:description&nouser&nolink&nodate}}

===== Signature =====

<code>
const Efl.Gfx.Stack_Layer_Min: short = +32768;
</code>

===== C signature =====

<code c>
#define EFL_GFX_STACK_LAYER_MIN -32768/* +32768 */
</code>
