~~Title: Efl.Input.Device~~
====== Efl.Input.Device (class) ======

===== Description =====

%%Represents a pointing device such as a touch finger, pen or mouse.%%

//Since 1.23//

{{page>:develop:api-include:efl:input:device:description&nouser&nolink&nodate}}

===== Inheritance =====

 => [[:develop:api:efl:object|Efl.Object]] //(class)//
++++ Full hierarchy |

  * [[:develop:api:efl:object|Efl.Object]] //(class)//


++++
===== Members =====

**[[:develop:api:efl:input:device:method:children_iterate|children_iterate]]**\\
> %%Lists the children attached to this device.%%
<code c>
Eina_Iterator *efl_input_device_children_iterate(Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:method:constructor|constructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide optional initialization code for your object.%%
<code c>
Efl_Object *efl_constructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%Implement this method to provide deinitialization code for your object if you need it.%%
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:property:device_type|device_type]]** //**(get, set)**//\\
> %%Device type property%%
<code c>
Efl_Input_Device_Type efl_input_device_type_get(const Eo *obj);
void efl_input_device_type_set(Eo *obj, Efl_Input_Device_Type klass);
</code>
\\
**[[:develop:api:efl:input:device:property:is_pointer_type|is_pointer_type]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_input_device_is_pointer_type_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:property:parent|parent]]** //**(get, set)**//// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> %%The parent of an object.%%
<code c>
Efl_Object *efl_parent_get(const Eo *obj);
void efl_parent_set(Eo *obj, Efl_Object *parent);
</code>
\\
**[[:develop:api:efl:input:device:property:pointer_device_count|pointer_device_count]]** //**(get)**//\\
> 
<code c>
int efl_input_device_pointer_device_count_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:property:seat|seat]]** //**(get)**//\\
> 
<code c>
Efl_Input_Device *efl_input_device_seat_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:input:device:property:seat_id|seat_id]]** //**(get, set)**//\\
> %%Seat id number%%
<code c>
unsigned int efl_input_device_seat_id_get(const Eo *obj);
void efl_input_device_seat_id_set(Eo *obj, unsigned int id);
</code>
\\
**[[:develop:api:efl:input:device:property:source|source]]** //**(get, set)**//\\
> %%Device source property%%
<code c>
Efl_Input_Device *efl_input_device_source_get(const Eo *obj);
void efl_input_device_source_set(Eo *obj, Efl_Input_Device *src);
</code>
\\

==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:property:allow_parent_unref|allow_parent_unref]]** //**(get, set)**// | %%Allow an object to be deleted by unref even if it has a parent.%% |
|  | **[[:develop:api:efl:object:method:children_iterator_new|children_iterator_new]]** | %%Get an iterator on all children.%% |
|  | **[[:develop:api:efl:object:property:comment|comment]]** //**(get, set)**// | %%A human readable comment for the object.%% |
|  | **[[:develop:api:efl:object:method:composite_attach|composite_attach]]** | %%Make an object a composite object of another.%% |
|  | **[[:develop:api:efl:object:method:composite_detach|composite_detach]]** | %%Detach a composite object from another object.%% |
|  | **[[:develop:api:efl:object:method:composite_part_is|composite_part_is]]** | %%Check if an object is part of a composite object.%% |
|  | **[[:develop:api:efl:object:method:debug_name_override|debug_name_override]]** | %%Build a read-only name for this object used for debugging.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_del|event_callback_forwarder_del]]** | %%Remove an event callback forwarder for a specified event and object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_forwarder_priority_add|event_callback_forwarder_priority_add]]** | %%Add an event callback forwarder that will make this object emit an event whenever another object (%%''source''%%) emits it. The event is said to be forwarded from %%''source''%% to this object.%% |
|  | **[[:develop:api:efl:object:method:event_callback_stop|event_callback_stop]]** | %%Stop the current callback call.%% |
|  | **[[:develop:api:efl:object:method:event_freeze|event_freeze]]** | %%Freeze events of this object.%% |
|  | **[[:develop:api:efl:object:property:event_freeze_count|event_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_freeze|event_global_freeze]]** | %%Globally freeze events for ALL EFL OBJECTS.%% |
|  ''static'' | **[[:develop:api:efl:object:property:event_global_freeze_count|event_global_freeze_count]]** //**(get)**// |  |
|  ''static'' | **[[:develop:api:efl:object:method:event_global_thaw|event_global_thaw]]** | %%Globally thaw events for ALL EFL OBJECTS.%% |
|  | **[[:develop:api:efl:object:method:event_thaw|event_thaw]]** | %%Thaw events of object.%% |
|  | **[[:develop:api:efl:object:method:finalize|finalize]]** | %%Implement this method to finish the initialization of your object after all (if any) user-provided configuration methods have been executed.%% |
|  | **[[:develop:api:efl:object:property:finalized|finalized]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:method:invalidate|invalidate]]** | %%Implement this method to perform special actions when your object loses its parent, if you need to.%% |
|  | **[[:develop:api:efl:object:property:invalidated|invalidated]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:invalidating|invalidating]]** //**(get)**// |  |
|  | **[[:develop:api:efl:object:property:name|name]]** //**(get, set)**// | %%The name of the object.%% |
|  | **[[:develop:api:efl:object:method:name_find|name_find]]** | %%Find a child object with the given name and return it.%% |
|  | **[[:develop:api:efl:object:method:provider_find|provider_find]]** | %%Searches upwards in the object tree for a provider which knows the given class/interface.%% |
|  | **[[:develop:api:efl:object:method:provider_register|provider_register]]** | %%Will register a manager of a specific class to be answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |
|  | **[[:develop:api:efl:object:method:provider_unregister|provider_unregister]]** | %%Will unregister a manager of a specific class that was previously registered and answered by %%[[:develop:api:efl:object:method:provider_find|Efl.Object.provider_find]]%%.%% |

===== Events =====

==== Inherited ====

^ [[:develop:api:efl:object|Efl.Object]] ^^^
|  | **[[:develop:api:efl:object:event:del|del]]** | %%Object is being deleted. See %%[[:develop:api:efl:object:method:destructor|Efl.Object.destructor]]%%.%% |
|  | **[[:develop:api:efl:object:event:destruct|destruct]]** | %%Object has been fully destroyed. It can not be used beyond this point. This event should only serve to clean up any reference you keep to the object.%% |
|  | **[[:develop:api:efl:object:event:invalidate|invalidate]]** | %%Object is being invalidated and losing its parent. See %%[[:develop:api:efl:object:method:invalidate|Efl.Object.invalidate]]%%.%% |
|  | **[[:develop:api:efl:object:event:noref|noref]]** | %%Object has lost its last reference, only parent relationship is keeping it alive. Advanced usage.%% |
|  | **[[:develop:api:efl:object:event:ownership_shared|ownership,shared]]** | %%Object has acquired a second reference. It has multiple owners now. Triggered whenever increasing the refcount from one to two, it will not trigger by further increasing the refcount beyond two.%% |
|  | **[[:develop:api:efl:object:event:ownership_unique|ownership,unique]]** | %%Object has lost a reference and only one is left. It has just one owner now. Triggered whenever the refcount goes from two to one.%% |
