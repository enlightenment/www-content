~~Title: Efl.Input.Device.is_pointer_type~~
====== Efl.Input.Device.is_pointer_type ======

===== Values =====

  * **pointer_type** - %%%%''true''%% if the device has pointing capabilities.%%


\\ {{page>:develop:api-include:efl:input:device:property:is_pointer_type:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property is_pointer_type {
    get {}
    values {
        pointer_type: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_input_device_is_pointer_type_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:device:property:is_pointer_type|Efl.Input.Device.is_pointer_type]]

