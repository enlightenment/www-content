~~Title: Efl.Input.Hold.event_flags~~
====== Efl.Input.Hold.event_flags ======

===== Description =====

%%Extra flags for this event, may be changed by the user.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:hold:property:event_flags:description&nouser&nolink&nodate}}

===== Values =====

  * **flags** - %%Input event flags%%

//Overridden from [[:develop:api:efl:input:event:property:event_flags|Efl.Input.Event.event_flags]] **(get, set)**.//===== Signature =====

<code>
@property event_flags @pure_virtual {
    get {}
    set {}
    values {
        flags: Efl.Input.Flags;
    }
}
</code>

===== C signature =====

<code c>
Efl_Input_Flags efl_input_event_flags_get(const Eo *obj);
void efl_input_event_flags_set(Eo *obj, Efl_Input_Flags flags);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:event:property:event_flags|Efl.Input.Event.event_flags]]
  * [[:develop:api:efl:input:pointer:property:event_flags|Efl.Input.Pointer.event_flags]]
  * [[:develop:api:efl:input:key:property:event_flags|Efl.Input.Key.event_flags]]
  * [[:develop:api:efl:input:hold:property:event_flags|Efl.Input.Hold.event_flags]]
  * [[:develop:api:efl:input:focus:property:event_flags|Efl.Input.Focus.event_flags]]

