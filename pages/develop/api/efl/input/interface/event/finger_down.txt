~~Title: Efl.Input.Interface: finger,down~~

===== Description =====

%%Finger pressed (finger id is known).%%

//Since 1.23//

{{page>:develop:api-include:efl:input:interface:event:finger_down:description&nouser&nolink&nodate}}

===== Signature =====

<code>
finger,down: Efl.Input.Pointer;
</code>

===== C information =====

<code c>
EFL_EVENT_FINGER_DOWN(Efl_Input_Pointer *)
</code>

===== C usage =====

<code c>
static void
on_efl_event_finger_down(void *data, const Efl_Event *event)
{
    Efl_Input_Pointer *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_EVENT_FINGER_DOWN, on_efl_event_finger_down, d);
}

</code>
