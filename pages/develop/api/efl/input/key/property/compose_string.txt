~~Title: Efl.Input.Key.compose_string~~
====== Efl.Input.Key.compose_string ======

===== Description =====

%%A UTF8 string if this keystroke has modified a string in the middle of being composed.%%

<note>
%%This string replaces the previous one.%%
</note>

//Since 1.23//
{{page>:develop:api-include:efl:input:key:property:compose_string:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%Composed string in UTF8.%%

===== Signature =====

<code>
@property compose_string {
    get {}
    set {}
    values {
        val: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_input_key_compose_string_get(const Eo *obj);
void efl_input_key_compose_string_set(Eo *obj, const char *val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:key:property:compose_string|Efl.Input.Key.compose_string]]

