~~Title: Efl.Input.Pointer.button~~
====== Efl.Input.Pointer.button ======

===== Description =====

%%The mouse button that triggered the event.%%

%%Valid if and only if %%[[:develop:api:efl:input:pointer:property:value_has|Efl.Input.Pointer.value_has]]%%(%%''button''%%) is %%''true''%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:pointer:property:button:description&nouser&nolink&nodate}}

===== Values =====

  * **but** - %%1 to 32, 0 if not a button event.%%

===== Signature =====

<code>
@property button {
    get {}
    set {}
    values {
        but: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_input_pointer_button_get(const Eo *obj);
void efl_input_pointer_button_set(Eo *obj, int but);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:pointer:property:button|Efl.Input.Pointer.button]]

