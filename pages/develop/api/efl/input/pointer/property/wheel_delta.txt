~~Title: Efl.Input.Pointer.wheel_delta~~
====== Efl.Input.Pointer.wheel_delta ======

===== Description =====

%%Delta movement of the wheel in discrete steps.%%

//Since 1.23//
{{page>:develop:api-include:efl:input:pointer:property:wheel_delta:description&nouser&nolink&nodate}}

===== Values =====

  * **dist** - %%Wheel movement delta%%

===== Signature =====

<code>
@property wheel_delta {
    get {}
    set {}
    values {
        dist: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_input_pointer_wheel_delta_get(const Eo *obj);
void efl_input_pointer_wheel_delta_set(Eo *obj, int dist);
</code>

===== Implemented by =====

  * [[:develop:api:efl:input:pointer:property:wheel_delta|Efl.Input.Pointer.wheel_delta]]

