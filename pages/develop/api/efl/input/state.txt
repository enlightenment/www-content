~~Title: Efl.Input.State~~
====== Efl.Input.State (interface) ======

===== Description =====

%%Efl input state interface.%%

//Since 1.22//

{{page>:develop:api-include:efl:input:state:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:input:state:property:lock_enabled|lock_enabled]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_input_lock_enabled_get(const Eo *obj, Efl_Input_Lock lock, const Efl_Input_Device *seat);
</code>
\\
**[[:develop:api:efl:input:state:property:modifier_enabled|modifier_enabled]]** //**(get)**//\\
> 
<code c>
Eina_Bool efl_input_modifier_enabled_get(const Eo *obj, Efl_Input_Modifier mod, const Efl_Input_Device *seat);
</code>
\\

===== Events =====

