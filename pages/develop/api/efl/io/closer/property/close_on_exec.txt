~~Title: Efl.Io.Closer.close_on_exec~~
====== Efl.Io.Closer.close_on_exec ======

===== Description =====

%%If true will automatically close resources on exec() calls.%%

%%When using file descriptors this should set FD_CLOEXEC so they are not inherited by the processes (children or self) doing exec().%%

//Since 1.22//


{{page>:develop:api-include:efl:io:closer:property:close_on_exec:description&nouser&nolink&nodate}}

===== Values =====

  * **close_on_exec** - %%%%''true''%% if close on exec(), %%''false''%% otherwise%%
==== Setter ====

%%If %%''true''%%, will close on exec() call.%%

//Since 1.22//
{{page>:develop:api-include:efl:io:closer:property:close_on_exec:getter_description&nouser&nolink&nodate}}


===== Signature =====

<code>
@property close_on_exec @pure_virtual {
    get {}
    set {
        return: bool;
    }
    values {
        close_on_exec: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_io_closer_close_on_exec_get(const Eo *obj);
Eina_Bool efl_io_closer_close_on_exec_set(Eo *obj, Eina_Bool close_on_exec);
</code>

===== Implemented by =====

  * [[:develop:api:efl:io:closer:property:close_on_exec|Efl.Io.Closer.close_on_exec]]
  * [[:develop:api:efl:io:closer_fd:property:close_on_exec|Efl.Io.Closer_Fd.close_on_exec]]
  * [[:develop:api:efl:io:file:property:close_on_exec|Efl.Io.File.close_on_exec]]
  * [[:develop:api:efl:io:buffered_stream:property:close_on_exec|Efl.Io.Buffered_Stream.close_on_exec]]
  * [[:develop:api:efl:net:socket_ssl:property:close_on_exec|Efl.Net.Socket_Ssl.close_on_exec]]
  * [[:develop:api:efl:net:server_udp_client:property:close_on_exec|Efl.Net.Server_Udp_Client.close_on_exec]]
  * [[:develop:api:efl:net:dialer_websocket:property:close_on_exec|Efl.Net.Dialer_Websocket.close_on_exec]]
  * [[:develop:api:efl:net:dialer_http:property:close_on_exec|Efl.Net.Dialer_Http.close_on_exec]]
  * [[:develop:api:efl:net:socket_windows:property:close_on_exec|Efl.Net.Socket_Windows.close_on_exec]]
  * [[:develop:api:efl:io:buffer:property:close_on_exec|Efl.Io.Buffer.close_on_exec]]
  * [[:develop:api:efl:io:queue:property:close_on_exec|Efl.Io.Queue.close_on_exec]]
  * [[:develop:api:efl:io:copier:property:close_on_exec|Efl.Io.Copier.close_on_exec]]

