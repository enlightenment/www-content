~~Title: Efl.Io.Reader.read~~
====== Efl.Io.Reader.read ======

===== Description =====

%%Reads data into a pre-allocated buffer.%%

%%This operation will be executed immediately and may or may not block the caller thread for some time. The details of blocking behavior is to be defined by the implementation and may be subject to other parameters such as non-blocking flags, maximum timeout or even retry attempts.%%

%%You can understand this method as read(2) libc function.%%

//Since 1.22//
{{page>:develop:api-include:efl:io:reader:method:read:description&nouser&nolink&nodate}}

===== Signature =====

<code>
read @pure_virtual {
    params {
        @inout rw_slice: rw_slice<ubyte>;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_io_reader_read(Eo *obj, Eina_Rw_Slice rw_slice);
</code>

===== Parameters =====

  * **rw_slice** //(inout)// - %%Provides a pre-allocated memory to be filled up to rw_slice.len. It will be populated and the length will be set to the actually used amount of bytes, which can be smaller than the request.%%

===== Implemented by =====

  * [[:develop:api:efl:io:reader:method:read|Efl.Io.Reader.read]]
  * [[:develop:api:efl:io:buffered_stream:method:read|Efl.Io.Buffered_Stream.read]]
  * [[:develop:api:efl:appthread:method:read|Efl.Appthread.read]]
  * [[:develop:api:efl:io:reader_fd:method:read|Efl.Io.Reader_Fd.read]]
  * [[:develop:api:efl:io:file:method:read|Efl.Io.File.read]]
  * [[:develop:api:efl:io:stdin:method:read|Efl.Io.Stdin.read]]
  * [[:develop:api:efl:net:socket_fd:method:read|Efl.Net.Socket_Fd.read]]
  * [[:develop:api:efl:net:socket_udp:method:read|Efl.Net.Socket_Udp.read]]
  * [[:develop:api:efl:exe:method:read|Efl.Exe.read]]
  * [[:develop:api:efl:net:socket_ssl:method:read|Efl.Net.Socket_Ssl.read]]
  * [[:develop:api:efl:net:server_udp_client:method:read|Efl.Net.Server_Udp_Client.read]]
  * [[:develop:api:efl:net:dialer_websocket:method:read|Efl.Net.Dialer_Websocket.read]]
  * [[:develop:api:efl:net:dialer_http:method:read|Efl.Net.Dialer_Http.read]]
  * [[:develop:api:efl:net:socket_windows:method:read|Efl.Net.Socket_Windows.read]]
  * [[:develop:api:efl:thread:method:read|Efl.Thread.read]]
  * [[:develop:api:efl:io:buffer:method:read|Efl.Io.Buffer.read]]
  * [[:develop:api:efl:io:queue:method:read|Efl.Io.Queue.read]]

