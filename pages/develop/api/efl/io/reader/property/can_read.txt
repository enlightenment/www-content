~~Title: Efl.Io.Reader.can_read~~
====== Efl.Io.Reader.can_read ======

===== Description =====

%%If %%''true''%% will notify %%[[:develop:api:efl:io:reader:method:read|Efl.Io.Reader.read]]%% can be called without blocking or failing.%%

//Since 1.22//
{{page>:develop:api-include:efl:io:reader:property:can_read:description&nouser&nolink&nodate}}

===== Values =====

  * **can_read** - %%%%''true''%% if it can be read without blocking or failing, %%''false''%% otherwise%%

===== Signature =====

<code>
@property can_read @pure_virtual {
    get {}
    set @protected {}
    values {
        can_read: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_io_reader_can_read_get(const Eo *obj);
void efl_io_reader_can_read_set(Eo *obj, Eina_Bool can_read);
</code>

===== Implemented by =====

  * [[:develop:api:efl:io:reader:property:can_read|Efl.Io.Reader.can_read]]
  * [[:develop:api:efl:io:buffered_stream:property:can_read|Efl.Io.Buffered_Stream.can_read]]
  * [[:develop:api:efl:appthread:property:can_read|Efl.Appthread.can_read]]
  * [[:develop:api:efl:io:reader_fd:property:can_read|Efl.Io.Reader_Fd.can_read]]
  * [[:develop:api:efl:io:stdin:property:can_read|Efl.Io.Stdin.can_read]]
  * [[:develop:api:efl:net:socket_fd:property:can_read|Efl.Net.Socket_Fd.can_read]]
  * [[:develop:api:efl:exe:property:can_read|Efl.Exe.can_read]]
  * [[:develop:api:efl:net:socket_ssl:property:can_read|Efl.Net.Socket_Ssl.can_read]]
  * [[:develop:api:efl:net:server_udp_client:property:can_read|Efl.Net.Server_Udp_Client.can_read]]
  * [[:develop:api:efl:net:dialer_websocket:property:can_read|Efl.Net.Dialer_Websocket.can_read]]
  * [[:develop:api:efl:net:dialer_http:property:can_read|Efl.Net.Dialer_Http.can_read]]
  * [[:develop:api:efl:net:socket_windows:property:can_read|Efl.Net.Socket_Windows.can_read]]
  * [[:develop:api:efl:thread:property:can_read|Efl.Thread.can_read]]
  * [[:develop:api:efl:io:buffer:property:can_read|Efl.Io.Buffer.can_read]]
  * [[:develop:api:efl:io:queue:property:can_read|Efl.Io.Queue.can_read]]

