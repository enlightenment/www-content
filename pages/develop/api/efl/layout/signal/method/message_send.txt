~~Title: Efl.Layout.Signal.message_send~~
====== Efl.Layout.Signal.message_send ======

===== Description =====

%%Sends an (Edje) message to a given Edje object%%

%%This function sends an Edje message to obj and to all of its child objects, if it has any (swallowed objects are one kind of child object). Only a few types are supported: - int, - float/double, - string/stringshare, - arrays of int, float, double or strings.%%

%%Messages can go both ways, from code to theme, or theme to code.%%

%%The id argument as a form of code and theme defining a common interface on message communication. One should define the same IDs on both code and EDC declaration, to individualize messages (binding them to a given context).%%

//Since 1.22//
{{page>:develop:api-include:efl:layout:signal:method:message_send:description&nouser&nolink&nodate}}

===== Signature =====

<code>
message_send @pure_virtual {
    params {
        @in id: int;
        @in msg: const(any_value);
    }
}
</code>

===== C signature =====

<code c>
void efl_layout_signal_message_send(Eo *obj, int id, const Eina_Value msg);
</code>

===== Parameters =====

  * **id** //(in)// - %%A identification number for the message to be sent%%
  * **msg** //(in)// - %%The message's payload%%

===== Implemented by =====

  * [[:develop:api:efl:layout:signal:method:message_send|Efl.Layout.Signal.message_send]]
  * [[:develop:api:efl:canvas:layout:method:message_send|Efl.Canvas.Layout.message_send]]
  * [[:develop:api:efl:ui:image:method:message_send|Efl.Ui.Image.message_send]]
  * [[:develop:api:efl:ui:layout_base:method:message_send|Efl.Ui.Layout_Base.message_send]]

