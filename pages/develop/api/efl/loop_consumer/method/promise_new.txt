~~Title: Efl.Loop_Consumer.promise_new~~
====== Efl.Loop_Consumer.promise_new ======

===== Description =====

%%Create a new promise with the scheduler coming from the loop provided by this object.%%

<note>
%%You should not use eina_promise_data_set as this function rely on controlling the promise data.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:loop_consumer:method:promise_new:description&nouser&nolink&nodate}}

===== Signature =====

<code>
promise_new @const {
    return: Eina.Promise;
}
</code>

===== C signature =====

<code c>
Eina_Promise efl_loop_promise_new(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:loop_consumer:method:promise_new|Efl.Loop_Consumer.promise_new]]

