~~Title: Efl.Loop_Timer.timer_reset~~
====== Efl.Loop_Timer.timer_reset ======

===== Description =====

%%Resets a timer to its full interval. This effectively makes the timer start ticking off from zero now.%%

%%This is equal to delaying the timer by the already passed time, since the timer started ticking%%

//Since 1.22//
{{page>:develop:api-include:efl:loop_timer:method:timer_reset:description&nouser&nolink&nodate}}

===== Signature =====

<code>
timer_reset {}
</code>

===== C signature =====

<code c>
void efl_loop_timer_reset(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:loop_timer:method:timer_reset|Efl.Loop_Timer.timer_reset]]

