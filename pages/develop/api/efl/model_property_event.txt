~~Title: Efl.Model_Property_Event~~

===== Description =====

%%EFL model property event data structure%%

//Since 1.23//

{{page>:develop:api-include:efl:model_property_event:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:model_property_event:fields&nouser&nolink&nodate}}

  * **changed_properties** - %%List of changed properties%%
  * **invalidated_properties** - %%Removed properties identified by name%%

===== Signature =====

<code>
struct Efl.Model_Property_Event {
    changed_properties: array<stringshare>;
    invalidated_properties: array<stringshare>;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Model_Property_Event {
    Eina_Array *changed_properties;
    Eina_Array *invalidated_properties;
} Efl_Model_Property_Event;
</code>
