~~Title: Efl.Object.invalidated~~
====== Efl.Object.invalidated ======

===== Values =====

  * **finalized** - %%%%''true''%% if the object is invalidated, %%''false''%% otherwise.%%


\\ {{page>:develop:api-include:efl:object:property:invalidated:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property invalidated {
    get {}
    values {
        finalized: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_invalidated_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:object:property:invalidated|Efl.Object.invalidated]]

