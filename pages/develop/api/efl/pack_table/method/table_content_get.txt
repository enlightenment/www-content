~~Title: Efl.Pack_Table.table_content_get~~
====== Efl.Pack_Table.table_content_get ======

===== Description =====

%%Returns a child at a given position, see %%[[:develop:api:efl:pack_table:method:table_contents_get|Efl.Pack_Table.table_contents_get]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:pack_table:method:table_content_get:description&nouser&nolink&nodate}}

===== Signature =====

<code>
table_content_get @pure_virtual {
    params {
        @in col: int;
        @in row: int;
    }
    return: Efl.Gfx.Entity;
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_pack_table_content_get(Eo *obj, int col, int row);
</code>

===== Parameters =====

  * **col** //(in)// - %%Column number%%
  * **row** //(in)// - %%Row number%%

===== Implemented by =====

  * [[:develop:api:efl:pack_table:method:table_content_get|Efl.Pack_Table.table_content_get]]
  * [[:develop:api:efl:ui:table:method:table_content_get|Efl.Ui.Table.table_content_get]]
  * [[:develop:api:efl:canvas:layout_part_table:method:table_content_get|Efl.Canvas.Layout_Part_Table.table_content_get]]
  * [[:develop:api:efl:ui:layout_part_table:method:table_content_get|Efl.Ui.Layout_Part_Table.table_content_get]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:table_content_get|Efl.Canvas.Layout_Part_Invalid.table_content_get]]

