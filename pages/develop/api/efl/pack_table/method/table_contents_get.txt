~~Title: Efl.Pack_Table.table_contents_get~~
====== Efl.Pack_Table.table_contents_get ======

===== Description =====

%%Returns all objects at a given position in this table.%%

//Since 1.23//
{{page>:develop:api-include:efl:pack_table:method:table_contents_get:description&nouser&nolink&nodate}}

===== Signature =====

<code>
table_contents_get @pure_virtual {
    params {
        @in col: int;
        @in row: int;
        @in below: bool @optional;
    }
    return: iterator<Efl.Gfx.Entity>;
}
</code>

===== C signature =====

<code c>
Eina_Iterator *efl_pack_table_contents_get(Eo *obj, int col, int row, Eina_Bool below);
</code>

===== Parameters =====

  * **col** //(in)// - %%Column number%%
  * **row** //(in)// - %%Row number%%
  * **below** //(in)// - %%If %%''true''%% get objects spanning over this cell.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_table:method:table_contents_get|Efl.Pack_Table.table_contents_get]]
  * [[:develop:api:efl:ui:table:method:table_contents_get|Efl.Ui.Table.table_contents_get]]
  * [[:develop:api:efl:canvas:layout_part_table:method:table_contents_get|Efl.Canvas.Layout_Part_Table.table_contents_get]]
  * [[:develop:api:efl:ui:layout_part_table:method:table_contents_get|Efl.Ui.Layout_Part_Table.table_contents_get]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:table_contents_get|Efl.Canvas.Layout_Part_Invalid.table_contents_get]]

