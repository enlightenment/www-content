~~Title: Efl.Pack_Table.table_size~~
====== Efl.Pack_Table.table_size ======

===== Description =====

%%Combines %%[[:develop:api:efl:pack_table:property:table_columns|Efl.Pack_Table.table_columns]]%% and %%[[:develop:api:efl:pack_table:property:table_rows|Efl.Pack_Table.table_rows]]%%%%

//Since 1.23//
{{page>:develop:api-include:efl:pack_table:property:table_size:description&nouser&nolink&nodate}}

===== Values =====

  * **cols** - %%Number of columns%%
  * **rows** - %%Number of rows%%

===== Signature =====

<code>
@property table_size @pure_virtual {
    get {}
    set {}
    values {
        cols: int;
        rows: int;
    }
}
</code>

===== C signature =====

<code c>
void efl_pack_table_size_get(const Eo *obj, int *cols, int *rows);
void efl_pack_table_size_set(Eo *obj, int cols, int rows);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack_table:property:table_size|Efl.Pack_Table.table_size]]
  * [[:develop:api:efl:ui:table:property:table_size|Efl.Ui.Table.table_size]]
  * [[:develop:api:efl:canvas:layout_part_table:property:table_size|Efl.Canvas.Layout_Part_Table.table_size]]
  * [[:develop:api:efl:ui:layout_part_table:property:table_size|Efl.Ui.Layout_Part_Table.table_size]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:table_size|Efl.Canvas.Layout_Part_Invalid.table_size]]

