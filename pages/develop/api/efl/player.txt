~~Title: Efl.Player~~
====== Efl.Player (interface) ======

===== Description =====

%%Efl media player interface%%

//Since 1.23//

{{page>:develop:api-include:efl:player:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:player:property:paused|paused]]** //**(get, set)**//\\
> %%Pause state of the media file.%%
<code c>
Eina_Bool efl_player_paused_get(const Eo *obj);
Eina_Bool efl_player_paused_set(Eo *obj, Eina_Bool paused);
</code>
\\
**[[:develop:api:efl:player:property:playback_position|playback_position]]** //**(get, set)**//\\
> %%Position in the media file.%%
<code c>
double efl_player_playback_position_get(const Eo *obj);
void efl_player_playback_position_set(Eo *obj, double sec);
</code>
\\
**[[:develop:api:efl:player:property:playback_progress|playback_progress]]** //**(get)**//\\
> 
<code c>
double efl_player_playback_progress_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:player:property:playback_speed|playback_speed]]** //**(get, set)**//\\
> %%Control the playback speed of the media file.%%
<code c>
double efl_player_playback_speed_get(const Eo *obj);
void efl_player_playback_speed_set(Eo *obj, double speed);
</code>
\\
**[[:develop:api:efl:player:property:playing|playing]]** //**(get, set)**//\\
> %%Playback state of the media file.%%
<code c>
Eina_Bool efl_player_playing_get(const Eo *obj);
Eina_Bool efl_player_playing_set(Eo *obj, Eina_Bool playing);
</code>
\\

===== Events =====

