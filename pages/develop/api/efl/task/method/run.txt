~~Title: Efl.Task.run~~
====== Efl.Task.run ======

===== Description =====

%%Actually run the task.%%

//Since 1.22//
{{page>:develop:api-include:efl:task:method:run:description&nouser&nolink&nodate}}

===== Signature =====

<code>
run @pure_virtual {
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_task_run(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:task:method:run|Efl.Task.run]]
  * [[:develop:api:efl:loop:method:run|Efl.Loop.run]]
  * [[:develop:api:efl:exe:method:run|Efl.Exe.run]]
  * [[:develop:api:efl:thread:method:run|Efl.Thread.run]]

