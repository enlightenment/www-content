~~Title: Efl.Task.flags~~
====== Efl.Task.flags ======

===== Description =====

%%Flags to further customize task's behavior.%%

//Since 1.22//
{{page>:develop:api-include:efl:task:property:flags:description&nouser&nolink&nodate}}

===== Values =====

  * **flags** - %%Desired task flags.%%

===== Signature =====

<code>
@property flags {
    get {}
    set {}
    values {
        flags: Efl.Task_Flags (Efl.Task_Flags.exit_with_parent);
    }
}
</code>

===== C signature =====

<code c>
Efl_Task_Flags efl_task_flags_get(const Eo *obj);
void efl_task_flags_set(Eo *obj, Efl_Task_Flags flags);
</code>

===== Implemented by =====

  * [[:develop:api:efl:task:property:flags|Efl.Task.flags]]

