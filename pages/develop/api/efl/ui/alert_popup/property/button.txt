~~Title: Efl.Ui.Alert_Popup.button~~
====== Efl.Ui.Alert_Popup.button ======

===== Keys =====

  * **type** - %%Alert_Popup button type.%%
===== Values =====

  * **text** - %%Text of the specified button type.%%
  * **icon** - %%Visual to use as an icon for the specified button type.%%


\\ {{page>:develop:api-include:efl:ui:alert_popup:property:button:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property button {
    set {
        keys {
            type: Efl.Ui.Alert_Popup_Button;
        }
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_alert_popup_button_set(Eo *obj, Efl_Ui_Alert_Popup_Button type, const char *text, Efl_Canvas_Object *icon);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:alert_popup:property:button|Efl.Ui.Alert_Popup.button]]

