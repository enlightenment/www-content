~~Title: Efl.Ui.Box.pack_at~~
====== Efl.Ui.Box.pack_at ======

===== Description =====

%%Inserts %%''subobj''%% BEFORE the sub-object at position %%''index''%%.%%

%%%%''index''%% ranges from %%''-count''%% to %%''count-1''%%, where positive numbers go from first sub-object (%%''0''%%) to last (%%''count-1''%%), and negative numbers go from last sub-object (%%''-1''%%) to first (%%''-count''%%). %%''count''%% is the number of sub-objects currently in the container as returned by %%[[:develop:api:efl:container:method:content_count|Efl.Container.content_count]]%%.%%

%%If %%''index''%% is less than %%''-count''%%, it will trigger %%[[:develop:api:efl:pack_linear:method:pack_begin|Efl.Pack_Linear.pack_begin]]%% whereas %%''index''%% greater than %%''count-1''%% will trigger %%[[:develop:api:efl:pack_linear:method:pack_end|Efl.Pack_Linear.pack_end]]%%.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:box:method:pack_at:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_at|Efl.Pack_Linear.pack_at]].//===== Signature =====

<code>
pack_at @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
        @in index: int;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_at(Eo *obj, Efl_Gfx_Entity *subobj, int index);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%Object to pack.%%
  * **index** //(in)// - %%Index of existing sub-object to insert BEFORE. Valid range is %%''-count''%% to %%''count-1''%%).%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_at|Efl.Pack_Linear.pack_at]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_at|Efl.Ui.Tab_Bar.pack_at]]
  * [[:develop:api:efl:ui:flip:method:pack_at|Efl.Ui.Flip.pack_at]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_at|Efl.Canvas.Layout_Part_Box.pack_at]]
  * [[:develop:api:efl:ui:box:method:pack_at|Efl.Ui.Box.pack_at]]
  * [[:develop:api:efl:ui:radio_box:method:pack_at|Efl.Ui.Radio_Box.pack_at]]
  * [[:develop:api:efl:ui:group_item:method:pack_at|Efl.Ui.Group_Item.pack_at]]
  * [[:develop:api:efl:ui:collection:method:pack_at|Efl.Ui.Collection.pack_at]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_at|Efl.Ui.Layout_Part_Box.pack_at]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_at|Efl.Canvas.Layout_Part_Invalid.pack_at]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_at|Efl.Ui.Spotlight.Container.pack_at]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_at|Efl.Ui.Tab_Pager.pack_at]]

