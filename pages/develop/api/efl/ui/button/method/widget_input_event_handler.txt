~~Title: Efl.Ui.Button.widget_input_event_handler~~
====== Efl.Ui.Button.widget_input_event_handler ======

===== Description =====

%%Virtual function handling input events on the widget.%%

%%This method should return %%''true''%% if the event has been processed. Only key down, key up and pointer wheel events will be propagated through this function.%%

%%It is common for the event to be also marked as processed as in %%[[:develop:api:efl:input:event:property:processed|Efl.Input.Event.processed]]%%, if this operation was successful. This makes sure other widgets will not also process this input event.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:button:method:widget_input_event_handler:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:widget:method:widget_input_event_handler|Efl.Ui.Widget.widget_input_event_handler]].//===== Signature =====

<code>
widget_input_event_handler @protected {
    params {
        @in eo_event: const(event);
        @in source: Efl.Canvas.Object;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_input_event_handler(Eo *obj, const Efl_Event *eo_event, Efl_Canvas_Object *source);
</code>

===== Parameters =====

  * **eo_event** //(in)// - %%EO event struct with an Efl.Input.Event as info.%%
  * **source** //(in)// - %%Source object where the event originated. Often same as this.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:widget_input_event_handler|Efl.Ui.Widget.widget_input_event_handler]]
  * [[:develop:api:efl:ui:win:method:widget_input_event_handler|Efl.Ui.Win.widget_input_event_handler]]
  * [[:develop:api:efl:ui:image:method:widget_input_event_handler|Efl.Ui.Image.widget_input_event_handler]]
  * [[:develop:api:efl:ui:image_zoomable:method:widget_input_event_handler|Efl.Ui.Image_Zoomable.widget_input_event_handler]]
  * [[:develop:api:efl:ui:slider:method:widget_input_event_handler|Efl.Ui.Slider.widget_input_event_handler]]
  * [[:develop:api:efl:ui:check:method:widget_input_event_handler|Efl.Ui.Check.widget_input_event_handler]]
  * [[:develop:api:efl:ui:radio:method:widget_input_event_handler|Efl.Ui.Radio.widget_input_event_handler]]
  * [[:develop:api:efl:ui:spin_button:method:widget_input_event_handler|Efl.Ui.Spin_Button.widget_input_event_handler]]
  * [[:develop:api:efl:ui:panel:method:widget_input_event_handler|Efl.Ui.Panel.widget_input_event_handler]]
  * [[:develop:api:efl:ui:item:method:widget_input_event_handler|Efl.Ui.Item.widget_input_event_handler]]
  * [[:develop:api:efl:ui:slider_interval:method:widget_input_event_handler|Efl.Ui.Slider_Interval.widget_input_event_handler]]
  * [[:develop:api:elm:code_widget:method:widget_input_event_handler|Elm.Code_Widget.widget_input_event_handler]]
  * [[:develop:api:efl:ui:tags:method:widget_input_event_handler|Efl.Ui.Tags.widget_input_event_handler]]
  * [[:develop:api:efl:ui:scroller:method:widget_input_event_handler|Efl.Ui.Scroller.widget_input_event_handler]]
  * [[:develop:api:efl:ui:calendar:method:widget_input_event_handler|Efl.Ui.Calendar.widget_input_event_handler]]
  * [[:develop:api:efl:ui:video:method:widget_input_event_handler|Efl.Ui.Video.widget_input_event_handler]]
  * [[:develop:api:efl:ui:button:method:widget_input_event_handler|Efl.Ui.Button.widget_input_event_handler]]

