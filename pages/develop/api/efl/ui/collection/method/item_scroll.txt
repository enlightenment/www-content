~~Title: Efl.Ui.Collection.item_scroll~~
====== Efl.Ui.Collection.item_scroll ======

===== Description =====

%%Brings the passed item into the viewport.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:method:item_scroll:description&nouser&nolink&nodate}}

===== Signature =====

<code>
item_scroll {
    params {
        @in item: Efl.Ui.Item;
        @in animation: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_collection_item_scroll(Eo *obj, Efl_Ui_Item *item, Eina_Bool animation);
</code>

===== Parameters =====

  * **item** //(in)// - %%The target to move into view.%%
  * **animation** //(in)// - %%If you want to have an animated transition.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:collection:method:item_scroll|Efl.Ui.Collection.item_scroll]]

