~~Title: Efl.Ui.Collection.pack_clear~~
====== Efl.Ui.Collection.pack_clear ======

===== Description =====

%%Removes all packed sub-objects and unreferences them.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:collection:method:pack_clear:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack:method:pack_clear|Efl.Pack.pack_clear]].//===== Signature =====

<code>
pack_clear @pure_virtual {
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_clear(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack:method:pack_clear|Efl.Pack.pack_clear]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_clear|Efl.Ui.Tab_Bar.pack_clear]]
  * [[:develop:api:efl:ui:table:method:pack_clear|Efl.Ui.Table.pack_clear]]
  * [[:develop:api:efl:canvas:layout_part_table:method:pack_clear|Efl.Canvas.Layout_Part_Table.pack_clear]]
  * [[:develop:api:efl:ui:layout_part_table:method:pack_clear|Efl.Ui.Layout_Part_Table.pack_clear]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_clear|Efl.Canvas.Layout_Part_Invalid.pack_clear]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_clear|Efl.Canvas.Layout_Part_Box.pack_clear]]
  * [[:develop:api:efl:ui:box:method:pack_clear|Efl.Ui.Box.pack_clear]]
  * [[:develop:api:efl:ui:radio_box:method:pack_clear|Efl.Ui.Radio_Box.pack_clear]]
  * [[:develop:api:efl:ui:group_item:method:pack_clear|Efl.Ui.Group_Item.pack_clear]]
  * [[:develop:api:efl:ui:collection:method:pack_clear|Efl.Ui.Collection.pack_clear]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_clear|Efl.Ui.Layout_Part_Box.pack_clear]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_clear|Efl.Ui.Spotlight.Container.pack_clear]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_clear|Efl.Ui.Tab_Pager.pack_clear]]
  * [[:develop:api:efl:ui:relative_layout:method:pack_clear|Efl.Ui.Relative_Layout.pack_clear]]

