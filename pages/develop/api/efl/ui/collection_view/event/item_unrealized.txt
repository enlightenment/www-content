~~Title: Efl.Ui.Collection_View: item,unrealized~~

===== Description =====

%%Event triggered when the %%[[:develop:api:efl:ui:collection_view|Efl.Ui.Collection_View]]%% is about to give an %%[[:develop:api:efl:ui:item|Efl.Ui.Item]]%% back to the %%[[:develop:api:efl:ui:factory|Efl.Ui.Factory]]%%.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:collection_view:event:item_unrealized:description&nouser&nolink&nodate}}

===== Signature =====

<code>
item,unrealized: Efl.Ui.Item;
</code>

===== C information =====

<code c>
EFL_UI_COLLECTION_VIEW_EVENT_ITEM_UNREALIZED(Efl_Ui_Item *)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_collection_view_event_item_unrealized(void *data, const Efl_Event *event)
{
    Efl_Ui_Item *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_COLLECTION_VIEW_EVENT_ITEM_UNREALIZED, on_efl_ui_collection_view_event_item_unrealized, d);
}

</code>
