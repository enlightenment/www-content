~~Title: Efl.Ui.Collection_View.match_content~~
====== Efl.Ui.Collection_View.match_content ======

===== Values =====

  * **w** - %%Whether to limit the minimum horizontal size.%%
  * **h** - %%Whether to limit the minimum vertical size.%%


\\ {{page>:develop:api-include:efl:ui:collection_view:property:match_content:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:scrollable:property:match_content|Efl.Ui.Scrollable.match_content]] **(set)**.//===== Signature =====

<code>
@property match_content @pure_virtual {
    set {}
}
</code>

===== C signature =====

<code c>
void efl_ui_scrollable_match_content_set(Eo *obj, Eina_Bool w, Eina_Bool h);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:match_content|Efl.Ui.Scrollable.match_content]]
  * [[:develop:api:efl:ui:collection_view:property:match_content|Efl.Ui.Collection_View.match_content]]
  * [[:develop:api:efl:ui:scroller:property:match_content|Efl.Ui.Scroller.match_content]]
  * [[:develop:api:efl:ui:collection:property:match_content|Efl.Ui.Collection.match_content]]
  * [[:develop:api:efl:ui:scroll:manager:property:match_content|Efl.Ui.Scroll.Manager.match_content]]

