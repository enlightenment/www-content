~~Title: Efl.Ui.Datepicker.date_min~~
====== Efl.Ui.Datepicker.date_min ======

===== Description =====

%%The lower boundary of date.%%

%%%%''year''%%: Year. The year range is from 1900 to 2137.%%

%%%%''month''%%: Month. The month range is from 1 to 12.%%

%%%%''day''%%: Day. The day range is from 1 to 31 according to %%''month''%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:datepicker:property:date_min:description&nouser&nolink&nodate}}

===== Values =====

  * **year** - %%The year value.%%
  * **month** - %%The month value from 1 to 12.%%
  * **day** - %%The day value from 1 to 31.%%

===== Signature =====

<code>
@property date_min {
    get {}
    set {}
    values {
        year: int;
        month: int;
        day: int;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_datepicker_date_min_get(const Eo *obj, int *year, int *month, int *day);
void efl_ui_datepicker_date_min_set(Eo *obj, int year, int month, int day);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:datepicker:property:date_min|Efl.Ui.Datepicker.date_min]]

