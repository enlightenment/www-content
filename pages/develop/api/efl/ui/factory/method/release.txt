~~Title: Efl.Ui.Factory.release~~
====== Efl.Ui.Factory.release ======

===== Description =====

%%Release a UI object and disconnect from models.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:factory:method:release:description&nouser&nolink&nodate}}

===== Signature =====

<code>
release @pure_virtual {
    params {
        @in ui_views: iterator<Efl.Gfx.Entity>;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_factory_release(Eo *obj, Eina_Iterator *ui_views);
</code>

===== Parameters =====

  * **ui_views** //(in)// - %%Object to remove.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:factory:method:release|Efl.Ui.Factory.release]]
  * [[:develop:api:efl:ui:widget_factory:method:release|Efl.Ui.Widget_Factory.release]]
  * [[:develop:api:efl:ui:caching_factory:method:release|Efl.Ui.Caching_Factory.release]]

