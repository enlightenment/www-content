~~Title: Efl.Ui.Focus.Manager.dirty_logic_freeze~~
====== Efl.Ui.Focus.Manager.dirty_logic_freeze ======

===== Description =====

%%Disables the cache invalidation when an object is moved.%%

%%Even if an object is moved, the focus manager will not recalculate its relations. This can be used when you know that the set of widgets in the focus manager is moved the same way, so the relations between the widgets in the set do not change and complex calculations can be avoided. Use %%[[:develop:api:efl:ui:focus:manager:method:dirty_logic_unfreeze|Efl.Ui.Focus.Manager.dirty_logic_unfreeze]]%% to re-enable relationship calculation.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:dirty_logic_freeze:description&nouser&nolink&nodate}}

===== Signature =====

<code>
dirty_logic_freeze @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_ui_focus_manager_dirty_logic_freeze(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:dirty_logic_freeze|Efl.Ui.Focus.Manager.dirty_logic_freeze]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:dirty_logic_freeze|Efl.Ui.Focus.Manager_Calc.dirty_logic_freeze]]

