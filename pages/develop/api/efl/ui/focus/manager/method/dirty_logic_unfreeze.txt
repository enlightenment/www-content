~~Title: Efl.Ui.Focus.Manager.dirty_logic_unfreeze~~
====== Efl.Ui.Focus.Manager.dirty_logic_unfreeze ======

===== Description =====

%%Enables the cache invalidation when an object is moved.%%

%%This is the counterpart to %%[[:develop:api:efl:ui:focus:manager:method:dirty_logic_freeze|Efl.Ui.Focus.Manager.dirty_logic_freeze]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:dirty_logic_unfreeze:description&nouser&nolink&nodate}}

===== Signature =====

<code>
dirty_logic_unfreeze @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_ui_focus_manager_dirty_logic_unfreeze(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:dirty_logic_unfreeze|Efl.Ui.Focus.Manager.dirty_logic_unfreeze]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:dirty_logic_unfreeze|Efl.Ui.Focus.Manager_Calc.dirty_logic_unfreeze]]

