~~Title: Efl.Ui.Focus.Manager.move~~
====== Efl.Ui.Focus.Manager.move ======

===== Description =====

%%Moves the focus in the given direction to the next regular widget.%%

%%This call flushes all changes. This means all changes since last flush are computed.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:move:description&nouser&nolink&nodate}}

===== Signature =====

<code>
move @pure_virtual {
    params {
        @in direction: Efl.Ui.Focus.Direction;
    }
    return: Efl.Ui.Focus.Object;
}
</code>

===== C signature =====

<code c>
Efl_Ui_Focus_Object *efl_ui_focus_manager_move(Eo *obj, Efl_Ui_Focus_Direction direction);
</code>

===== Parameters =====

  * **direction** //(in)// - %%The direction to move to.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:move|Efl.Ui.Focus.Manager.move]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:move|Efl.Ui.Focus.Manager_Calc.move]]
  * [[:develop:api:efl:ui:focus:manager_root_focus:method:move|Efl.Ui.Focus.Manager_Root_Focus.move]]
  * [[:develop:api:efl:ui:focus:manager_sub:method:move|Efl.Ui.Focus.Manager_Sub.move]]
  * [[:develop:api:efl:ui:collection_view:method:move|Efl.Ui.Collection_View.move]]
  * [[:develop:api:efl:ui:collection:method:move|Efl.Ui.Collection.move]]
  * [[:develop:api:efl:ui:focus:layer:method:move|Efl.Ui.Focus.Layer.move]]

