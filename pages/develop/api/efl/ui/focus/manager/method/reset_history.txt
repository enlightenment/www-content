~~Title: Efl.Ui.Focus.Manager.reset_history~~
====== Efl.Ui.Focus.Manager.reset_history ======

===== Description =====

%%Resets the history stack of this manager object. This means the uppermost element will be unfocused, and all other elements will be removed from the remembered list.%%

%%You should focus another element immediately after calling this, in order to always have a focused object.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:manager:method:reset_history:description&nouser&nolink&nodate}}

===== Signature =====

<code>
reset_history @pure_virtual {}
</code>

===== C signature =====

<code c>
void efl_ui_focus_manager_reset_history(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:method:reset_history|Efl.Ui.Focus.Manager.reset_history]]
  * [[:develop:api:efl:ui:focus:manager_calc:method:reset_history|Efl.Ui.Focus.Manager_Calc.reset_history]]

