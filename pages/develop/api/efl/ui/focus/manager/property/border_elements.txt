~~Title: Efl.Ui.Focus.Manager.border_elements~~
====== Efl.Ui.Focus.Manager.border_elements ======

===== Values =====

  * **border_elements** - %%An iterator over the border objects.%%


\\ {{page>:develop:api-include:efl:ui:focus:manager:property:border_elements:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property border_elements @pure_virtual {
    get {}
    values {
        border_elements: iterator<Efl.Ui.Focus.Object>;
    }
}
</code>

===== C signature =====

<code c>
Eina_Iterator *efl_ui_focus_manager_border_elements_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:manager:property:border_elements|Efl.Ui.Focus.Manager.border_elements]]
  * [[:develop:api:efl:ui:focus:manager_calc:property:border_elements|Efl.Ui.Focus.Manager_Calc.border_elements]]
  * [[:develop:api:efl:ui:focus:manager_root_focus:property:border_elements|Efl.Ui.Focus.Manager_Root_Focus.border_elements]]
  * [[:develop:api:elm:interface_scrollable:property:border_elements|Elm.Interface_Scrollable.border_elements]]

