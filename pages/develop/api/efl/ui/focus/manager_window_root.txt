~~Title: Efl.Ui.Focus.Manager_Window_Root~~
====== Efl.Ui.Focus.Manager_Window_Root (interface) ======

===== Description =====

%%An interface to indicate the end of a focus chain.%%

%%Focus managers are ensuring that if they give focus to something, that is registered in the upper focus manager. The uppermost focus manager does not need to do that, and can implement this interface to indicate so.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:manager_window_root:description&nouser&nolink&nodate}}

===== Members =====

===== Events =====

