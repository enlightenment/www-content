~~Title: Efl.Ui.Focus.Object~~
====== Efl.Ui.Focus.Object (mixin) ======

===== Description =====

%%Functions of focusable objects.%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:focus:object:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:focus:object:property:focus|focus]]** //**(get, set)**// ''protected set''\\
> %%Whether the widget is currently focused or not.%%
<code c>
Eina_Bool efl_ui_focus_object_focus_get(const Eo *obj);
void efl_ui_focus_object_focus_set(Eo *obj, Eina_Bool focus);
</code>
\\
**[[:develop:api:efl:ui:focus:object:property:focus_geometry|focus_geometry]]** //**(get)**//\\
> 
<code c>
Eina_Rect efl_ui_focus_object_focus_geometry_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:object:property:focus_manager|focus_manager]]** //**(get)**//\\
> 
<code c>
Efl_Ui_Focus_Manager *efl_ui_focus_object_focus_manager_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:object:property:focus_parent|focus_parent]]** //**(get)**//\\
> 
<code c>
Efl_Ui_Focus_Object *efl_ui_focus_object_focus_parent_get(const Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:object:method:setup_order|setup_order]]**\\
> %%Tells the object that its children will be queried soon by the focus manager. Overwrite this to have a chance to update the order of the children. Deleting items in this call will result in undefined behaviour and may cause your system to crash.%%
<code c>
void efl_ui_focus_object_setup_order(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:object:property:child_focus|child_focus]]** //**(get, set)**// ''protected''\\
> %%Indicates if a child of this object has focus set to true.%%
<code c>
Eina_Bool efl_ui_focus_object_child_focus_get(const Eo *obj);
void efl_ui_focus_object_child_focus_set(Eo *obj, Eina_Bool child_focus);
</code>
\\
**[[:develop:api:efl:ui:focus:object:method:on_focus_update|on_focus_update]]** ''protected''\\
> %%Virtual function handling focus in/out events on the widget.%%
<code c>
Eina_Bool efl_ui_focus_object_on_focus_update(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:focus:object:method:setup_order_non_recursive|setup_order_non_recursive]]** ''protected''\\
> %%This is called when %%[[:develop:api:efl:ui:focus:object:method:setup_order|Efl.Ui.Focus.Object.setup_order]]%% is called, but only on the first call, additional recursive calls to %%[[:develop:api:efl:ui:focus:object:method:setup_order|Efl.Ui.Focus.Object.setup_order]]%% will not call this function again.%%
<code c>
void efl_ui_focus_object_setup_order_non_recursive(Eo *obj);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:focus:object:event:child_focus_changed|child_focus,changed]]**\\
> %%Emitted if child_focus has changed.%%
<code c>
EFL_UI_FOCUS_OBJECT_EVENT_CHILD_FOCUS_CHANGED(Eina_Bool)
</code>
\\ **[[:develop:api:efl:ui:focus:object:event:focus_changed|focus,changed]]**\\
> %%Emitted if the focus state has changed.%%
<code c>
EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_CHANGED(Eina_Bool)
</code>
\\ **[[:develop:api:efl:ui:focus:object:event:focus_geometry_changed|focus_geometry,changed]]**\\
> %%Emitted if focus geometry of this object has changed.%%
<code c>
EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_GEOMETRY_CHANGED(Eina_Rect)
</code>
\\ **[[:develop:api:efl:ui:focus:object:event:focus_manager_changed|focus_manager,changed]]**\\
> %%Emitted when a new manager is the parent for this object.%%
<code c>
EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_MANAGER_CHANGED(Efl_Ui_Focus_Manager *)
</code>
\\ **[[:develop:api:efl:ui:focus:object:event:focus_parent_changed|focus_parent,changed]]**\\
> %%Emitted when a new logical parent should be used.%%
<code c>
EFL_UI_FOCUS_OBJECT_EVENT_FOCUS_PARENT_CHANGED(Efl_Ui_Focus_Object *)
</code>
\\ 