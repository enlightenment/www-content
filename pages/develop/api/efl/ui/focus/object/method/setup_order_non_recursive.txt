~~Title: Efl.Ui.Focus.Object.setup_order_non_recursive~~
====== Efl.Ui.Focus.Object.setup_order_non_recursive ======

===== Description =====

%%This is called when %%[[:develop:api:efl:ui:focus:object:method:setup_order|Efl.Ui.Focus.Object.setup_order]]%% is called, but only on the first call, additional recursive calls to %%[[:develop:api:efl:ui:focus:object:method:setup_order|Efl.Ui.Focus.Object.setup_order]]%% will not call this function again.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:object:method:setup_order_non_recursive:description&nouser&nolink&nodate}}

===== Signature =====

<code>
setup_order_non_recursive @protected {}
</code>

===== C signature =====

<code c>
void efl_ui_focus_object_setup_order_non_recursive(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:object:method:setup_order_non_recursive|Efl.Ui.Focus.Object.setup_order_non_recursive]]

