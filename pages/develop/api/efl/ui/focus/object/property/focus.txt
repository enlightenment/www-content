~~Title: Efl.Ui.Focus.Object.focus~~
====== Efl.Ui.Focus.Object.focus ======

===== Description =====

%%Whether the widget is currently focused or not.%%

//Since 1.22//


{{page>:develop:api-include:efl:ui:focus:object:property:focus:description&nouser&nolink&nodate}}

===== Values =====

  * **focus** - %%The focused state of the object.%%
==== Setter ====

%%This is called by the manager and should never be called by anyone else.%%

%%The function emits the focus state events, if focus is different to the previous state.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:focus:object:property:focus:getter_description&nouser&nolink&nodate}}


===== Signature =====

<code>
@property focus {
    get {}
    set @protected {}
    values {
        focus: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_focus_object_focus_get(const Eo *obj);
void efl_ui_focus_object_focus_set(Eo *obj, Eina_Bool focus);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:object:property:focus|Efl.Ui.Focus.Object.focus]]
  * [[:develop:api:efl:ui:widget:property:focus|Efl.Ui.Widget.focus]]
  * [[:develop:api:efl:ui:win:property:focus|Efl.Ui.Win.focus]]
  * [[:develop:api:efl:ui:focus:composition_adapter:property:focus|Efl.Ui.Focus.Composition_Adapter.focus]]
  * [[:develop:api:efl:ui:calendar_item:property:focus|Efl.Ui.Calendar_Item.focus]]

