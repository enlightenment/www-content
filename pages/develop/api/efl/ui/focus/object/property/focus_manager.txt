~~Title: Efl.Ui.Focus.Object.focus_manager~~
====== Efl.Ui.Focus.Object.focus_manager ======

===== Values =====

  * **manager** - %%The manager object.%%


\\ {{page>:develop:api-include:efl:ui:focus:object:property:focus_manager:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property focus_manager {
    get {}
    values {
        manager: Efl.Ui.Focus.Manager;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Focus_Manager *efl_ui_focus_object_focus_manager_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:object:property:focus_manager|Efl.Ui.Focus.Object.focus_manager]]
  * [[:develop:api:efl:ui:widget:property:focus_manager|Efl.Ui.Widget.focus_manager]]
  * [[:develop:api:efl:ui:focus:composition_adapter:property:focus_manager|Efl.Ui.Focus.Composition_Adapter.focus_manager]]

