~~Title: Efl.Ui.Format~~
====== Efl.Ui.Format (mixin) ======

===== Description =====

%%Helper mixin that simplifies converting numerical values to text.%%

%%A number of widgets represent a numerical value but display a text representation. For example, an %%[[:develop:api:efl:ui:progressbar|Efl.Ui.Progressbar]]%% can hold the number 0.75 but display the string "75%", or an %%[[:develop:api:efl:ui:spin|Efl.Ui.Spin]]%% can hold numbers 1 to 7, but display the strings "Monday" thru "Sunday".%%

%%This conversion can be controlled through the %%[[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]%%, %%[[:develop:api:efl:ui:format:property:format_values|Efl.Ui.Format.format_values]]%% and %%[[:develop:api:efl:ui:format:property:format_string|Efl.Ui.Format.format_string]]%% properties. Only one of them needs to be set. When more than one is set %%[[:develop:api:efl:ui:format:property:format_values|Efl.Ui.Format.format_values]]%% has the highest priority, followed by %%[[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]%% and then %%[[:develop:api:efl:ui:format:property:format_string|Efl.Ui.Format.format_string]]%%. If one mechanism fails to produce a valid string the others will be tried (if provided) in descending order of priority. If no user-provided mechanism works, a fallback is used that just displays the value.%%

%%Widgets including this mixin offer their users different properties to control how %%[[:develop:api:eina:value|Eina.Value]]%%'s are converted to text.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:format:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:format:method:destructor|destructor]]**// [Overridden from [[:develop:api:efl:object|Efl.Object]]]//\\
> 
<code c>
void efl_destructor(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:format:property:format_func|format_func]]** //**(get, set)**//\\
> %%User-provided function which takes care of converting an %%[[:develop:api:eina:value|Eina.Value]]%% into a text string. The user is then completely in control of how the string is generated, but it is the most cumbersome method to use. If the conversion fails the other mechanisms will be tried, according to their priorities.%%
<code c>
Efl_Ui_Format_Func efl_ui_format_func_get(const Eo *obj);
void efl_ui_format_func_set(Eo *obj, Efl_Ui_Format_Func func);
</code>
\\
**[[:develop:api:efl:ui:format:property:format_string|format_string]]** //**(get, set)**//\\
> %%A user-provided, string used to format the numerical value.%%
<code c>
void efl_ui_format_string_get(const Eo *obj, const char **string, Efl_Ui_Format_String_Type *type);
void efl_ui_format_string_set(Eo *obj, const char *string, Efl_Ui_Format_String_Type type);
</code>
\\
**[[:develop:api:efl:ui:format:property:format_values|format_values]]** //**(get, set)**//\\
> %%User-provided list of values which are to be rendered using specific text strings. This is more convenient to use than %%[[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]%% and is perfectly suited for cases where the strings make more sense than the numerical values. For example, weekday names ("Monday", "Tuesday", ...) are friendlier than numbers 1 to 7. If a value is not found in the list, the other mechanisms will be tried according to their priorities. List members do not need to be in any particular order. They are sorted internally for performance reasons.%%
<code c>
Eina_Accessor *efl_ui_format_values_get(const Eo *obj);
void efl_ui_format_values_set(Eo *obj, Eina_Accessor *values);
</code>
\\
**[[:develop:api:efl:ui:format:method:apply_formatted_value|apply_formatted_value]]** ''protected''\\
> %%Internal method to be implemented by widgets including this mixin.%%
<code c>
void efl_ui_format_apply_formatted_value(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:format:method:decimal_places_get|decimal_places_get]]** ''protected''\\
> %%Internal method to be used by widgets including this mixin. It can only be used when a %%[[:develop:api:efl:ui:format:property:format_string|Efl.Ui.Format.format_string]]%% has been supplied, and it returns the number of decimal places that the format string will produce for floating point values.%%
<code c>
int efl_ui_format_decimal_places_get(Eo *obj);
</code>
\\
**[[:develop:api:efl:ui:format:method:formatted_value_get|formatted_value_get]]** ''protected''\\
> %%Internal method to be used by widgets including this mixin to perform the conversion from the internal numerical value into the text representation (Users of these widgets do not need to call this method).%%
<code c>
void efl_ui_format_formatted_value_get(Eo *obj, Eina_Strbuf *str, const Eina_Value value);
</code>
\\

===== Events =====

