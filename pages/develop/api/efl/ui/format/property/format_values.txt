~~Title: Efl.Ui.Format.format_values~~
====== Efl.Ui.Format.format_values ======

===== Description =====

%%User-provided list of values which are to be rendered using specific text strings. This is more convenient to use than %%[[:develop:api:efl:ui:format:property:format_func|Efl.Ui.Format.format_func]]%% and is perfectly suited for cases where the strings make more sense than the numerical values. For example, weekday names ("Monday", "Tuesday", ...) are friendlier than numbers 1 to 7. If a value is not found in the list, the other mechanisms will be tried according to their priorities. List members do not need to be in any particular order. They are sorted internally for performance reasons.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:format:property:format_values:description&nouser&nolink&nodate}}

===== Values =====

  * **values** - %%Accessor over a list of value-text pairs. The method will dispose of the accessor, but not of its contents. For convenience, Eina offers a range of helper methods to obtain accessors from Eina.Array, Eina.List or even plain C arrays.%%

===== Signature =====

<code>
@property format_values {
    get {}
    set {}
    values {
        values: accessor<Efl.Ui.Format_Value>;
    }
}
</code>

===== C signature =====

<code c>
Eina_Accessor *efl_ui_format_values_get(const Eo *obj);
void efl_ui_format_values_set(Eo *obj, Eina_Accessor *values);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:format:property:format_values|Efl.Ui.Format.format_values]]

