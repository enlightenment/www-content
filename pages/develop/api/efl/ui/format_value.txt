~~Title: Efl.Ui.Format_Value~~

===== Description =====

%%A value which should always be displayed as a specific text string. See %%[[:develop:api:efl:ui:format:property:format_values|Efl.Ui.Format.format_values]]%%.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:format_value:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:format_value:fields&nouser&nolink&nodate}}

  * **value** - %%Input value.%%
  * **text** - %%Text string to replace it.%%

===== Signature =====

<code>
struct Efl.Ui.Format_Value {
    value: int;
    text: string;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Ui_Format_Value {
    int value;
    const char *text;
} Efl_Ui_Format_Value;
</code>
