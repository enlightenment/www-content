~~Title: Efl.Ui.Image: drop~~

===== Description =====

%%Called when drop from drag and drop happened%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:image:event:drop:description&nouser&nolink&nodate}}

===== Signature =====

<code>
drop @beta: string;
</code>

===== C information =====

<code c>
EFL_UI_IMAGE_EVENT_DROP(const char *, @beta)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_image_event_drop(void *data, const Efl_Event *event)
{
    const char *info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_IMAGE_EVENT_DROP, on_efl_ui_image_event_drop, d);
}

</code>
