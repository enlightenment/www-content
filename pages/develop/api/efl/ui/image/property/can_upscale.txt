~~Title: Efl.Ui.Image.can_upscale~~
====== Efl.Ui.Image.can_upscale ======

===== Description =====

%%If %%''true''%%, the image may be scaled to a larger size. If %%''false''%%, the image will never be resized larger than its native size.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:can_upscale:description&nouser&nolink&nodate}}

===== Values =====

  * **upscale** - %%Whether to allow image upscaling.%%

//Overridden from [[:develop:api:efl:gfx:image:property:can_upscale|Efl.Gfx.Image.can_upscale]] **(get, set)**.//===== Signature =====

<code>
@property can_upscale @pure_virtual {
    get {}
    set {}
    values {
        upscale: bool (true);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_image_can_upscale_get(const Eo *obj);
void efl_gfx_image_can_upscale_set(Eo *obj, Eina_Bool upscale);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:can_upscale|Efl.Gfx.Image.can_upscale]]
  * [[:develop:api:efl:ui:image:property:can_upscale|Efl.Ui.Image.can_upscale]]

