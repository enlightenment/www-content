~~Title: Efl.Ui.Image.content_hint~~
====== Efl.Ui.Image.content_hint ======

===== Description =====

%%Content hint setting for the image. These hints might be used by EFL to enable optimizations.%%

%%For example, if you're on the GL engine and your driver implementation supports it, setting this hint to %%[[:develop:api:efl:gfx:image_content_hint|Efl.Gfx.Image_Content_Hint.dynamic]]%% will make it need zero copies at texture upload time, which is an "expensive" operation.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:content_hint:description&nouser&nolink&nodate}}

===== Values =====

  * **hint** - %%Dynamic or static content hint.%%

//Overridden from [[:develop:api:efl:gfx:image:property:content_hint|Efl.Gfx.Image.content_hint]] **(get, set)**.//===== Signature =====

<code>
@property content_hint @pure_virtual {
    get {}
    set {}
    values {
        hint: Efl.Gfx.Image_Content_Hint (Efl.Gfx.Image_Content_Hint.none);
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Image_Content_Hint efl_gfx_image_content_hint_get(const Eo *obj);
void efl_gfx_image_content_hint_set(Eo *obj, Efl_Gfx_Image_Content_Hint hint);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:content_hint|Efl.Gfx.Image.content_hint]]
  * [[:develop:api:efl:canvas:image_internal:property:content_hint|Efl.Canvas.Image_Internal.content_hint]]
  * [[:develop:api:efl:ui:image:property:content_hint|Efl.Ui.Image.content_hint]]

