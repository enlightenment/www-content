~~Title: Efl.Ui.Image.load_region_support~~
====== Efl.Ui.Image.load_region_support ======

===== Values =====

  * **support** - %%%%''true''%% if region load of the image is supported, %%''false''%% otherwise.%%


\\ {{page>:develop:api-include:efl:ui:image:property:load_region_support:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:gfx:image_load_controller:property:load_region_support|Efl.Gfx.Image_Load_Controller.load_region_support]] **(get)**.//===== Signature =====

<code>
@property load_region_support @pure_virtual {
    get {}
    values {
        support: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_gfx_image_load_controller_load_region_support_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image_load_controller:property:load_region_support|Efl.Gfx.Image_Load_Controller.load_region_support]]
  * [[:develop:api:efl:canvas:image:property:load_region_support|Efl.Canvas.Image.load_region_support]]
  * [[:develop:api:efl:ui:image:property:load_region_support|Efl.Ui.Image.load_region_support]]

