~~Title: Efl.Ui.Image.load_scale_down~~
====== Efl.Ui.Image.load_scale_down ======

===== Description =====

%%The scale down factor is a divider on the original image size.%%

%%Setting the scale down factor can reduce load time and memory usage at the cost of having a scaled down image in memory.%%

%%This function sets the scale down factor of a given canvas image. Most useful for the SVG image loader but also applies to JPEG, PNG and BMP.%%

%%Powers of two (2, 4, 8, ...) are best supported (especially with JPEG).%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:load_scale_down:description&nouser&nolink&nodate}}

===== Values =====

  * **div** - %%The scale down dividing factor.%%

//Overridden from [[:develop:api:efl:gfx:image_load_controller:property:load_scale_down|Efl.Gfx.Image_Load_Controller.load_scale_down]] **(get, set)**.//===== Signature =====

<code>
@property load_scale_down @pure_virtual {
    get {}
    set {}
    values {
        div: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_gfx_image_load_controller_load_scale_down_get(const Eo *obj);
void efl_gfx_image_load_controller_load_scale_down_set(Eo *obj, int div);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image_load_controller:property:load_scale_down|Efl.Gfx.Image_Load_Controller.load_scale_down]]
  * [[:develop:api:efl:canvas:image:property:load_scale_down|Efl.Canvas.Image.load_scale_down]]
  * [[:develop:api:efl:ui:image:property:load_scale_down|Efl.Ui.Image.load_scale_down]]

