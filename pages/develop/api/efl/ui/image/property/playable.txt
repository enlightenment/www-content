~~Title: Efl.Ui.Image.playable~~
====== Efl.Ui.Image.playable ======

===== Values =====

  * **playable** - No description supplied.


\\ {{page>:develop:api-include:efl:ui:image:property:playable:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:playable:property:playable|Efl.Playable.playable]] **(get)**.//===== Signature =====

<code>
@property playable @pure_virtual {
    get {}
    values {
        playable: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_playable_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:playable:property:playable|Efl.Playable.playable]]
  * [[:develop:api:efl:canvas:animation_player:property:playable|Efl.Canvas.Animation_Player.playable]]
  * [[:develop:api:efl:canvas:layout:property:playable|Efl.Canvas.Layout.playable]]
  * [[:develop:api:efl:ui:image:property:playable|Efl.Ui.Image.playable]]
  * [[:develop:api:efl:ui:image_zoomable:property:playable|Efl.Ui.Image_Zoomable.playable]]
  * [[:develop:api:efl:canvas:animation:property:playable|Efl.Canvas.Animation.playable]]

