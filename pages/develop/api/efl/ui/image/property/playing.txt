~~Title: Efl.Ui.Image.playing~~
====== Efl.Ui.Image.playing ======

===== Description =====

%%Playback state of the media file.%%

%%This property sets the playback state of the object. Re-setting the current playback state has no effect.%%

%%If set to %%''false''%%, the object's %%[[:develop:api:efl:player:property:playback_progress|Efl.Player.playback_progress]]%% property is, by default, reset to %%''0.0''%%. A class may alter this behavior, and it will be stated in the documentation for a class if such behavior changes should be expected.%%

%%Applying the %%''false''%% playing state also has the same effect as the player object reaching the end of its playback, which may invoke additional behavior based on a class's implementation.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:playing:description&nouser&nolink&nodate}}

===== Values =====

  * **playing** - %%%%''true''%% if playing, %%''false''%% otherwise.%%

//Overridden from [[:develop:api:efl:player:property:playing|Efl.Player.playing]] **(get, set)**.//===== Signature =====

<code>
@property playing @pure_virtual {
    get {}
    set {
        return: bool (false);
    }
    values {
        playing: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_player_playing_get(const Eo *obj);
Eina_Bool efl_player_playing_set(Eo *obj, Eina_Bool playing);
</code>

===== Implemented by =====

  * [[:develop:api:efl:player:property:playing|Efl.Player.playing]]
  * [[:develop:api:efl:canvas:video:property:playing|Efl.Canvas.Video.playing]]
  * [[:develop:api:efl:canvas:animation_player:property:playing|Efl.Canvas.Animation_Player.playing]]
  * [[:develop:api:efl:ui:image:property:playing|Efl.Ui.Image.playing]]
  * [[:develop:api:efl:ui:image_zoomable:property:playing|Efl.Ui.Image_Zoomable.playing]]
  * [[:develop:api:efl:ui:video:property:playing|Efl.Ui.Video.playing]]

