~~Title: Efl.Ui.Image.scale_hint~~
====== Efl.Ui.Image.scale_hint ======

===== Description =====

%%The scale hint of a given image of the canvas.%%

%%The scale hint affects how EFL is to cache scaled versions of its original source image.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image:property:scale_hint:description&nouser&nolink&nodate}}

===== Values =====

  * **hint** - %%Scalable or static size hint.%%

//Overridden from [[:develop:api:efl:gfx:image:property:scale_hint|Efl.Gfx.Image.scale_hint]] **(get, set)**.//===== Signature =====

<code>
@property scale_hint @pure_virtual {
    get {}
    set {}
    values {
        hint: Efl.Gfx.Image_Scale_Hint (Efl.Gfx.Image_Scale_Hint.none);
    }
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Image_Scale_Hint efl_gfx_image_scale_hint_get(const Eo *obj);
void efl_gfx_image_scale_hint_set(Eo *obj, Efl_Gfx_Image_Scale_Hint hint);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:image:property:scale_hint|Efl.Gfx.Image.scale_hint]]
  * [[:develop:api:efl:canvas:image_internal:property:scale_hint|Efl.Canvas.Image_Internal.scale_hint]]
  * [[:develop:api:efl:ui:image:property:scale_hint|Efl.Ui.Image.scale_hint]]

