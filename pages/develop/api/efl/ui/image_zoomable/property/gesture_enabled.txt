~~Title: Efl.Ui.Image_Zoomable.gesture_enabled~~
====== Efl.Ui.Image_Zoomable.gesture_enabled ======

===== Description =====

%%The gesture state for photocam.%%

%%This sets the gesture state to on or off for photocam. The default is off. This will start multi touch zooming.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:image_zoomable:property:gesture_enabled:description&nouser&nolink&nodate}}

===== Values =====

  * **gesture** - %%The gesture state.%%

===== Signature =====

<code>
@property gesture_enabled {
    get {}
    set {}
    values {
        gesture: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_image_zoomable_gesture_enabled_get(const Eo *obj);
void efl_ui_image_zoomable_gesture_enabled_set(Eo *obj, Eina_Bool gesture);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:image_zoomable:property:gesture_enabled|Efl.Ui.Image_Zoomable.gesture_enabled]]

