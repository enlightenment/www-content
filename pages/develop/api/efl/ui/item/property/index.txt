~~Title: Efl.Ui.Item.index~~
====== Efl.Ui.Item.index ======

===== Values =====

  * **index** - %%The index where to find this item in its %%[[:develop:api:efl:ui:item:property:container|Efl.Ui.Item.container]]%%.%%


\\ {{page>:develop:api-include:efl:ui:item:property:index:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property index {
    get {}
    values {
        index: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_ui_item_index_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:item:property:index|Efl.Ui.Item.index]]

