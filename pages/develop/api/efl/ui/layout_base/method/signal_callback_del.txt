~~Title: Efl.Ui.Layout_Base.signal_callback_del~~
====== Efl.Ui.Layout_Base.signal_callback_del ======

===== Description =====

%%Removes a signal-triggered callback from an object.%%

%%This function removes a callback, previously attached to the emission of a signal, from the object  obj. The parameters emission, source and func must match exactly those passed to a previous call to %%[[:develop:api:efl:layout:signal:method:signal_callback_add|Efl.Layout.Signal.signal_callback_add]]%%().%%

%%See %%[[:develop:api:efl:layout:signal:method:signal_callback_add|Efl.Layout.Signal.signal_callback_add]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:method:signal_callback_del:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:signal:method:signal_callback_del|Efl.Layout.Signal.signal_callback_del]].//===== Signature =====

<code>
signal_callback_del @pure_virtual {
    params {
        @in emission: string;
        @in source: string;
        @in func: EflLayoutSignalCb;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_layout_signal_callback_del(Eo *obj, const char *emission, const char *source, EflLayoutSignalCb func);
</code>

===== Parameters =====

  * **emission** //(in)// - %%The signal's "emission" string%%
  * **source** //(in)// - %%The signal's "source" string%%
  * **func** //(in)// - %%The callback function to be executed when the signal is emitted.%%

===== Implemented by =====

  * [[:develop:api:efl:layout:signal:method:signal_callback_del|Efl.Layout.Signal.signal_callback_del]]
  * [[:develop:api:efl:canvas:layout:method:signal_callback_del|Efl.Canvas.Layout.signal_callback_del]]
  * [[:develop:api:efl:ui:image:method:signal_callback_del|Efl.Ui.Image.signal_callback_del]]
  * [[:develop:api:efl:ui:image_zoomable:method:signal_callback_del|Efl.Ui.Image_Zoomable.signal_callback_del]]
  * [[:develop:api:efl:ui:layout_base:method:signal_callback_del|Efl.Ui.Layout_Base.signal_callback_del]]
  * [[:develop:api:efl:ui:text:method:signal_callback_del|Efl.Ui.Text.signal_callback_del]]

