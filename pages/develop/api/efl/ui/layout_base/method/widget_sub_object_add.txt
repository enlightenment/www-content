~~Title: Efl.Ui.Layout_Base.widget_sub_object_add~~
====== Efl.Ui.Layout_Base.widget_sub_object_add ======

===== Description =====

%%Virtual function customizing sub objects being added.%%

%%When a widget is added as a sub-object of another widget (like list elements inside a list container, for example) some of its properties are automatically adapted to the parent's current values (like focus, access, theme, scale, mirror, scrollable child get, translate, display mode set, tree dump). Override this method if you want to customize differently sub-objects being added to this object.%%

%%Sub objects can be any canvas object, not necessarily widgets.%%

%%See also %%[[:develop:api:efl:ui:widget:property:widget_parent|Efl.Ui.Widget.widget_parent]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_base:method:widget_sub_object_add:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:widget:method:widget_sub_object_add|Efl.Ui.Widget.widget_sub_object_add]].//===== Signature =====

<code>
widget_sub_object_add @protected {
    params {
        @in sub_obj: Efl.Canvas.Object;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_widget_sub_object_add(Eo *obj, Efl_Canvas_Object *sub_obj);
</code>

===== Parameters =====

  * **sub_obj** //(in)// - %%Sub object to be added. Not necessarily a widget itself.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:widget_sub_object_add|Efl.Ui.Widget.widget_sub_object_add]]
  * [[:develop:api:efl:ui:flip:method:widget_sub_object_add|Efl.Ui.Flip.widget_sub_object_add]]
  * [[:develop:api:efl:ui:layout_base:method:widget_sub_object_add|Efl.Ui.Layout_Base.widget_sub_object_add]]

