~~Title: Efl.Ui.Layout_Base.group_size_min~~
====== Efl.Ui.Layout_Base.group_size_min ======

===== Values =====

  * **min** - %%The minimum size as set in EDC.%%


\\ {{page>:develop:api-include:efl:ui:layout_base:property:group_size_min:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:layout:group:property:group_size_min|Efl.Layout.Group.group_size_min]] **(get)**.//===== Signature =====

<code>
@property group_size_min @pure_virtual {
    get {}
    values {
        min: Eina.Size2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Size2D efl_layout_group_size_min_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:layout:group:property:group_size_min|Efl.Layout.Group.group_size_min]]
  * [[:develop:api:efl:canvas:layout:property:group_size_min|Efl.Canvas.Layout.group_size_min]]
  * [[:develop:api:efl:ui:image:property:group_size_min|Efl.Ui.Image.group_size_min]]
  * [[:develop:api:efl:ui:image_zoomable:property:group_size_min|Efl.Ui.Image_Zoomable.group_size_min]]
  * [[:develop:api:efl:ui:layout_base:property:group_size_min|Efl.Ui.Layout_Base.group_size_min]]

