~~Title: Efl.Ui.Layout_Part_Box.content_count~~
====== Efl.Ui.Layout_Part_Box.content_count ======

===== Description =====

%%Returns the number of contained sub-objects.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_part_box:method:content_count:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:container:method:content_count|Efl.Container.content_count]].//===== Signature =====

<code>
content_count @pure_virtual {
    return: int;
}
</code>

===== C signature =====

<code c>
int efl_content_count(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:container:method:content_count|Efl.Container.content_count]]
  * [[:develop:api:efl:canvas:layout:method:content_count|Efl.Canvas.Layout.content_count]]
  * [[:develop:api:efl:ui:tab_bar:method:content_count|Efl.Ui.Tab_Bar.content_count]]
  * [[:develop:api:efl:ui:table:method:content_count|Efl.Ui.Table.content_count]]
  * [[:develop:api:efl:canvas:layout_part_table:method:content_count|Efl.Canvas.Layout_Part_Table.content_count]]
  * [[:develop:api:efl:ui:layout_part_table:method:content_count|Efl.Ui.Layout_Part_Table.content_count]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:content_count|Efl.Canvas.Layout_Part_Invalid.content_count]]
  * [[:develop:api:efl:ui:flip:method:content_count|Efl.Ui.Flip.content_count]]
  * [[:develop:api:efl:canvas:layout_part_box:method:content_count|Efl.Canvas.Layout_Part_Box.content_count]]
  * [[:develop:api:efl:ui:box:method:content_count|Efl.Ui.Box.content_count]]
  * [[:develop:api:efl:ui:group_item:method:content_count|Efl.Ui.Group_Item.content_count]]
  * [[:develop:api:efl:ui:collection:method:content_count|Efl.Ui.Collection.content_count]]
  * [[:develop:api:efl:ui:layout_part_box:method:content_count|Efl.Ui.Layout_Part_Box.content_count]]
  * [[:develop:api:efl:ui:spotlight:container:method:content_count|Efl.Ui.Spotlight.Container.content_count]]
  * [[:develop:api:efl:ui:relative_layout:method:content_count|Efl.Ui.Relative_Layout.content_count]]
  * [[:develop:api:efl:ui:layout_base:method:content_count|Efl.Ui.Layout_Base.content_count]]

