~~Title: Efl.Ui.Layout_Part_Box.pack_after~~
====== Efl.Ui.Layout_Part_Box.pack_after ======

===== Description =====

%%Append an object after the %%''existing''%% sub-object.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

%%If %%''existing''%% is %%''NULL''%% this method behaves like %%[[:develop:api:efl:pack_linear:method:pack_end|Efl.Pack_Linear.pack_end]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:layout_part_box:method:pack_after:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_linear:method:pack_after|Efl.Pack_Linear.pack_after]].//===== Signature =====

<code>
pack_after @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
        @in existing: const(Efl.Gfx.Entity);
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_after(Eo *obj, Efl_Gfx_Entity *subobj, const Efl_Gfx_Entity *existing);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%Object to pack after %%''existing''%%.%%
  * **existing** //(in)// - %%Existing reference sub-object. Must already belong to the container or be %%''NULL''%%.%%

===== Implemented by =====

  * [[:develop:api:efl:pack_linear:method:pack_after|Efl.Pack_Linear.pack_after]]
  * [[:develop:api:efl:ui:tab_bar:method:pack_after|Efl.Ui.Tab_Bar.pack_after]]
  * [[:develop:api:efl:ui:flip:method:pack_after|Efl.Ui.Flip.pack_after]]
  * [[:develop:api:efl:canvas:layout_part_box:method:pack_after|Efl.Canvas.Layout_Part_Box.pack_after]]
  * [[:develop:api:efl:ui:box:method:pack_after|Efl.Ui.Box.pack_after]]
  * [[:develop:api:efl:ui:radio_box:method:pack_after|Efl.Ui.Radio_Box.pack_after]]
  * [[:develop:api:efl:ui:group_item:method:pack_after|Efl.Ui.Group_Item.pack_after]]
  * [[:develop:api:efl:ui:collection:method:pack_after|Efl.Ui.Collection.pack_after]]
  * [[:develop:api:efl:ui:layout_part_box:method:pack_after|Efl.Ui.Layout_Part_Box.pack_after]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_after|Efl.Canvas.Layout_Part_Invalid.pack_after]]
  * [[:develop:api:efl:ui:spotlight:container:method:pack_after|Efl.Ui.Spotlight.Container.pack_after]]
  * [[:develop:api:efl:ui:tab_pager:method:pack_after|Efl.Ui.Tab_Pager.pack_after]]

