~~Title: Efl.Ui.Layout_Part_Content.content_unset~~
====== Efl.Ui.Layout_Part_Content.content_unset ======

===== Description =====

%%Remove the sub-object currently set as content of this object and return it. This object becomes empty.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_part_content:method:content_unset:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:content:method:content_unset|Efl.Content.content_unset]].//===== Signature =====

<code>
content_unset @pure_virtual {
    return: Efl.Gfx.Entity;
}
</code>

===== C signature =====

<code c>
Efl_Gfx_Entity *efl_content_unset(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:content:method:content_unset|Efl.Content.content_unset]]
  * [[:develop:api:efl:ui:check:method:content_unset|Efl.Ui.Check.content_unset]]
  * [[:develop:api:efl:ui:win:method:content_unset|Efl.Ui.Win.content_unset]]
  * [[:develop:api:efl:ui:default_item:method:content_unset|Efl.Ui.Default_Item.content_unset]]
  * [[:develop:api:efl:ui:win_part:method:content_unset|Efl.Ui.Win_Part.content_unset]]
  * [[:develop:api:efl:canvas:layout_part_external:method:content_unset|Efl.Canvas.Layout_Part_External.content_unset]]
  * [[:develop:api:efl:ui:navigation_bar_part_back_button:method:content_unset|Efl.Ui.Navigation_Bar_Part_Back_Button.content_unset]]
  * [[:develop:api:efl:ui:layout_part_legacy:method:content_unset|Efl.Ui.Layout_Part_Legacy.content_unset]]
  * [[:develop:api:elm:dayselector:part:method:content_unset|Elm.Dayselector.Part.content_unset]]
  * [[:develop:api:elm:entry:part:method:content_unset|Elm.Entry.Part.content_unset]]
  * [[:develop:api:elm:naviframe:part:method:content_unset|Elm.Naviframe.Part.content_unset]]
  * [[:develop:api:elm:ctxpopup:part:method:content_unset|Elm.Ctxpopup.Part.content_unset]]
  * [[:develop:api:elm:fileselector:entry:part:method:content_unset|Elm.Fileselector.Entry.Part.content_unset]]
  * [[:develop:api:elm:popup:part:method:content_unset|Elm.Popup.Part.content_unset]]
  * [[:develop:api:elm:hover:part:method:content_unset|Elm.Hover.Part.content_unset]]
  * [[:develop:api:elm:scroller:part:method:content_unset|Elm.Scroller.Part.content_unset]]
  * [[:develop:api:elm:notify:part:method:content_unset|Elm.Notify.Part.content_unset]]
  * [[:develop:api:efl:ui:panel:method:content_unset|Efl.Ui.Panel.content_unset]]
  * [[:develop:api:elm:panel:part:method:content_unset|Elm.Panel.Part.content_unset]]
  * [[:develop:api:efl:ui:tab_page:method:content_unset|Efl.Ui.Tab_Page.content_unset]]
  * [[:develop:api:efl:ui:list_placeholder_item:method:content_unset|Efl.Ui.List_Placeholder_Item.content_unset]]
  * [[:develop:api:elm:mapbuf:part:method:content_unset|Elm.Mapbuf.Part.content_unset]]
  * [[:develop:api:efl:ui:scroller:method:content_unset|Efl.Ui.Scroller.content_unset]]
  * [[:develop:api:efl:ui:flip_part:method:content_unset|Efl.Ui.Flip_Part.content_unset]]
  * [[:develop:api:efl:canvas:layout_part_swallow:method:content_unset|Efl.Canvas.Layout_Part_Swallow.content_unset]]
  * [[:develop:api:efl:ui:pan:method:content_unset|Efl.Ui.Pan.content_unset]]
  * [[:develop:api:efl:ui:frame:method:content_unset|Efl.Ui.Frame.content_unset]]
  * [[:develop:api:efl:ui:progressbar:method:content_unset|Efl.Ui.Progressbar.content_unset]]
  * [[:develop:api:efl:ui:popup:method:content_unset|Efl.Ui.Popup.content_unset]]
  * [[:develop:api:efl:ui:navigation_layout:method:content_unset|Efl.Ui.Navigation_Layout.content_unset]]
  * [[:develop:api:elm:flip:part:method:content_unset|Elm.Flip.Part.content_unset]]
  * [[:develop:api:efl:ui:button:method:content_unset|Efl.Ui.Button.content_unset]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:content_unset|Efl.Canvas.Layout_Part_Invalid.content_unset]]
  * [[:develop:api:efl:ui:layout_part_content:method:content_unset|Efl.Ui.Layout_Part_Content.content_unset]]
  * [[:develop:api:efl:ui:navigation_bar_part:method:content_unset|Efl.Ui.Navigation_Bar_Part.content_unset]]

