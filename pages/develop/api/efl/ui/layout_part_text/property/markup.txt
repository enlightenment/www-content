~~Title: Efl.Ui.Layout_Part_Text.markup~~
====== Efl.Ui.Layout_Part_Text.markup ======

===== Description =====

%%Markup property%%
{{page>:develop:api-include:efl:ui:layout_part_text:property:markup:description&nouser&nolink&nodate}}

===== Values =====

  * **markup** - %%The markup-text representation set to this text.%%

//Overridden from [[:develop:api:efl:text_markup:property:markup|Efl.Text_Markup.markup]] **(get, set)**.//===== Signature =====

<code>
@property markup @pure_virtual {
    get {}
    set {}
    values {
        markup: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_text_markup_get(const Eo *obj);
void efl_text_markup_set(Eo *obj, const char *markup);
</code>

===== Implemented by =====

  * [[:develop:api:efl:text_markup:property:markup|Efl.Text_Markup.markup]]
  * [[:develop:api:efl:ui:default_item:property:markup|Efl.Ui.Default_Item.markup]]
  * [[:develop:api:efl:ui:layout_part_legacy:property:markup|Efl.Ui.Layout_Part_Legacy.markup]]
  * [[:develop:api:efl:canvas:text:property:markup|Efl.Canvas.Text.markup]]
  * [[:develop:api:efl:ui:layout_part_text:property:markup|Efl.Ui.Layout_Part_Text.markup]]
  * [[:develop:api:efl:ui:frame:property:markup|Efl.Ui.Frame.markup]]
  * [[:develop:api:efl:ui:progressbar:property:markup|Efl.Ui.Progressbar.markup]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:markup|Efl.Canvas.Layout_Part_Invalid.markup]]
  * [[:develop:api:efl:canvas:layout_part_text:property:markup|Efl.Canvas.Layout_Part_Text.markup]]

