~~Title: Efl.Ui.Layout_Part_Text.text~~
====== Efl.Ui.Layout_Part_Text.text ======

===== Description =====

%%The text string to be displayed by the given text object.%%

%%Do not release (free) the returned value.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:layout_part_text:property:text:description&nouser&nolink&nodate}}

===== Values =====

  * **text** - %%Text string to display.%%

//Overridden from [[:develop:api:efl:text:property:text|Efl.Text.text]] **(get, set)**.//===== Signature =====

<code>
@property text @pure_virtual {
    get {}
    set {}
    values {
        text: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_text_get(const Eo *obj);
void efl_text_set(Eo *obj, const char *text);
</code>

===== Implemented by =====

  * [[:develop:api:efl:text:property:text|Efl.Text.text]]
  * [[:develop:api:efl:ui:check:property:text|Efl.Ui.Check.text]]
  * [[:develop:api:efl:ui:win:property:text|Efl.Ui.Win.text]]
  * [[:develop:api:efl:ui:default_item:property:text|Efl.Ui.Default_Item.text]]
  * [[:develop:api:efl:ui:navigation_bar_part_back_button:property:text|Efl.Ui.Navigation_Bar_Part_Back_Button.text]]
  * [[:develop:api:efl:ui:layout_part_legacy:property:text|Efl.Ui.Layout_Part_Legacy.text]]
  * [[:develop:api:elm_actionslider:part:property:text|Elm_Actionslider.Part.text]]
  * [[:develop:api:elm:entry:part:property:text|Elm.Entry.Part.text]]
  * [[:develop:api:elm_label:part:property:text|Elm_Label.Part.text]]
  * [[:develop:api:elm:multibuttonentry_part:property:text|Elm.Multibuttonentry_Part.text]]
  * [[:develop:api:elm:naviframe:part:property:text|Elm.Naviframe.Part.text]]
  * [[:develop:api:elm_bubble:part:property:text|Elm_Bubble.Part.text]]
  * [[:develop:api:elm:fileselector:entry:part:property:text|Elm.Fileselector.Entry.Part.text]]
  * [[:develop:api:elm:fileselector:part:property:text|Elm.Fileselector.Part.text]]
  * [[:develop:api:elm:popup:part:property:text|Elm.Popup.Part.text]]
  * [[:develop:api:elm:notify:part:property:text|Elm.Notify.Part.text]]
  * [[:develop:api:efl:canvas:text:property:text|Efl.Canvas.Text.text]]
  * [[:develop:api:efl:ui:layout_part_text:property:text|Efl.Ui.Layout_Part_Text.text]]
  * [[:develop:api:efl:ui:textpath_part:property:text|Efl.Ui.Textpath_Part.text]]
  * [[:develop:api:efl:ui:alert_popup_part:property:text|Efl.Ui.Alert_Popup_Part.text]]
  * [[:develop:api:efl:ui:text_part:property:text|Efl.Ui.Text_Part.text]]
  * [[:develop:api:efl:ui:textpath:property:text|Efl.Ui.Textpath.text]]
  * [[:develop:api:efl:ui:tags:property:text|Efl.Ui.Tags.text]]
  * [[:develop:api:efl:ui:frame:property:text|Efl.Ui.Frame.text]]
  * [[:develop:api:efl:ui:progressbar:property:text|Efl.Ui.Progressbar.text]]
  * [[:develop:api:efl:ui:button:property:text|Efl.Ui.Button.text]]
  * [[:develop:api:efl:ui:navigation_bar:property:text|Efl.Ui.Navigation_Bar.text]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:text|Efl.Canvas.Layout_Part_Invalid.text]]
  * [[:develop:api:efl:canvas:layout_part_text:property:text|Efl.Canvas.Layout_Part_Text.text]]

