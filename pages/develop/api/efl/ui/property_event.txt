~~Title: Efl.Ui.Property_Event~~

===== Description =====

%%EFL Ui property event data structure triggered when an object property change due to the interaction on the object.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:property_event:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:property_event:fields&nouser&nolink&nodate}}

  * **changed_properties** - %%List of changed properties%%

===== Signature =====

<code>
struct Efl.Ui.Property_Event {
    changed_properties: array<stringshare>;
}
</code>

===== C signature =====

<code c>
typedef struct _Efl_Ui_Property_Event {
    Eina_Array *changed_properties;
} Efl_Ui_Property_Event;
</code>
