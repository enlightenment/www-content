~~Title: Efl.Ui.Radio.state_set~~
====== Efl.Ui.Radio.state_set ======

===== Values =====

  * **states** - %%Accessible state set%%


\\ {{page>:develop:api-include:efl:ui:radio:property:state_set:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:check:property:state_set|Efl.Access.Object.state_set]] **(get)**.//===== Signature =====

<code>
@property state_set @beta {
    get @protected {}
    values {
        states: Efl.Access.State_Set;
    }
}
</code>

===== C signature =====

<code c>
Efl_Access_State_Set efl_access_object_state_set_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:object:property:state_set|Efl.Access.Object.state_set]]
  * [[:develop:api:efl:ui:widget:property:state_set|Efl.Ui.Widget.state_set]]
  * [[:develop:api:efl:ui:win:property:state_set|Efl.Ui.Win.state_set]]
  * [[:develop:api:efl:ui:check:property:state_set|Efl.Ui.Check.state_set]]
  * [[:develop:api:efl:ui:radio:property:state_set|Efl.Ui.Radio.state_set]]
  * [[:develop:api:efl:ui:text:property:state_set|Efl.Ui.Text.state_set]]

