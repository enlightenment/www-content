~~Title: Efl.Ui.Radio.state_value~~
====== Efl.Ui.Radio.state_value ======

===== Description =====

%%Integer value that this radio button represents.%%

%%Each radio button in a group must have a unique value. The selected button in a group can then be set or retrieved through the %%[[:develop:api:efl:ui:radio_group:property:selected_value|Efl.Ui.Radio_Group.selected_value]]%% property. This value is also informed through the %%[[:develop:api:efl:ui:radio_group:event:value,changed|Efl.Ui.Radio_Group.value,changed]]%% event.%%

%%All non-negative values are legal but keep in mind that 0 is the starting value for all new groups: If no button in the group has this value, then no button in the group is initially selected. -1 is the value that %%[[:develop:api:efl:ui:radio_group:property:selected_value|Efl.Ui.Radio_Group.selected_value]]%% returns when no button is selected and therefore cannot be used.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:radio:property:state_value:description&nouser&nolink&nodate}}

===== Values =====

  * **value** - %%The value to use when this radio button is selected. Any value can be used but 0 and -1 have special meanings as described above.%%

===== Signature =====

<code>
@property state_value {
    get {}
    set {}
    values {
        value: int;
    }
}
</code>

===== C signature =====

<code c>
int efl_ui_radio_state_value_get(const Eo *obj);
void efl_ui_radio_state_value_set(Eo *obj, int value);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:radio:property:state_value|Efl.Ui.Radio.state_value]]

