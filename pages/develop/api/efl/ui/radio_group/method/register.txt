~~Title: Efl.Ui.Radio_Group.register~~
====== Efl.Ui.Radio_Group.register ======

===== Description =====

%%Register a new %%[[:develop:api:efl:ui:radio|Efl.Ui.Radio]]%% button to this group. Keep in mind that registering to a group will only handle button grouping, you still need to add the button to a layout for it to be rendered.%%

%%If the %%[[:develop:api:efl:ui:radio:property:state_value|Efl.Ui.Radio.state_value]]%% of the new button is already used by a previous button in the group, the button will not be added.%%

%%See also %%[[:develop:api:efl:ui:radio_group:method:unregister|Efl.Ui.Radio_Group.unregister]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:radio_group:method:register:description&nouser&nolink&nodate}}

===== Signature =====

<code>
register @pure_virtual {
    params {
        @in radio: Efl.Ui.Radio;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_radio_group_register(Eo *obj, Efl_Ui_Radio *radio);
</code>

===== Parameters =====

  * **radio** //(in)// - %%The radio button to add to the group.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:radio_group:method:register|Efl.Ui.Radio_Group.register]]
  * [[:develop:api:efl:ui:radio_group_impl:method:register|Efl.Ui.Radio_Group_Impl.register]]

