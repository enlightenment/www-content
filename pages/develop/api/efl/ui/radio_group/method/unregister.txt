~~Title: Efl.Ui.Radio_Group.unregister~~
====== Efl.Ui.Radio_Group.unregister ======

===== Description =====

%%Unregister an %%[[:develop:api:efl:ui:radio|Efl.Ui.Radio]]%% button from this group. This will unlink the behavior of this button from the other buttons in the group, but if it still belongs to a layout, it will still be rendered.%%

%%If the button was not registered in the group the call is ignored. If the button was selected, no button will be selected in the group after this call.%%

%%See also %%[[:develop:api:efl:ui:radio_group:method:register|Efl.Ui.Radio_Group.register]]%%.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:radio_group:method:unregister:description&nouser&nolink&nodate}}

===== Signature =====

<code>
unregister @pure_virtual {
    params {
        @in radio: Efl.Ui.Radio;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_radio_group_unregister(Eo *obj, Efl_Ui_Radio *radio);
</code>

===== Parameters =====

  * **radio** //(in)// - %%The radio button to remove from the group.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:radio_group:method:unregister|Efl.Ui.Radio_Group.unregister]]
  * [[:develop:api:efl:ui:radio_group_impl:method:unregister|Efl.Ui.Radio_Group_Impl.unregister]]

