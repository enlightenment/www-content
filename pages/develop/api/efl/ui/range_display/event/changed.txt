~~Title: Efl.Ui.Range_Display: changed~~

===== Description =====

%%Emitted when the %%[[:develop:api:efl:ui:range_display:property:range_value|Efl.Ui.Range_Display.range_value]]%% is getting changed.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:range_display:event:changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
changed;
</code>

===== C information =====

<code c>
EFL_UI_RANGE_EVENT_CHANGED(void)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_range_event_changed(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_RANGE_EVENT_CHANGED, on_efl_ui_range_event_changed, d);
}

</code>
