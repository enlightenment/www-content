~~Title: Efl.Ui.Range_Display: max,reached~~

===== Description =====

%%Emitted when the %%''range_value''%% has reached the maximum of %%[[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]%%.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:range_display:event:max_reached:description&nouser&nolink&nodate}}

===== Signature =====

<code>
max,reached;
</code>

===== C information =====

<code c>
EFL_UI_RANGE_EVENT_MAX_REACHED(void)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_range_event_max_reached(void *data, const Efl_Event *event)
{
    void info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_RANGE_EVENT_MAX_REACHED, on_efl_ui_range_event_max_reached, d);
}

</code>
