~~Title: Efl.Ui.Scrollable.scroll~~
====== Efl.Ui.Scrollable.scroll ======

===== Description =====

%%Show a specific virtual region within the scroller content object.%%

%%This will ensure all (or part if it does not fit) of the designated region in the virtual content object (%%''0,0''%% starting at the top-left of the virtual content object) is shown within the scroller. This allows the scroller to "smoothly slide" to this location (if configuration in general calls for transitions). It may not jump immediately to the new location and make take a while and show other content along the way.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:method:scroll:description&nouser&nolink&nodate}}

===== Signature =====

<code>
scroll @pure_virtual {
    params {
        @in rect: Eina.Rect;
        @in animation: bool;
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_scrollable_scroll(Eo *obj, Eina_Rect rect, Eina_Bool animation);
</code>

===== Parameters =====

  * **rect** //(in)// - %%The position where to scroll and the size user want to see.%%
  * **animation** //(in)// - %%Whether to scroll with animation or not.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:method:scroll|Efl.Ui.Scrollable.scroll]]
  * [[:develop:api:efl:ui:image_zoomable:method:scroll|Efl.Ui.Image_Zoomable.scroll]]
  * [[:develop:api:efl:ui:scroll:manager:method:scroll|Efl.Ui.Scroll.Manager.scroll]]

