~~Title: Efl.Ui.Scrollable.content_pos~~
====== Efl.Ui.Scrollable.content_pos ======

===== Description =====

%%Position of the content inside the scroller.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:property:content_pos:description&nouser&nolink&nodate}}

===== Values =====

  * **pos** - %%The position is a virtual value, where %%''0,0''%% means the top-left corner.%%

===== Signature =====

<code>
@property content_pos @pure_virtual {
    get {}
    set {}
    values {
        pos: Eina.Position2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Position2D efl_ui_scrollable_content_pos_get(const Eo *obj);
void efl_ui_scrollable_content_pos_set(Eo *obj, Eina_Position2D pos);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:content_pos|Efl.Ui.Scrollable.content_pos]]
  * [[:develop:api:efl:ui:scroll:manager:property:content_pos|Efl.Ui.Scroll.Manager.content_pos]]

