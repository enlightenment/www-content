~~Title: Efl.Ui.Scrollable.movement_block~~
====== Efl.Ui.Scrollable.movement_block ======

===== Description =====

%%Blocking of scrolling (per axis).%%

%%This function will block scrolling movement (by input of a user) in a given direction. You can disable movements in the X axis, the Y axis or both. The default value is %%[[:develop:api:efl:ui:layout_orientation|Efl.Ui.Layout_Orientation.default]]%% meaning that movements are allowed in both directions.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollable:property:movement_block:description&nouser&nolink&nodate}}

===== Values =====

  * **block** - %%Which axis (or axes) to block.%%

===== Signature =====

<code>
@property movement_block @pure_virtual {
    get {}
    set {}
    values {
        block: Efl.Ui.Layout_Orientation (Efl.Ui.Layout_Orientation.default);
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Layout_Orientation efl_ui_scrollable_movement_block_get(const Eo *obj);
void efl_ui_scrollable_movement_block_set(Eo *obj, Efl_Ui_Layout_Orientation block);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:movement_block|Efl.Ui.Scrollable.movement_block]]
  * [[:develop:api:efl:ui:scroll:manager:property:movement_block|Efl.Ui.Scroll.Manager.movement_block]]

