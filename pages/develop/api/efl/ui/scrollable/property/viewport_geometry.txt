~~Title: Efl.Ui.Scrollable.viewport_geometry~~
====== Efl.Ui.Scrollable.viewport_geometry ======

===== Values =====

  * **rect** - %%It is absolute geometry.%%


\\ {{page>:develop:api-include:efl:ui:scrollable:property:viewport_geometry:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property viewport_geometry @pure_virtual {
    get {}
    values {
        rect: Eina.Rect;
    }
}
</code>

===== C signature =====

<code c>
Eina_Rect efl_ui_scrollable_viewport_geometry_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollable:property:viewport_geometry|Efl.Ui.Scrollable.viewport_geometry]]
  * [[:develop:api:efl:ui:scroll:manager:property:viewport_geometry|Efl.Ui.Scroll.Manager.viewport_geometry]]

