~~Title: Efl.Ui.Scrollbar~~
====== Efl.Ui.Scrollbar (interface) ======

===== Description =====

%%Interface used by widgets which can display scrollbars, enabling them to hold more content than actually visible through the viewport. A scrollbar contains a draggable part (thumb) which allows the user to move the viewport around the content. The size of the thumb relates to the size of the viewport compared to the whole content.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:scrollbar:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:scrollbar:property:bar_mode|bar_mode]]** //**(get, set)**//\\
> %%Scrollbar visibility mode, for each of the scrollbars.%%
<code c>
void efl_ui_scrollbar_bar_mode_get(const Eo *obj, Efl_Ui_Scrollbar_Mode *hbar, Efl_Ui_Scrollbar_Mode *vbar);
void efl_ui_scrollbar_bar_mode_set(Eo *obj, Efl_Ui_Scrollbar_Mode hbar, Efl_Ui_Scrollbar_Mode vbar);
</code>
\\
**[[:develop:api:efl:ui:scrollbar:property:bar_position|bar_position]]** //**(get, set)**//\\
> %%Position of the thumb (the draggable zone) inside the scrollbar. It is calculated based on current position of the viewport inside the total content.%%
<code c>
void efl_ui_scrollbar_bar_position_get(const Eo *obj, double *posx, double *posy);
void efl_ui_scrollbar_bar_position_set(Eo *obj, double posx, double posy);
</code>
\\
**[[:develop:api:efl:ui:scrollbar:property:bar_size|bar_size]]** //**(get)**//\\
> 
<code c>
void efl_ui_scrollbar_bar_size_get(const Eo *obj, double *width, double *height);
</code>
\\
**[[:develop:api:efl:ui:scrollbar:method:bar_visibility_update|bar_visibility_update]]** ''protected''\\
> %%Update bar visibility.%%
<code c>
void efl_ui_scrollbar_bar_visibility_update(Eo *obj);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:scrollbar:event:bar_dragged|bar,dragged]]**\\
> %%Emitted when thumb is dragged.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_DRAGGED(Efl_Ui_Layout_Orientation)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_hide|bar,hide]]**\\
> %%Emitted when scrollbar is hidden.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_HIDE(Efl_Ui_Layout_Orientation)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_pos_changed|bar,pos,changed]]**\\
> %%Emitted when thumb position has changed.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_POS_CHANGED(void)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_pressed|bar,pressed]]**\\
> %%Emitted when thumb is pressed.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_PRESSED(Efl_Ui_Layout_Orientation)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_show|bar,show]]**\\
> %%Emitted when scrollbar is shown.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_SHOW(Efl_Ui_Layout_Orientation)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_size_changed|bar,size,changed]]**\\
> %%Emitted when thumb size has changed.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_SIZE_CHANGED(void)
</code>
\\ **[[:develop:api:efl:ui:scrollbar:event:bar_unpressed|bar,unpressed]]**\\
> %%Emitted when thumb is unpressed.%%
<code c>
EFL_UI_SCROLLBAR_EVENT_BAR_UNPRESSED(Efl_Ui_Layout_Orientation)
</code>
\\ 