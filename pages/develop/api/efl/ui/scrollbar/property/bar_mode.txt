~~Title: Efl.Ui.Scrollbar.bar_mode~~
====== Efl.Ui.Scrollbar.bar_mode ======

===== Description =====

%%Scrollbar visibility mode, for each of the scrollbars.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:scrollbar:property:bar_mode:description&nouser&nolink&nodate}}

===== Values =====

  * **hbar** - %%Horizontal scrollbar mode.%%
  * **vbar** - %%Vertical scrollbar mode.%%

===== Signature =====

<code>
@property bar_mode @pure_virtual {
    get {}
    set {}
    values {
        hbar: Efl.Ui.Scrollbar_Mode (Efl.Ui.Scrollbar_Mode.auto);
        vbar: Efl.Ui.Scrollbar_Mode (Efl.Ui.Scrollbar_Mode.auto);
    }
}
</code>

===== C signature =====

<code c>
void efl_ui_scrollbar_bar_mode_get(const Eo *obj, Efl_Ui_Scrollbar_Mode *hbar, Efl_Ui_Scrollbar_Mode *vbar);
void efl_ui_scrollbar_bar_mode_set(Eo *obj, Efl_Ui_Scrollbar_Mode hbar, Efl_Ui_Scrollbar_Mode vbar);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:scrollbar:property:bar_mode|Efl.Ui.Scrollbar.bar_mode]]
  * [[:develop:api:efl:ui:scroll:manager:property:bar_mode|Efl.Ui.Scroll.Manager.bar_mode]]

