~~Title: Efl.Ui.Scrollbar_Mode~~

===== Description =====

%%When should the scrollbar be shown.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:scrollbar_mode:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:efl:ui:scrollbar_mode:fields&nouser&nolink&nodate}}

  * **auto** - %%Visible if necessary.%%
  * **on** - %%Always visible.%%
  * **off** - %%Always invisible.%%
  * **last** - %%For internal use only.%%

===== Signature =====

<code>
enum Efl.Ui.Scrollbar_Mode {
    auto: 0,
    on,
    off,
    last
}
</code>

===== C signature =====

<code c>
typedef enum {
    EFL_UI_SCROLLBAR_MODE_AUTO = 0,
    EFL_UI_SCROLLBAR_MODE_ON,
    EFL_UI_SCROLLBAR_MODE_OFF,
    EFL_UI_SCROLLBAR_MODE_LAST
} Efl_Ui_Scrollbar_Mode;
</code>
