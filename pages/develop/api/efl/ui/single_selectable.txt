~~Title: Efl.Ui.Single_Selectable~~
====== Efl.Ui.Single_Selectable (interface) ======

===== Description =====

%%Interface for getting access to a single selected item in the implementor.%%

%%The implementor is free to allow a specific number of selectables being selected or not. This interface just covers always the latest selected selectable.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:single_selectable:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:single_selectable:property:fallback_selection|fallback_selection]]** //**(get, set)**//\\
> %%A object that will be selected in case nothing is selected%%
<code c>
Efl_Ui_Selectable *efl_ui_selectable_fallback_selection_get(const Eo *obj);
void efl_ui_selectable_fallback_selection_set(Eo *obj, Efl_Ui_Selectable *fallback);
</code>
\\
**[[:develop:api:efl:ui:single_selectable:property:last_selected|last_selected]]** //**(get)**//\\
> 
<code c>
Efl_Ui_Selectable *efl_ui_selectable_last_selected_get(const Eo *obj);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:single_selectable:event:selection_changed|selection_changed]]**\\
> %%Emitted when there is a change in the selection state. This event will collect all the item selection change events that are happening within one loop iteration. This means, you will only get this event once, even if a lot of items have changed. If you are interested in detailed changes, subscribe to the individual %%[[:develop:api:efl:ui:selectable:event:selected,changed|Efl.Ui.Selectable.selected,changed]]%% events of each item.%%
<code c>
EFL_UI_SELECTABLE_EVENT_SELECTION_CHANGED(void)
</code>
\\ 