~~Title: Efl.Ui.Slider.theme_apply~~
====== Efl.Ui.Slider.theme_apply ======

===== Description =====

%%Virtual function called when the widget needs to re-apply its theme.%%

%%This may be called when the object is first created, or whenever the widget is modified in any way that may require a reload of the theme. This may include but is not limited to scale, theme, or mirrored mode changes.%%

<note>
%%even widgets not based on layouts may override this method to handle widget updates (scale, mirrored mode, etc...).%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:ui:slider:method:theme_apply:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:layout_base:method:theme_apply|Efl.Ui.Widget.theme_apply]].//===== Signature =====

<code>
theme_apply @protected {
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_ui_widget_theme_apply(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:method:theme_apply|Efl.Ui.Widget.theme_apply]]
  * [[:develop:api:efl:ui:win:method:theme_apply|Efl.Ui.Win.theme_apply]]
  * [[:develop:api:efl:ui:flip:method:theme_apply|Efl.Ui.Flip.theme_apply]]
  * [[:develop:api:efl:ui:image:method:theme_apply|Efl.Ui.Image.theme_apply]]
  * [[:develop:api:efl:ui:image_zoomable:method:theme_apply|Efl.Ui.Image_Zoomable.theme_apply]]
  * [[:develop:api:efl:ui:layout_base:method:theme_apply|Efl.Ui.Layout_Base.theme_apply]]
  * [[:develop:api:efl:ui:slider:method:theme_apply|Efl.Ui.Slider.theme_apply]]
  * [[:develop:api:efl:ui:check:method:theme_apply|Efl.Ui.Check.theme_apply]]
  * [[:develop:api:efl:ui:radio:method:theme_apply|Efl.Ui.Radio.theme_apply]]
  * [[:develop:api:efl:ui:clock:method:theme_apply|Efl.Ui.Clock.theme_apply]]
  * [[:develop:api:efl:ui:text:method:theme_apply|Efl.Ui.Text.theme_apply]]
  * [[:develop:api:efl:ui:spin_button:method:theme_apply|Efl.Ui.Spin_Button.theme_apply]]
  * [[:develop:api:efl:ui:collection_view:method:theme_apply|Efl.Ui.Collection_View.theme_apply]]
  * [[:develop:api:efl:ui:panel:method:theme_apply|Efl.Ui.Panel.theme_apply]]
  * [[:develop:api:efl:ui:slider_interval:method:theme_apply|Efl.Ui.Slider_Interval.theme_apply]]
  * [[:develop:api:elm:code_widget:method:theme_apply|Elm.Code_Widget.theme_apply]]
  * [[:develop:api:efl:ui:textpath:method:theme_apply|Efl.Ui.Textpath.theme_apply]]
  * [[:develop:api:efl:ui:scroller:method:theme_apply|Efl.Ui.Scroller.theme_apply]]
  * [[:develop:api:efl:ui:collection:method:theme_apply|Efl.Ui.Collection.theme_apply]]
  * [[:develop:api:efl:ui:panes:method:theme_apply|Efl.Ui.Panes.theme_apply]]
  * [[:develop:api:efl:ui:calendar:method:theme_apply|Efl.Ui.Calendar.theme_apply]]
  * [[:develop:api:efl:ui:progressbar:method:theme_apply|Efl.Ui.Progressbar.theme_apply]]

