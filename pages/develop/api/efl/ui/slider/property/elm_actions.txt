~~Title: Efl.Ui.Slider.elm_actions~~
====== Efl.Ui.Slider.elm_actions ======

===== Values =====

  * **actions** - %%NULL-terminated array of Efl.Access.Action_Data.%%


\\ {{page>:develop:api-include:efl:ui:slider:property:elm_actions:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:access:widget:action:property:elm_actions|Efl.Access.Widget.Action.elm_actions]] **(get)**.//===== Signature =====

<code>
@property elm_actions @beta @pure_virtual {
    get @protected {}
    values {
        actions: ptr(const(Efl.Access.Action_Data));
    }
}
</code>

===== C signature =====

<code c>
const Efl_Access_Action_Data *efl_access_widget_action_elm_actions_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:widget:action:property:elm_actions|Efl.Access.Widget.Action.elm_actions]]
  * [[:develop:api:efl:ui:slider:property:elm_actions|Efl.Ui.Slider.elm_actions]]
  * [[:develop:api:efl:ui:check:property:elm_actions|Efl.Ui.Check.elm_actions]]
  * [[:develop:api:efl:ui:radio:property:elm_actions|Efl.Ui.Radio.elm_actions]]
  * [[:develop:api:efl:ui:win:property:elm_actions|Efl.Ui.Win.elm_actions]]
  * [[:develop:api:efl:ui:spin_button:property:elm_actions|Efl.Ui.Spin_Button.elm_actions]]
  * [[:develop:api:efl:ui:panel:property:elm_actions|Efl.Ui.Panel.elm_actions]]
  * [[:develop:api:efl:ui:slider_interval:property:elm_actions|Efl.Ui.Slider_Interval.elm_actions]]
  * [[:develop:api:efl:ui:image:property:elm_actions|Efl.Ui.Image.elm_actions]]
  * [[:develop:api:efl:ui:image_zoomable:property:elm_actions|Efl.Ui.Image_Zoomable.elm_actions]]
  * [[:develop:api:efl:ui:calendar:property:elm_actions|Efl.Ui.Calendar.elm_actions]]
  * [[:develop:api:efl:ui:video:property:elm_actions|Efl.Ui.Video.elm_actions]]
  * [[:develop:api:efl:ui:button:property:elm_actions|Efl.Ui.Button.elm_actions]]

