~~Title: Efl.Ui.Spin_Button.direct_text_input~~
====== Efl.Ui.Spin_Button.direct_text_input ======

===== Description =====

%%Control whether the spin can be directly edited by the user.%%

%%Spin objects can have editing disabled, in which case they can only be changed by using arrows. This is useful for situations where you don't want your users to write their own value. It's especially useful when using special values. The user can see the real values instead of special label when editing.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:spin_button:property:direct_text_input:description&nouser&nolink&nodate}}

===== Values =====

  * **direct_text_input** - %%%%''true''%% to allow users to directly edit the value.%%

===== Signature =====

<code>
@property direct_text_input @beta {
    get {}
    set {}
    values {
        direct_text_input: bool (false);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_spin_button_direct_text_input_get(const Eo *obj);
void efl_ui_spin_button_direct_text_input_set(Eo *obj, Eina_Bool direct_text_input);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:spin_button:property:direct_text_input|Efl.Ui.Spin_Button.direct_text_input]]

