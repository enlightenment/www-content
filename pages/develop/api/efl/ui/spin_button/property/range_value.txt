~~Title: Efl.Ui.Spin_Button.range_value~~
====== Efl.Ui.Spin_Button.range_value ======

===== Description =====

%%Control the value (position) of the widget within its valid range.%%

%%Values outside the limits defined in %%[[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]%% are ignored and an error is printed.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:spin_button:property:range_value:description&nouser&nolink&nodate}}

===== Values =====

  * **val** - %%The range value (must be within the bounds of %%[[:develop:api:efl:ui:range_display:property:range_limits|Efl.Ui.Range_Display.range_limits]]%%).%%

//Overridden from [[:develop:api:efl:ui:spin:property:range_value|Efl.Ui.Range_Display.range_value]] **(set)**.//===== Signature =====

<code>
@property range_value @pure_virtual {
    get {}
    set {}
    values {
        val: double;
    }
}
</code>

===== C signature =====

<code c>
double efl_ui_range_value_get(const Eo *obj);
void efl_ui_range_value_set(Eo *obj, double val);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:range_display:property:range_value|Efl.Ui.Range_Display.range_value]]
  * [[:develop:api:efl:ui:spin:property:range_value|Efl.Ui.Spin.range_value]]
  * [[:develop:api:efl:ui:spin_button:property:range_value|Efl.Ui.Spin_Button.range_value]]
  * [[:develop:api:efl:ui:progressbar_part:property:range_value|Efl.Ui.Progressbar_Part.range_value]]
  * [[:develop:api:efl:ui:slider:property:range_value|Efl.Ui.Slider.range_value]]
  * [[:develop:api:efl:ui:slider_interval:property:range_value|Efl.Ui.Slider_Interval.range_value]]
  * [[:develop:api:efl:ui:progressbar:property:range_value|Efl.Ui.Progressbar.range_value]]

