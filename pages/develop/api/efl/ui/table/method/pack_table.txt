~~Title: Efl.Ui.Table.pack_table~~
====== Efl.Ui.Table.pack_table ======

===== Description =====

%%Pack object at a given location in the table.%%

%%When this container is deleted, it will request deletion of the given %%''subobj''%%. Use %%[[:develop:api:efl:pack:method:unpack|Efl.Pack.unpack]]%% to remove %%''subobj''%% from this container without deleting it.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:method:pack_table:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:pack_table:method:pack_table|Efl.Pack_Table.pack_table]].//===== Signature =====

<code>
pack_table @pure_virtual {
    params {
        @in subobj: Efl.Gfx.Entity;
        @in col: int;
        @in row: int;
        @in colspan: int @optional;
        @in rowspan: int @optional;
    }
    return: bool;
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_table(Eo *obj, Efl_Gfx_Entity *subobj, int col, int row, int colspan, int rowspan);
</code>

===== Parameters =====

  * **subobj** //(in)// - %%A child object to pack in this table.%%
  * **col** //(in)// - %%Column number%%
  * **row** //(in)// - %%Row number%%
  * **colspan** //(in)// - %%0 means 1, -1 means %%[[:develop:api:efl:pack_table:property:table_columns|Efl.Pack_Table.table_columns]]%%%%
  * **rowspan** //(in)// - %%0 means 1, -1 means %%[[:develop:api:efl:pack_table:property:table_rows|Efl.Pack_Table.table_rows]]%%%%

===== Implemented by =====

  * [[:develop:api:efl:pack_table:method:pack_table|Efl.Pack_Table.pack_table]]
  * [[:develop:api:efl:ui:table:method:pack_table|Efl.Ui.Table.pack_table]]
  * [[:develop:api:efl:canvas:layout_part_table:method:pack_table|Efl.Canvas.Layout_Part_Table.pack_table]]
  * [[:develop:api:efl:ui:layout_part_table:method:pack_table|Efl.Ui.Layout_Part_Table.pack_table]]
  * [[:develop:api:efl:canvas:layout_part_invalid:method:pack_table|Efl.Canvas.Layout_Part_Invalid.pack_table]]

