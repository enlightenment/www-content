~~Title: Efl.Ui.Table.table_cell_row~~
====== Efl.Ui.Table.table_cell_row ======

===== Description =====

%%row of the %%''subobj''%% in this container.%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:table:property:table_cell_row:description&nouser&nolink&nodate}}

===== Keys =====

  * **subobj** - %%Child object%%
===== Values =====

  * **row** - %%Row number%%
  * **rowspan** - %%Row span%%

//Overridden from [[:develop:api:efl:pack_table:property:table_cell_row|Efl.Pack_Table.table_cell_row]] **(get, set)**.//===== Signature =====

<code>
@property table_cell_row @pure_virtual {
    get {
        return: bool;
    }
    set {}
    keys {
        subobj: Efl.Gfx.Entity;
    }
    values {
        row: int;
        rowspan: int;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_pack_table_cell_row_get(const Eo *obj, Efl_Gfx_Entity *subobj, int *row, int *rowspan);
void efl_pack_table_cell_row_set(Eo *obj, Efl_Gfx_Entity *subobj, int row, int rowspan);
</code>

===== Implemented by =====

  * [[:develop:api:efl:pack_table:property:table_cell_row|Efl.Pack_Table.table_cell_row]]
  * [[:develop:api:efl:ui:table:property:table_cell_row|Efl.Ui.Table.table_cell_row]]
  * [[:develop:api:efl:canvas:layout_part_table:property:table_cell_row|Efl.Canvas.Layout_Part_Table.table_cell_row]]
  * [[:develop:api:efl:ui:layout_part_table:property:table_cell_row|Efl.Ui.Layout_Part_Table.table_cell_row]]
  * [[:develop:api:efl:canvas:layout_part_invalid:property:table_cell_row|Efl.Canvas.Layout_Part_Invalid.table_cell_row]]

