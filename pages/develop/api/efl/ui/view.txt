~~Title: Efl.Ui.View~~
====== Efl.Ui.View (interface) ======

===== Description =====

%%Efl UI view interface.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:view:description&nouser&nolink&nodate}}

===== Members =====

**[[:develop:api:efl:ui:view:property:model|model]]** //**(get, set)**//\\
> %%Model that is/will be%%
<code c>
Efl_Model *efl_ui_view_model_get(const Eo *obj);
void efl_ui_view_model_set(Eo *obj, Efl_Model *model);
</code>
\\

===== Events =====

**[[:develop:api:efl:ui:view:event:model_changed|model,changed]]**\\
> %%Event dispatched when a new model is set.%%
<code c>
EFL_UI_VIEW_EVENT_MODEL_CHANGED(Efl_Model_Changed_Event)
</code>
\\ 