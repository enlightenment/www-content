~~Title: Efl.Ui.View: model,changed~~

===== Description =====

%%Event dispatched when a new model is set.%%

//Since 1.23//

{{page>:develop:api-include:efl:ui:view:event:model_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
model,changed: Efl.Model_Changed_Event;
</code>

===== C information =====

<code c>
EFL_UI_VIEW_EVENT_MODEL_CHANGED(Efl_Model_Changed_Event)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_view_event_model_changed(void *data, const Efl_Event *event)
{
    Efl_Model_Changed_Event info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_VIEW_EVENT_MODEL_CHANGED, on_efl_ui_view_event_model_changed, d);
}

</code>
