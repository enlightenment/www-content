~~Title: Efl.Ui.View_Model.property_string_add~~
====== Efl.Ui.View_Model.property_string_add ======

===== Description =====

%%Adds a synthetic string property, generated from a %%''definition''%% string and other properties in the model.%%

%%The %%''definition''%% string, similar to how %%''printf''%% works, contains ${} placeholders that are replaced by the actual value of the property inside the placeholder tags when the synthetic property is retrieved. For example, a numeric property %%''length''%% might be strange to use as a label, since it will only display a number. However, a synthetic string can be generated with the definition "Length ${length}." which renders more nicely and does not require any more code by the user of the property.%%

%%%%''not_ready''%% and %%''on_error''%% strings can be given to be used when the data is not ready or there is some error, respectively. These strings do accept placeholder tags.%%

%%See %%[[:develop:api:efl:ui:view_model:method:property_string_del|Efl.Ui.View_Model.property_string_del]]%%%%

//Since 1.23//
{{page>:develop:api-include:efl:ui:view_model:method:property_string_add:description&nouser&nolink&nodate}}

===== Signature =====

<code>
property_string_add {
    params {
        @in name: string;
        @in definition: string;
        @in not_ready: string;
        @in on_error: string;
    }
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_ui_view_model_property_string_add(Eo *obj, const char *name, const char *definition, const char *not_ready, const char *on_error);
</code>

===== Parameters =====

  * **name** //(in)// - %%The name for the new synthetic property.%%
  * **definition** //(in)// - %%The definition string for the new synthetic property.%%
  * **not_ready** //(in)// - %%The text to be used if any of the properties used in %%''definition''%% is not ready yet. If set to %%''null''%%, no check against EAGAIN will be done.%%
  * **on_error** //(in)// - %%The text to be used if any of the properties used in %%''definition''%% is in error. It takes precedence over %%''not_ready''%%. If set to %%''null''%%, no error checks are performed.%%

===== Implemented by =====

  * [[:develop:api:efl:ui:view_model:method:property_string_add|Efl.Ui.View_Model.property_string_add]]

