~~Title: Efl.Ui.Widget.access_info~~
====== Efl.Ui.Widget.access_info ======

===== Description =====

%%Accessibility information.%%

%%This is a replacement string to be read by the accessibility text-to-speech engine, if accessibility is enabled by configuration. This will take precedence over the default text for this object, which means for instance that the label of a button won't be read out loud, instead %%''txt''%% will be read out.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:access_info:description&nouser&nolink&nodate}}

===== Values =====

  * **txt** - %%Accessibility text description.%%

===== Signature =====

<code>
@property access_info @beta {
    get {}
    set {}
    values {
        txt: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_widget_access_info_get(const Eo *obj);
void efl_ui_widget_access_info_set(Eo *obj, const char *txt);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:access_info|Efl.Ui.Widget.access_info]]

