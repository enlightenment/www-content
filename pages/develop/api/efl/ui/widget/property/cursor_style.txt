~~Title: Efl.Ui.Widget.cursor_style~~
====== Efl.Ui.Widget.cursor_style ======

===== Description =====

%%A different style for the cursor.%%

%%This only makes sense if theme cursors are used. The cursor should be set with %%[[:develop:api:efl:ui:widget:property:cursor|Efl.Ui.Widget.cursor.set]]%% first before setting its style with this property.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:cursor_style:description&nouser&nolink&nodate}}

===== Values =====

  * **style** - %%A specific style to use, e.g. default, transparent, ....%%

===== Signature =====

<code>
@property cursor_style @beta {
    get {}
    set {
        return: bool;
    }
    values {
        style: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_widget_cursor_style_get(const Eo *obj);
Eina_Bool efl_ui_widget_cursor_style_set(Eo *obj, const char *style);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:cursor_style|Efl.Ui.Widget.cursor_style]]

