~~Title: Efl.Ui.Widget.focus_highlight_geometry~~
====== Efl.Ui.Widget.focus_highlight_geometry ======

===== Values =====

  * **region** - %%The rectangle area.%%


\\ {{page>:develop:api-include:efl:ui:widget:property:focus_highlight_geometry:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property focus_highlight_geometry {
    get @protected {}
    values {
        region: Eina.Rect;
    }
}
</code>

===== C signature =====

<code c>
Eina_Rect efl_ui_widget_focus_highlight_geometry_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:widget:property:focus_highlight_geometry|Efl.Ui.Widget.focus_highlight_geometry]]

