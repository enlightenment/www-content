~~Title: Efl.Ui.Widget.focus_parent~~
====== Efl.Ui.Widget.focus_parent ======

===== Values =====

  * **logical_parent** - %%The focus parent.%%


\\ {{page>:develop:api-include:efl:ui:widget:property:focus_parent:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:focus:object:property:focus_parent|Efl.Ui.Focus.Object.focus_parent]] **(get)**.//===== Signature =====

<code>
@property focus_parent {
    get {}
    values {
        logical_parent: Efl.Ui.Focus.Object;
    }
}
</code>

===== C signature =====

<code c>
Efl_Ui_Focus_Object *efl_ui_focus_object_focus_parent_get(const Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:focus:object:property:focus_parent|Efl.Ui.Focus.Object.focus_parent]]
  * [[:develop:api:efl:ui:widget:property:focus_parent|Efl.Ui.Widget.focus_parent]]
  * [[:develop:api:efl:ui:focus:composition_adapter:property:focus_parent|Efl.Ui.Focus.Composition_Adapter.focus_parent]]
  * [[:develop:api:efl:ui:calendar_item:property:focus_parent|Efl.Ui.Calendar_Item.focus_parent]]

