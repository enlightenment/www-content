~~Title: Efl.Ui.Widget.i18n_name~~
====== Efl.Ui.Widget.i18n_name ======

===== Description =====

%%Accessible name of the object.%%
{{page>:develop:api-include:efl:ui:widget:property:i18n_name:description&nouser&nolink&nodate}}

===== Values =====

  * **i18n_name** - %%Accessible name%%

//Overridden from [[:develop:api:efl:access:object:property:i18n_name|Efl.Access.Object.i18n_name]] **(get)**.//===== Signature =====

<code>
@property i18n_name @beta {
    get {}
    set {}
    values {
        i18n_name: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_access_object_i18n_name_get(const Eo *obj);
void efl_access_object_i18n_name_set(Eo *obj, const char *i18n_name);
</code>

===== Implemented by =====

  * [[:develop:api:efl:access:object:property:i18n_name|Efl.Access.Object.i18n_name]]
  * [[:develop:api:efl:ui:widget:property:i18n_name|Efl.Ui.Widget.i18n_name]]
  * [[:develop:api:efl:ui:win:property:i18n_name|Efl.Ui.Win.i18n_name]]
  * [[:develop:api:efl:ui:text:property:i18n_name|Efl.Ui.Text.i18n_name]]
  * [[:develop:api:efl:ui:spin_button:property:i18n_name|Efl.Ui.Spin_Button.i18n_name]]

