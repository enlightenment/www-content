~~Title: Efl.Ui.Widget.mirrored~~
====== Efl.Ui.Widget.mirrored ======

===== Description =====

%%Whether this object should be mirrored.%%

%%If mirrored, an object is in RTL (right to left) mode instead of LTR (left to right).%%


{{page>:develop:api-include:efl:ui:widget:property:mirrored:description&nouser&nolink&nodate}}

===== Values =====

  * **rtl** - %%%%''true''%% for RTL, %%''false''%% for LTR (default).%%
==== Setter ====

%%This sets the mirror state of the whole sub-tree.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget:property:mirrored:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:ui:i18n:property:mirrored|Efl.Ui.I18n.mirrored]] **(get, set)**.//===== Signature =====

<code>
@property mirrored @pure_virtual {
    get {}
    set {}
    values {
        rtl: bool (false);
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_mirrored_get(const Eo *obj);
void efl_ui_mirrored_set(Eo *obj, Eina_Bool rtl);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:i18n:property:mirrored|Efl.Ui.I18n.mirrored]]
  * [[:develop:api:efl:canvas:layout:property:mirrored|Efl.Canvas.Layout.mirrored]]
  * [[:develop:api:efl:ui:widget:property:mirrored|Efl.Ui.Widget.mirrored]]
  * [[:develop:api:efl:ui:panel:property:mirrored|Efl.Ui.Panel.mirrored]]
  * [[:develop:api:efl:ui:scroll:manager:property:mirrored|Efl.Ui.Scroll.Manager.mirrored]]

