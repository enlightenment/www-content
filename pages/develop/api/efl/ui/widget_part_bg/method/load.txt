~~Title: Efl.Ui.Widget_Part_Bg.load~~
====== Efl.Ui.Widget_Part_Bg.load ======

===== Description =====

%%Perform all necessary operations to open and load file data into the object using the %%[[:develop:api:efl:file:property:file|Efl.File.file]]%% (or %%[[:develop:api:efl:file:property:mmap|Efl.File.mmap]]%%) and %%[[:develop:api:efl:file:property:key|Efl.File.key]]%% properties.%%

%%In the case where %%[[:develop:api:efl:file:property:file|Efl.File.file.set]]%% has been called on an object, this will internally open the file and call %%[[:develop:api:efl:file:property:mmap|Efl.File.mmap.set]]%% on the object using the opened file handle.%%

%%Calling %%[[:develop:api:efl:file:method:load|Efl.File.load]]%% on an object which has already performed file operations based on the currently set properties will have no effect.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:widget_part_bg:method:load:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:file:method:load|Efl.File.load]].//===== Signature =====

<code>
load {
    return: Eina.Error;
}
</code>

===== C signature =====

<code c>
Eina_Error efl_file_load(Eo *obj);
</code>

===== Implemented by =====

  * [[:develop:api:efl:file:method:load|Efl.File.load]]
  * [[:develop:api:evas:canvas3d:mesh:method:load|Evas.Canvas3D.Mesh.load]]
  * [[:develop:api:efl:ui:win_part:method:load|Efl.Ui.Win_Part.load]]
  * [[:develop:api:efl:canvas:video:method:load|Efl.Canvas.Video.load]]
  * [[:develop:api:efl:ui:text:method:load|Efl.Ui.Text.load]]
  * [[:develop:api:efl:canvas:image:method:load|Efl.Canvas.Image.load]]
  * [[:develop:api:efl:canvas:layout:method:load|Efl.Canvas.Layout.load]]
  * [[:develop:api:efl:ui:layout:method:load|Efl.Ui.Layout.load]]
  * [[:develop:api:efl:ui:image:method:load|Efl.Ui.Image.load]]
  * [[:develop:api:efl:ui:image_zoomable:method:load|Efl.Ui.Image_Zoomable.load]]
  * [[:develop:api:efl:ui:animation_view:method:load|Efl.Ui.Animation_View.load]]
  * [[:develop:api:evas:canvas3d:texture:method:load|Evas.Canvas3D.Texture.load]]
  * [[:develop:api:efl:canvas:vg:object:method:load|Efl.Canvas.Vg.Object.load]]
  * [[:develop:api:efl:ui:widget_part_bg:method:load|Efl.Ui.Widget_Part_Bg.load]]
  * [[:develop:api:efl:ui:video:method:load|Efl.Ui.Video.load]]
  * [[:develop:api:efl:ui:popup_part_backwall:method:load|Efl.Ui.Popup_Part_Backwall.load]]
  * [[:develop:api:efl:ui:bg:method:load|Efl.Ui.Bg.load]]

