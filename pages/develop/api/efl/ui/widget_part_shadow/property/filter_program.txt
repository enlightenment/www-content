~~Title: Efl.Ui.Widget_Part_Shadow.filter_program~~
====== Efl.Ui.Widget_Part_Shadow.filter_program ======

===== Description =====

%%A graphical filter program on this object.%%

%%Valid for Text and Image objects at the moment.%%

%%The argument passed to this function is a string containing a valid Lua program based on the filters API as described in the "EFL Graphics Filters" reference page.%%

%%Set to %%''null''%% to disable filtering.%%
{{page>:develop:api-include:efl:ui:widget_part_shadow:property:filter_program:description&nouser&nolink&nodate}}

===== Values =====

  * **code** - %%The Lua program source code.%%
  * **name** - %%An optional name for this filter.%%

//Overridden from [[:develop:api:efl:gfx:filter:property:filter_program|Efl.Gfx.Filter.filter_program]] **(get, set)**.//===== Signature =====

<code>
@property filter_program @pure_virtual {
    get {}
    set {}
    values {
        code: string;
        name: string @optional;
    }
}
</code>

===== C signature =====

<code c>
void efl_gfx_filter_program_get(const Eo *obj, const char **code, const char **name);
void efl_gfx_filter_program_set(Eo *obj, const char *code, const char *name);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:filter:property:filter_program|Efl.Gfx.Filter.filter_program]]
  * [[:develop:api:efl:ui:widget_part_shadow:property:filter_program|Efl.Ui.Widget_Part_Shadow.filter_program]]
  * [[:develop:api:efl:canvas:filter:internal:property:filter_program|Efl.Canvas.Filter.Internal.filter_program]]
  * [[:develop:api:efl:canvas:image_internal:property:filter_program|Efl.Canvas.Image_Internal.filter_program]]
  * [[:develop:api:efl:canvas:text:property:filter_program|Efl.Canvas.Text.filter_program]]

