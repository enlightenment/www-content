~~Title: Efl.Ui.Win: maximized,changed~~

===== Description =====

%%Called when window is set to or from maximized%%

//Since 1.22//

{{page>:develop:api-include:efl:ui:win:event:maximized_changed:description&nouser&nolink&nodate}}

===== Signature =====

<code>
maximized,changed: bool;
</code>

===== C information =====

<code c>
EFL_UI_WIN_EVENT_MAXIMIZED_CHANGED(Eina_Bool)
</code>

===== C usage =====

<code c>
static void
on_efl_ui_win_event_maximized_changed(void *data, const Efl_Event *event)
{
    Eina_Bool info = event->info;
    Eo *obj = event->object;
    Data *d = data;

    /* event hander code */
}

static void
setup_event_handler(Eo *obj, Data *d)
{
    efl_event_callback_add(obj, EFL_UI_WIN_EVENT_MAXIMIZED_CHANGED, on_efl_ui_win_event_maximized_changed, d);
}

</code>
