~~Title: Efl.Ui.Win.debug_name_override~~
====== Efl.Ui.Win.debug_name_override ======

===== Description =====

%%Build a read-only name for this object used for debugging.%%

%%Multiple calls using efl_super() can be chained in order to build the entire debug name, from parent to child classes. In C the usual way to build the string is as follows:%%

%%efl_debug_name_override(efl_super(obj, MY_CLASS), sb); eina_strbuf_append_printf(sb, "new_information");%%

%%Usually more debug information should be added to %%''sb''%% after calling the super function.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:method:debug_name_override:description&nouser&nolink&nodate}}

//Overridden from [[:develop:api:efl:ui:widget:method:debug_name_override|Efl.Object.debug_name_override]].//===== Signature =====

<code>
debug_name_override {
    params {
        @in sb: strbuf;
    }
}
</code>

===== C signature =====

<code c>
void efl_debug_name_override(Eo *obj, Eina_Strbuf *sb);
</code>

===== Parameters =====

  * **sb** //(in)// - %%A string buffer, must not be %%''null''%%.%%

===== Implemented by =====

  * [[:develop:api:efl:object:method:debug_name_override|Efl.Object.debug_name_override]]
  * [[:develop:api:efl:canvas:object:method:debug_name_override|Efl.Canvas.Object.debug_name_override]]
  * [[:develop:api:efl:canvas:image_internal:method:debug_name_override|Efl.Canvas.Image_Internal.debug_name_override]]
  * [[:develop:api:efl:canvas:group:method:debug_name_override|Efl.Canvas.Group.debug_name_override]]
  * [[:develop:api:efl:canvas:layout:method:debug_name_override|Efl.Canvas.Layout.debug_name_override]]
  * [[:develop:api:efl:ui:widget:method:debug_name_override|Efl.Ui.Widget.debug_name_override]]
  * [[:develop:api:efl:ui:win:method:debug_name_override|Efl.Ui.Win.debug_name_override]]

