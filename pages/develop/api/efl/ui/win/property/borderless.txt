~~Title: Efl.Ui.Win.borderless~~
====== Efl.Ui.Win.borderless ======

===== Description =====

%%The borderless state of a window.%%

%%This function requests the Window Manager not to draw any decoration around the window.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:borderless:description&nouser&nolink&nodate}}

===== Values =====

  * **borderless** - %%If %%''true''%%, the window is borderless.%%

===== Signature =====

<code>
@property borderless @beta {
    get {}
    set {}
    values {
        borderless: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_borderless_get(const Eo *obj);
void efl_ui_win_borderless_set(Eo *obj, Eina_Bool borderless);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:borderless|Efl.Ui.Win.borderless]]

