~~Title: Efl.Ui.Win.focus_highlight_animate~~
====== Efl.Ui.Win.focus_highlight_animate ======

===== Description =====

%%Whether focus highlight should animate or not.%%

%%See also %%[[:develop:api:efl:ui:win:property:focus_highlight_style|Efl.Ui.Win.focus_highlight_style]]%%. See also %%[[:develop:api:efl:ui:win:property:focus_highlight_enabled|Efl.Ui.Win.focus_highlight_enabled]]%%.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:focus_highlight_animate:description&nouser&nolink&nodate}}

===== Values =====

  * **animate** - %%The enabled value for the highlight animation.%%

===== Signature =====

<code>
@property focus_highlight_animate {
    get {}
    set {}
    values {
        animate: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_focus_highlight_animate_get(const Eo *obj);
void efl_ui_win_focus_highlight_animate_set(Eo *obj, Eina_Bool animate);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:focus_highlight_animate|Efl.Ui.Win.focus_highlight_animate]]

