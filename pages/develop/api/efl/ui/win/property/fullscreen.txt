~~Title: Efl.Ui.Win.fullscreen~~
====== Efl.Ui.Win.fullscreen ======

===== Description =====

%%The fullscreen state of a window.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:fullscreen:description&nouser&nolink&nodate}}

===== Values =====

  * **fullscreen** - %%If %%''true''%%, the window is fullscreen.%%

===== Signature =====

<code>
@property fullscreen {
    get {}
    set {}
    values {
        fullscreen: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_fullscreen_get(const Eo *obj);
void efl_ui_win_fullscreen_set(Eo *obj, Eina_Bool fullscreen);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:fullscreen|Efl.Ui.Win.fullscreen]]

