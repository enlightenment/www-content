~~Title: Efl.Ui.Win.position~~
====== Efl.Ui.Win.position ======

===== Description =====

%%The 2D position of a canvas object.%%

%%The position is absolute, in pixels, relative to the top-left corner of the window, within its border decorations (application space).%%

//Since 1.22//


{{page>:develop:api-include:efl:ui:win:property:position:description&nouser&nolink&nodate}}

===== Values =====

  * **pos** - %%A 2D coordinate in pixel units.%%
==== Getter ====

%%Retrieves the position of the given canvas object.%%

//Since 1.22//


{{page>:develop:api-include:efl:ui:win:property:position:getter_description&nouser&nolink&nodate}}

==== Setter ====

%%Moves the given canvas object to the given location inside its canvas' viewport. If unchanged this call may be entirely skipped, but if changed this will trigger move events, as well as potential pointer,in or pointer,out events.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:position:getter_description&nouser&nolink&nodate}}


//Overridden from [[:develop:api:efl:ui:widget:property:position|Efl.Gfx.Entity.position]] **(set)**.//===== Signature =====

<code>
@property position @pure_virtual {
    get {}
    set {}
    values {
        pos: Eina.Position2D;
    }
}
</code>

===== C signature =====

<code c>
Eina_Position2D efl_gfx_entity_position_get(const Eo *obj);
void efl_gfx_entity_position_set(Eo *obj, Eina_Position2D pos);
</code>

===== Implemented by =====

  * [[:develop:api:efl:gfx:entity:property:position|Efl.Gfx.Entity.position]]
  * [[:develop:api:efl:canvas:vg:node:property:position|Efl.Canvas.Vg.Node.position]]
  * [[:develop:api:efl:ui:win:property:position|Efl.Ui.Win.position]]
  * [[:develop:api:efl:ui:image:property:position|Efl.Ui.Image.position]]
  * [[:develop:api:efl:ui:image_zoomable:property:position|Efl.Ui.Image_Zoomable.position]]
  * [[:develop:api:efl:ui:widget:property:position|Efl.Ui.Widget.position]]
  * [[:develop:api:efl:ui:table:property:position|Efl.Ui.Table.position]]
  * [[:develop:api:efl:ui:box:property:position|Efl.Ui.Box.position]]
  * [[:develop:api:efl:ui:animation_view:property:position|Efl.Ui.Animation_View.position]]
  * [[:develop:api:efl:ui:text:property:position|Efl.Ui.Text.position]]
  * [[:develop:api:efl:ui:textpath:property:position|Efl.Ui.Textpath.position]]
  * [[:develop:api:efl:ui:popup:property:position|Efl.Ui.Popup.position]]
  * [[:develop:api:efl:ui:relative_layout:property:position|Efl.Ui.Relative_Layout.position]]
  * [[:develop:api:efl:canvas:object:property:position|Efl.Canvas.Object.position]]
  * [[:develop:api:efl:canvas:group:property:position|Efl.Canvas.Group.position]]
  * [[:develop:api:efl:canvas:video:property:position|Efl.Canvas.Video.position]]
  * [[:develop:api:efl:canvas:event_grabber:property:position|Efl.Canvas.Event_Grabber.position]]
  * [[:develop:api:efl:canvas:layout:property:position|Efl.Canvas.Layout.position]]
  * [[:develop:api:efl:ui:pan:property:position|Efl.Ui.Pan.position]]
  * [[:develop:api:efl:ui:image_zoomable_pan:property:position|Efl.Ui.Image_Zoomable_Pan.position]]

