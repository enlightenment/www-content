~~Title: Efl.Ui.Win.prop_focus_skip~~
====== Efl.Ui.Win.prop_focus_skip ======

===== Values =====

  * **skip** - %%The skip flag state (%%''true''%% if it is to be skipped).%%


\\ {{page>:develop:api-include:efl:ui:win:property:prop_focus_skip:description&nouser&nolink&nodate}}

===== Signature =====

<code>
@property prop_focus_skip @beta {
    set {}
}
</code>

===== C signature =====

<code c>
void efl_ui_win_prop_focus_skip_set(Eo *obj, Eina_Bool skip);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:prop_focus_skip|Efl.Ui.Win.prop_focus_skip]]

