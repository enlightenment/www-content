~~Title: Efl.Ui.Win.screen_constrain~~
====== Efl.Ui.Win.screen_constrain ======

===== Description =====

%%Constrain the maximum width and height of a window to the width and height of the screen.%%

%%When %%''constrain''%% is %%''true''%%, %%''obj''%% will never resize larger than the screen.%%

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:screen_constrain:description&nouser&nolink&nodate}}

===== Values =====

  * **constrain** - %%%%''true''%% to restrict the window's maximum size.%%

===== Signature =====

<code>
@property screen_constrain @beta {
    get {}
    set {}
    values {
        constrain: bool;
    }
}
</code>

===== C signature =====

<code c>
Eina_Bool efl_ui_win_screen_constrain_get(const Eo *obj);
void efl_ui_win_screen_constrain_set(Eo *obj, Eina_Bool constrain);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:screen_constrain|Efl.Ui.Win.screen_constrain]]

