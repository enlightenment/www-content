~~Title: Efl.Ui.Win.win_name~~
====== Efl.Ui.Win.win_name ======

===== Description =====

%%The window name.%%

%%The meaning of name depends on the underlying windowing system.%%

%%The window name is a construction property that can only be set at creation time, before finalize. In C this means inside %%''efl_add''%%().%%

<note>
%%Once set, it cannot be modified afterwards.%%
</note>

//Since 1.22//
{{page>:develop:api-include:efl:ui:win:property:win_name:description&nouser&nolink&nodate}}

===== Values =====

  * **name** - %%Window name%%

===== Signature =====

<code>
@property win_name {
    get {}
    set {}
    values {
        name: string;
    }
}
</code>

===== C signature =====

<code c>
const char *efl_ui_win_name_get(const Eo *obj);
void efl_ui_win_name_set(Eo *obj, const char *name);
</code>

===== Implemented by =====

  * [[:develop:api:efl:ui:win:property:win_name|Efl.Ui.Win.win_name]]

