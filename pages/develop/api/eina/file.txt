~~Title: Eina.File~~

===== Description =====

%%Eina file data structure%%

//Since 1.22//

{{page>:develop:api-include:eina:file:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:eina:file:fields&nouser&nolink&nodate}}


===== Signature =====

<code>
struct @extern Eina.File;
</code>

===== C signature =====

<code c>
typedef struct _Eina_File Eina_File;
</code>
