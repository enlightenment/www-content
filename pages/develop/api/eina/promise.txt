~~Title: Eina.Promise~~

===== Description =====

%%Eina promise type%%

//Since 1.22//

{{page>:develop:api-include:eina:promise:description&nouser&nolink&nodate}}

===== Fields =====

{{page>:develop:api-include:eina:promise:fields&nouser&nolink&nodate}}


===== Signature =====

<code>
struct @extern @free(eina_promise_free) Eina.Promise;
</code>

===== C signature =====

<code c>
typedef struct _Eina_Promise Eina_Promise;
</code>
