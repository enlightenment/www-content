---
~~Title: Building EFL for Nvidia ~~
---

# Building EFL for Nvidia
[EFL](/about-efl.md) defaults to using OpenGL-ES and EGL. This is by far the most widely supported set of OpenGL APIs with drivers. [Nvidia](https://www.nvidia.com) support both full OpenGL/GLX and OpenGL-ES/EGL as do all the OSS Mesa drivers and various embedded platforms only support OpenGL-ES/EGL. Unfortunately there is a [Nvidia](https://www.nvidia.com)-specific problem. Performance with EGL/GL-ES is bad (you will get stuttering) and other glitch bugs will happen too. Users have reported this and the solution appears to be to switch to full OpenGL/GLX as opposed to OpenGL-ES/EGL. The long-term solution is to report issues to [Nvidia](https://www.nvidia.com) and have them fix things or to move away from [Nvidia](https://www.nvidia.com) GPUs to ones with better support. Our recommendation by far is to stick to hardware that has proper full Open Source driver support, and that does not include [Nvidia](https://www.nvidia.com).

To do this you will need to recompile [EFL](/about-efl). You will need to ensure the meson build options have `-Dopengl=full` as the OpenGL build option. You also will have to ensure the **DRM** and **Wayland** options are not enabled as these depend on OpenGL-ES/EGL and cannot work with GLX. If they are enabled, switch them to being disabled with `-Ddrm=false` and `-Dwl=false`.

You will also need to ensure [Enlightenment](/about-enlightenment) itself is also built without **Wayland** support so has `-Dwl=false` or no **Wayland** option set to true.
