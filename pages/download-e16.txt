=== Download Enlightenment E16 ===

The latest version of [[e16|DR16]] is 1.0.30, released on August 10th, 2024.
Packages for the current and previous releases of DR16,
core themes, epplets, e16keyedit, and imlib2 can be found on the
[[https://sourceforge.net/project/showfiles.php?group_id=2|SourceForge download page]].

