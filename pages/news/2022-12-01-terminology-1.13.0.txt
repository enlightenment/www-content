=== Terminology 1.13.0 Release ===
  * //2022-12-01 - by Boris Faure//

> “Life is like riding a bicycle. To keep your balance, you must keep moving.” ― Albert Einstein

This year is coming to an end and it is time again for a new release of Terminology.

This new release comes with the following changes:

== Additions ==
    * Colorshemes: add Fir Dark, Selenized Dark, Selenized Black, Selenized Light and Selenized White schemes
    * New translation: Indonesian

== Improvements ==
    * Translation updates for Catalan, Chinese (Simplified), Croatian, Dutch, French, German, Italian, Portuguese, Portuguese (Brazil), Russian, Spanish, Swedish, Turkish
    * Handle scale changes on the fly
    * Better named option to enable/disable typing sounds
    * Improved README file
    * The ''tyls'' tool now supports ''.pls'' files
    * Fade the background of the terminal with the background color defined in the color scheme
    * Update the default theme to customize selection arrows
    * Update color schemes about selection arrows
    * Better documentation of the ''tyalpha'' tool

== Fixes ==
    * Fix issue when restoring the cursor state
    * Fix issue preventing some red color from being displayed

== Internal improvements ==
    * Cleanup the code base about C reserved identifiers
    * Code analyzed with Coverity 2022.06
    * Test code with some Coccinelle scripts in GitHub's CI
    * Update the ChangeLog.theme file

== Download ==
^ ** LINK ** ^ ** SHA256 ** ^
| [[ https://download.enlightenment.org/rel/apps/terminology/terminology-1.13.0.tar.xz | Terminology 1.13.0 XZ]]  | ''16a37fecd7bbd63ec9de3ec6c0af331cee77d6dfda838a1b1573d6f298474da5'' |

{{ :news:195199316-b893d45b-9d7b-4ae2-be06-8cd256f3c72d.png |}}

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~