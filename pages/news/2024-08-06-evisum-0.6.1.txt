=== Evisum 0.6.1 Release ===
  * //2024-08-06 - by Alastair Poole//

The latest version of evisum.

Much of this was written from a psychiatric ward bed.

Changes:
      * Lots of bug fixes.

| LINK | SHA256 |
| [[https://download.enlightenment.org/rel/apps/evisum/evisum-0.6.1.tar.xz | evisum-0.6.1.tar.xz ]] | 832f20b8de13e290890810267cf41ed84cbb0c84e2e20a14f6783632612dadb9 |

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
