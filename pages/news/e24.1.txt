=== Enlightenment DR 0.24.1 Release ===
  * //2020-05-31 - by Carsten Haitzler//

{{ :e-flat-sample.jpg?640&direct | Sample cropped screenshot of Enlightenment }}

Hilights:
      * Optimized keymap response handling to not stall on startup
      * Fixed wayland frame request timestamp to be the 0 base same as others
      * Fix polkit auth handling when having to auth for non-user UID
      * Randr fix to move pointer on-screen if off screen after reconfigure
      * Fix non-systemd system suport for resume to not think it's suspended
      * Add delay to allow any pending frames to display before suspend

== Download ==

^ ** LINK ** ^ ** SHA256 ** ^
| [[ http://download.enlightenment.org/rel/apps/enlightenment/enlightenment-0.24.1.tar.xz | Enlightenment DR 0.24.1 XZ]]  | aee2b6178c918d71ebe661129f4008d773e70e5784651dadbcf56eec0a6d4a09 |

== Building and Dependencies ==

  - [[https://git.enlightenment.org/core/efl.git/tree/README                     | EFL]]
  - libpam (Linux only)

Highly recommended to ensure proper functionality (though you can live
without these):

  - connman (For network configuration support)
  - bluez5 (For bluetooth configuration and control)
  - bc (For the evrything module calculator mode)
  - pulseaudio (For proper audio device control and redirection)
  - acpid (For systems with ACPI for lid events, AC/Battery plug in/out etc.)
  - packagekit (For the built in system updates monitoring and updater)
  - udisks2 (For removable storage mounting/unmounting)
  - ddcutil (specifically libddcutil.so.2 for backlight control)
  - gdb (If you want automatic backtraces on a crash in ~/.e-crashdump.txt - don't forget to build EFL and E with gdb debugging to make this useful)

**Note:** Enlightenment 0.24.1 depends on EFL **v1.24.1** or newer.

{{:blank.png?nolink&100|}}
~~DISCUSSIONS~~
